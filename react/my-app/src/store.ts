import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import logger from 'redux-logger';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { IAuthState } from './auth/state'
import { IAuthActions } from './auth/actions'

import { IRegisterState } from './register/state'
import { IRegisterActions } from './register/actions'

import { IUserDataState } from './userData/state'
import { IUserDataActions } from './userData/actions'

import { IPersonalInfoState } from './personalInfo/state';
import { IPersonalInfoActions } from './personalInfo/actions'

import { INotificationState } from './notification/state'
import { INotificationActions } from './notification/actions'

import { IPropertiesListState } from './propertiesList/state'
import { IPropertiesListActions } from './propertiesList/actions'

import { IDistinctCountriesState } from './distinctCountries/state';
import { IDistinctCountriesActions } from './distinctCountries/actions'

import { IAddPropertyFailState } from './addPropertiesFail/state';
import { IAddPropertyFailActions } from './addPropertiesFail/actions';

import { ICountriesListState, ICurrenciesListState, IPropertyTypesListState, IBusinessNaturesListState } from './list/state';
import { ICountriesListActions, ICurrenciesListActions, IPropertyTypesListActions, IBusinessNaturesListActions } from './list/actions'

import { IPropertyState } from './property/state'
import { IPropertyActions } from './property/actions'

import { IPropertyEditState } from './editPropertyState/state'
import { IPropertyEditActions } from './editPropertyState/actions'

import { IAddUnitFailState } from './addUnitFail/state'
import { IAddUnitFailActions } from './addUnitFail/actions'

import { IUnitState } from './unit/state'
import { IUnitActions } from './unit/actions'

import { ISingleUnitState } from './singleUnit/state'
import { ISingleUnitActions } from './singleUnit/actions'

import { IContractDetailsState } from './contract/state'
import { IContractDetailsActions } from './contract/actions'

import { IUnitsListState } from './unitsList/state'
import { IUnitsListActions } from './unitsList/actions'

import { IContractDraftState } from  './contractDraft/state'
import { IContractDraftActions } from './contractDraft/actions'

import { IBriefContractsState } from  './briefContract/state'
import { IBriefContractsActions } from './briefContract/actions'

import { IContractDistinctCountriesState } from './contractDistinctCountries/state'
import { IContractDistinctCountriesActions } from './contractDistinctCountries/actions'

import { IDistinctUnitsListState } from './distinctUnitsList/state';
import { IDistinctUnitsListActions } from './distinctUnitsList/actions'

import { IContractListState } from './contractList/state';
import { IContractListActions } from './contractList/actions'

import {IFloorPlanUnitState} from './floorPlanUnit/state'
import {IFloorPlanUnitActions} from './floorPlanUnit/actions'

import {IAddImageState} from './addImage/state'
import {IAddImageActions} from './addImage/actions'

import {ICoordinatesState} from './draggable/state'
import {ICoordinatesActions} from './draggable/actions'

import {IUserDataEditState} from './userDataEdit/state'
import {IUserDataEditActions} from './userDataEdit/actions'


import { authReducer } from './auth/reducers'
import { registerReducer } from './register/reducers'
import { personalInfoReducer } from './personalInfo/reducers';
import { notificationReducer } from './notification/reducers'
import { propertiesListReducer } from './propertiesList/reducers'
import { distinctCountriesListReducer } from './distinctCountries/reducers';
import { countriesListReducer, currenciesListReducer, propertyTypesListReducer, businessNaturesListReducer } from './list/reducers'
import { addPropertyFailedReducer } from './addPropertiesFail/reducers';
import { propertyReducer } from './property/reducers';
import { propertyEditReducer } from './editPropertyState/reducers'
import { addUnitFailReducer } from './addUnitFail/reducers'
import { unitReducer } from './unit/reducers'
import { singleUnitReducer } from './singleUnit/reducers'
import { contractDetailsReducer } from './contract/reducers'
import { unitsListReducer } from './unitsList/reducers'
import { contractDraftReducer } from './contractDraft/reducers'
import { briefContractsReducer } from './briefContract/reducers'
import { contractDistinctCountriesListReducer } from './contractDistinctCountries/reducers'
import { distinctUnitsListReducer } from './distinctUnitsList/reducers'
import { contractListReducer } from './contractList/reducers'
import { floorPlanUnitReducer } from './floorPlanUnit/reducers'
import { addImageReducer } from './addImage/reducers' 
import { coordinatesReducer } from './draggable/reducers'
import { userDataEditReducer } from './userDataEdit/reducers'

import { userDataReducer } from './userData/reducers'

export const history = createBrowserHistory()

export interface IRootState {
    auth: IAuthState,
    register: IRegisterState,
    personalInfo: IPersonalInfoState
    userData: IUserDataState
    notification: INotificationState
    propertiesList: IPropertiesListState
    distinctCountriesList: IDistinctCountriesState
    countriesList: ICountriesListState
    currenciesList: ICurrenciesListState
    propertyTypesList: IPropertyTypesListState
    addPropertyFail: IAddPropertyFailState
    property: IPropertyState
    propertyForEdit: IPropertyEditState
    addUnitFail: IAddUnitFailState
    unit: IUnitState
    singleUnit: ISingleUnitState
    contractDetails: IContractDetailsState
    unitsList: IUnitsListState
    contractDraft: IContractDraftState
    businessNaturesList: IBusinessNaturesListState
    briefContracts: IBriefContractsState
    contractDistinctCountriesList: IContractDistinctCountriesState
    distinctUnitsList: IDistinctUnitsListState
    contractList: IContractListState
    unitDetails: IFloorPlanUnitState
    image: IAddImageState
    coordinates: ICoordinatesState
    userDataEdit: IUserDataEditState
    // today: ITodayState
    // calendars: ICalendarState
    // todo: ITodoState
    // date: IDateState
    router: RouterState
}

type IRootAction =
    IAuthActions |
    IRegisterActions |
    IPersonalInfoActions |
    IUserDataActions |
    IPropertiesListActions |
    INotificationActions |
    IDistinctCountriesActions |
    ICountriesListActions |
    ICurrenciesListActions |
    IPropertyTypesListActions |
    IAddPropertyFailActions |
    IPropertyActions |
    IPropertyEditActions |
    IAddUnitFailActions |
    IUnitActions |
    ISingleUnitActions |
    IContractDetailsActions |
    IUnitsListActions |
    IContractDraftActions |
    IBusinessNaturesListActions |
    IBriefContractsActions |
    IContractDistinctCountriesActions |
    IDistinctUnitsListActions |
    IContractListActions |
    IFloorPlanUnitActions |
    IAddImageActions |
    ICoordinatesActions |
    IUserDataEditActions |
    CallHistoryMethodAction

const rootReducer = combineReducers<IRootState>({
    auth: authReducer,
    userData: userDataReducer,
    register: registerReducer,
    personalInfo: personalInfoReducer,
    notification: notificationReducer,
    propertiesList: propertiesListReducer,
    distinctCountriesList: distinctCountriesListReducer,
    countriesList: countriesListReducer,
    currenciesList: currenciesListReducer,
    propertyTypesList: propertyTypesListReducer,
    addPropertyFail: addPropertyFailedReducer,
    property: propertyReducer,
    propertyForEdit: propertyEditReducer,
    addUnitFail: addUnitFailReducer,
    unit: unitReducer,
    singleUnit: singleUnitReducer,
    contractDetails: contractDetailsReducer,
    unitsList: unitsListReducer,
    contractDraft: contractDraftReducer,
    businessNaturesList: businessNaturesListReducer,
    briefContracts: briefContractsReducer,
    contractDistinctCountriesList: contractDistinctCountriesListReducer,
    distinctUnitsList: distinctUnitsListReducer,
    unitDetails: floorPlanUnitReducer,
    contractList: contractListReducer,
    image: addImageReducer,
    coordinates: coordinatesReducer,
    userDataEdit: userDataEditReducer,
    
    router: connectRouter(history)
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    ));
