import React from 'react'


import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { ListGroupItem, UncontrolledCollapse } from 'reactstrap';
import { getPropertiesList } from './propertiesList/thunks';
import { push } from 'connected-react-router';
import { getDistinctCountries } from './distinctCountries/thunks';
import { getProperty } from './property/thunks';
import { getUnit } from './unit/thunks';
import { getContractByStatus } from './contractList/thunks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolderPlus } from '@fortawesome/free-solid-svg-icons';



interface IPropertiesListProps {
    propertiesList: {
        id: number
        address?: string
        country?: string
    }[]

    distinctCountriesList: {
        id?: number
        country?: string
    }[]

    getPropertiesList: () => void
    gotoCreatePage: () => void
    getDistinctCountries: () => void
    getProperty: (id:number) => void
    gotoPropertyPage: (id:number) => void
    getUnit: (id:number) => void
    getContractByStatus: (id:number) => void
}



class propertiesList extends React.Component<IPropertiesListProps> {

    private loadProperty = (id: number) => {
        this.props.getProperty(id)
        this.props.getUnit(id)
        this.props.gotoPropertyPage(id)
        this.props.getContractByStatus(id)
    }

    private create = () => {
        this.props.gotoCreatePage()
    }

    public componentDidMount() {
        this.props.getPropertiesList()
        this.props.getDistinctCountries()
    }

    public render() {
        console.log(this.props.distinctCountriesList)
        console.log(this.props.propertiesList)
        return (
            <div className='primary2'>
                {
                    this.props.propertiesList.length === 0 &&
                    <div className="primary2">
                        <h5>Please create a property</h5>
                    <br />
                        <Button onClick={() => this.create()}>{<FontAwesomeIcon icon={faFolderPlus} size = "3x"/>}</Button>
                    </div>
                }
                {
                    this.props.distinctCountriesList.length !== 0 &&
                    <div>
                        
                        <h6>Number of properties: {this.props.propertiesList.length}</h6>
                        
                        <br></br>
                        <div>
                            {this.props.distinctCountriesList.map((country, idx) =>
                                <div>
                                      <br></br>
                                    <Button color="primary" id={`btn_${country.id}`} style={{ }}>
                                        {country.country}
                                    </Button>
                                    
                                    <UncontrolledCollapse toggler={`#btn_${country.id}`} >
                                        {this.props.propertiesList.filter(property =>
                                            property.country === country.country).map(property =>
                                                <ListGroupItem key={property.id} className='lightblue'>
                                                    <div className="property">
                                                        <Button onClick={() => this.loadProperty(property.id)}>{property.address}</Button>
                                                    </div>
                                                </ListGroupItem>
                                            )
                                        }
                                    </UncontrolledCollapse>
                                </div>
                            )}
                        </div>
                        <div className="create-property"><Button onClick={() => this.create()}>{<FontAwesomeIcon icon={faFolderPlus} size = "3x"/>}</Button></div>
                        <br></br>
                    </div>
                    
                }
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({

    distinctCountriesList: state.distinctCountriesList.distinctCountriesList,
    propertiesList: state.propertiesList.propertiesList,

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getDistinctCountries: () => dispatch(getDistinctCountries()),
        getPropertiesList: () => dispatch(getPropertiesList()),
        gotoCreatePage: () => dispatch(push('/menu/properties/create_property')),
        getProperty: (id:number) => dispatch(getProperty(id)) ,
        getUnit: (id:number) => dispatch(getUnit(id)),
        gotoPropertyPage: (id:number) => dispatch(push(`/menu/properties/property/${id}`)),
        getContractByStatus: (id:number) => dispatch(getContractByStatus(id))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(propertiesList)