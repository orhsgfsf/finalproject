import React from 'react'
import { push } from 'connected-react-router';
import { getUserData } from './userData/thunks';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';


interface IHomeProps{
    getUserData:() => void
    gotoPersonalForm: () => void
    gotoProperties: () => void
    userData:any

}
class Home extends React.Component<IHomeProps>{


    public async componentDidMount() {
        await this.props.getUserData()
        if (this.props.userData.first_name === "" || this.props.userData.first_name === null){
            await this.props.gotoPersonalForm()
        }else{
            await this.props.gotoProperties()
        }
    }
    public render () {
        return (
            <div></div>
        )
    }
}

const mapStateToProps = (state:IRootState)=>({
    userData: state.userData.userData
});

const mapDispatchToProps = (dispatch:ThunkDispatch)=>({
    getUserData: () => dispatch(getUserData()),
    gotoPersonalForm: () => dispatch(push('/personal_information')),
    gotoProperties: () => dispatch(push('/menu/properties/property_interface'))
})


export default connect(mapStateToProps,mapDispatchToProps)( Home );
