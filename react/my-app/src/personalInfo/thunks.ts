import { Dispatch } from "redux";
import { IPersonalInfoActions, uploadInfoFailed} from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";

export function uploadInfo(personalInfo:{}){
    return async (dispatch:Dispatch<IPersonalInfoActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/user/personalinfo`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(personalInfo)
        })
       
        const result = await res.json()
        if (res.status !== 200){
            dispatch(uploadInfoFailed(result.msg))
          
        }else{
            dispatch(push('/menu/properties'))
        }
    }
}