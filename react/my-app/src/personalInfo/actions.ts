// export function uploadInfoSuccess(status:{}){
//     return {
//         type: "UPLOAD_PERSONAL_INFORMATION_SUCCESS" as "UPLOAD_PERSONAL_INFORMATION_SUCCESS",
//         status
//     } 
// }
// import  {IPersonalInfoState} from './state'

export function uploadInfoFailed(personalInfo:{ 
    first_name:  string,
    last_name: string,
    gender: string,
    id_number: string,
    date_of_birth: string,
    district_number: string,
    contact_number: string,
    company: string,
    br_number: string
}){
    console.log(personalInfo)
    return {
        type: "UPLOAD_PERSONAL_INFORMATION_FAILED" as "UPLOAD_PERSONAL_INFORMATION_FAILED",
        personalInfo
    }
}

type PersonalInfoCreators =  typeof uploadInfoFailed
export type IPersonalInfoActions = ReturnType<PersonalInfoCreators>

// typeof uploadInfoSuccess |