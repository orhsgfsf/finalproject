export interface IPersonalInfoState{
    msg: { 
        first_name: string,
        last_name: string,
        gender: string,
        id_number: string,
        date_of_birth: string,
        district_number: string,
        contact_number: string,
        company: string,
        br_number: string
    }

}
