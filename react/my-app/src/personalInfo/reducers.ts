import { IPersonalInfoState } from './state'
import { IPersonalInfoActions } from './actions'

const initialState = { 
    msg: {
        first_name:  "",
        last_name: "",
        gender: "",
        id_number: "",
        date_of_birth: "",
        district_number: "",
        contact_number: "",
        company: "",
        br_number: ""
    }

}

export function personalInfoReducer(state: IPersonalInfoState = initialState, action: IPersonalInfoActions) : IPersonalInfoState{
    switch (action.type) {
        // case "UPLOAD_PERSONAL_INFORMATION_SUCCESS":
        //     return {
        //         ...state,
        //         status: action.status
        //     }
        case "UPLOAD_PERSONAL_INFORMATION_FAILED":
            console.log(action.personalInfo)
            return {
                ...state,
                msg: action.personalInfo
            }
        default:
            return state
    }
}