import React from 'react'


import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';

import { connect } from 'react-redux';

import { ListGroupItem, UncontrolledCollapse } from 'reactstrap';
import { getPropertiesList } from './propertiesList/thunks';
import { push } from 'connected-react-router';
import { getDistinctCountries } from './distinctCountries/thunks';
// import { getProperty } from './property/thunks';
import { getUnit } from './unit/thunks';
import { getContractByPropertyId } from './contractList/thunks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileSignature } from '@fortawesome/free-solid-svg-icons';



interface IPropertiesListProps {
    propertiesList: {
        id: number
        address?: string
        country?: string
    }[]

    distinctCountriesList: {
        id?: number
        country?: string
    }[]

    getPropertiesList: () => void
   
    getDistinctCountries: () => void
   
    
    getUnit: (property_id:number) => void
    getContractByPropertyId:(property_id:number) => void
    gotoCreateContractPage: () => void

    gotoContractListPage: (property_id:number) => void
}



class contractProperties extends React.Component<IPropertiesListProps> {

    private getContractByPropertyId = (property_id: number) => {
        this.props.getContractByPropertyId(property_id)
        this.props.getUnit(property_id)
        this.props.gotoContractListPage(property_id)
       
    }

    private create = () => {
        this.props.gotoCreateContractPage()
    }

    public componentDidMount() {
        console.log("DIDMOUNT?")
        this.props.getPropertiesList()
        this.props.getDistinctCountries()
    }

    public render() {
        console.log(this.props.distinctCountriesList)
        console.log(this.props.propertiesList)
        return (
            <div>
                
                   
                    <div>
                        <div className='primary2'>
                           <h5> Contract List </h5>
                        
                        <div>
                            {this.props.distinctCountriesList.map((country, idx) =>
                                <div>
                                    <br></br>
                                    <Button color="primary" id={`btn_${country.id}`} style={{  }}>
                                        {country.country}
                                    </Button>
                                    <UncontrolledCollapse toggler={`#btn_${country.id}`}>
                                        {this.props.propertiesList.filter(property =>
                                            property.country === country.country).map(property =>
                                                <ListGroupItem key={property.id} className='lightorange'>
                                                    <div className="property">
                                                        <Button onClick={() => this.getContractByPropertyId(property.id)}>{property.address}</Button>
                                                    </div>
                                                </ListGroupItem>
                                            )
                                        }
                                    </UncontrolledCollapse>
                                </div>
                            )}
                        </div>
                        <div className="create-property"><Button onClick={() => this.create()}>{<FontAwesomeIcon icon={faFileSignature} size = "2x"/>}</Button></div>
                        </div>
                    </div>
                    <br></br>
                
            </div>
            
        )
    }
}

const mapStateToProps = (state: IRootState) => ({

    distinctCountriesList: state.distinctCountriesList.distinctCountriesList,
    propertiesList: state.propertiesList.propertiesList,

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getDistinctCountries: () => dispatch(getDistinctCountries()),
        getPropertiesList: () => dispatch(getPropertiesList()),
        gotoCreateContractPage:() => dispatch(push('/menu/create_contract')),
        
        
        getUnit: (property_id:number) => dispatch(getUnit(property_id)),
        getContractByPropertyId: (property_id:number) => dispatch(getContractByPropertyId(property_id)),

        gotoContractListPage: (property_id:number) => dispatch(push(`/menu/contract/contract_list/${property_id}`))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(contractProperties)