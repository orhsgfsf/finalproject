import { IAddPropertyFailState } from './state'
import { IAddPropertyFailActions } from './actions'

const initialState: IAddPropertyFailState = {
    addFail:{
        address:"",
        country:"",
        propertyType:"",
        grossFloorArea:"",
        saleableArea:"",
        governmentRent:"",
        managementFee:"",
        currency:"",
        rates:"",
        deposit:"",
        reminder:"",
    }
    
    
}

export function addPropertyFailedReducer(state: IAddPropertyFailState = initialState, action: IAddPropertyFailActions):IAddPropertyFailState{
    switch (action.type) {
        case "ADD_PROPERTY_FAILED":
            console.log(action.addFail)
            return {
                ...state,
                addFail: action.addFail
            }
        case "RESET":
            return{
                ...state,
                addFail: initialState.addFail
            }

        default:
            return state
    }
}