import { Dispatch } from "redux";
import { IAddPropertyFailActions, addPropertyFailed, } from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";
import { IPropertyInformation } from '../CreateProperty'
import { addDistinctCountrySuccess, IDistinctCountriesActions, deleteDistinctCountrySuccess } from "../distinctCountries/actions";
import { addPropertySuccess, IPropertiesListActions, deletePropertySuccess } from "../propertiesList/actions";
import { ThunkDispatch } from "../store";



export function addProperty(propertyInformation: IPropertyInformation) {
    return async (dispatch: Dispatch<IAddPropertyFailActions | IDistinctCountriesActions | IPropertiesListActions | CallHistoryMethodAction>) => {
        console.log(propertyInformation)
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/properties`, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(propertyInformation)
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            dispatch(addPropertyFailed(result.msg))
        } else {
            dispatch(addPropertySuccess({
                id: result.data[0][0],
                address: propertyInformation.address,
                country: propertyInformation.country
            }))
            dispatch(addDistinctCountrySuccess({
                id: result.data[1][0],
                country: propertyInformation.country
            }))
            dispatch(push('/menu/properties/property_interface'))
        }
    }
}

export function updateProperty(id:number, propertyInformation: IPropertyInformation) {
    return async (dispatch: Dispatch<IAddPropertyFailActions |  IDistinctCountriesActions | IPropertiesListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/properties`, {
            method: "PATCH",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(propertyInformation)
        })
        const result = await res.json()
        if (res.status !== 200) {
            dispatch(addPropertyFailed(result.msg))

        } else {
            // dispatch(addDistinctCountrySuccess({
            //     id: result.data[1][0],
            //     country: propertyInformation.country
            // }))
            dispatch(deletePropertySuccess({
                id: id,
                address: propertyInformation.address,
                country: propertyInformation.country
            }))
            dispatch(addPropertySuccess({
                id: result.data[0][0],
                address: propertyInformation.address,
                country: propertyInformation.country
            }))
            console.log(result.data[0][0])
            dispatch(push(`/menu/properties/property/${result.data[0][0]}`))
        }
    }
}

export function deleteProperty(id:number, country_id:number, country:string){
    return async (dispatch:ThunkDispatch) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/properties/properties`,{
            method: "DELETE",
            headers:{
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        
        dispatch(deletePropertySuccess({id}))
        dispatch(checkCountry(country_id, country))
        dispatch(push('/menu/properties/property_interface'))
    }
}

export function checkCountry(country_id:number, country:string){
    return async (dispatch:Dispatch<IDistinctCountriesActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/country`,{
            method:"POST",
            headers:{
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({country_id})})
        const result = await res.json()
           
        if (result.data.length === 0){
            dispatch(deleteDistinctCountrySuccess({
                id:country_id,
                country: country
            }))

        }
        dispatch(push('/menu/properties/property_interface'))
    }
}