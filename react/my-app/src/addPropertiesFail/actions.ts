import { IInsideAddFailState } from "./state";

export function addPropertyFailed(addFail: IInsideAddFailState){
    return {
        type: "ADD_PROPERTY_FAILED" as "ADD_PROPERTY_FAILED",
        addFail
    }
}

export function resetPropertyFailed(){
    return {
        type: "RESET" as "RESET"
    }
}

type AddPropertyFailActionCreators = typeof addPropertyFailed | typeof resetPropertyFailed
export type IAddPropertyFailActions = ReturnType<AddPropertyFailActionCreators>