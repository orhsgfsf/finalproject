
export interface IAddPropertyFailState{
    addFail:{
        address:string
        country:string
        propertyType:string
        grossFloorArea:string
        saleableArea:string
        governmentRent:string
        managementFee:string
        currency:string
        rates:string
        deposit:string
        reminder:string

    }
  
}

export interface IInsideAddFailState{
    address:string
        country:string
        propertyType:string
        grossFloorArea:string
        saleableArea:string
        governmentRent:string
        managementFee:string
        currency:string
        rates:string
        deposit:string
        reminder:string
    
}