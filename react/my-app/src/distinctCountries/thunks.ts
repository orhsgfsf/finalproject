import {Dispatch} from "redux"
import { CallHistoryMethodAction} from "connected-react-router"
import { IDistinctCountriesActions,  getDistinctCountriesSuccess } from "./actions";

export function getDistinctCountries(){
    return async(dispatch:Dispatch<IDistinctCountriesActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/properties/distinctCountries`,{
            headers: {
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }})
            const result = await res.json() 
            console.log(result)

            if (res.status !== 200){
                // dispatch(failed("GET_DISTINCT_COUNTRIES_FAILED", result.msg))
            }else{
                console.log(result.data)
                dispatch(getDistinctCountriesSuccess(result.data))
            }
        }
    }