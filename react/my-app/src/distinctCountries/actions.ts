import { IDistinctCountries } from './state';



export function getDistinctCountriesSuccess(distinctCountriesList: IDistinctCountries[]) {
    console.log(distinctCountriesList)
    return {
        type: "GET_DISTINCT_COUNTRIES_SUCCESS" as "GET_DISTINCT_COUNTRIES_SUCCESS",
        distinctCountriesList
    }
}

export function addDistinctCountrySuccess(country:IDistinctCountries){
    return {
        type: "ADD_DISTINCT_COUNTRY_SUCCESS" as "ADD_DISTINCT_COUNTRY_SUCCESS",
        country
    }
}

export function deleteDistinctCountrySuccess(country:IDistinctCountries){
    
    return {
        type: "DELETE_DISTINCT_COUNTRY_SUCCESS" as "DELETE_DISTINCT_COUNTRY_SUCCESS",
        country
    }
}

type FAILED = "GET_DISTINCT_COUNTRIES_FAILED"

export function failed(type: FAILED, msg: string) {
    return{
        type,
        msg
    }
}

type distinctCountriesListActionCreators = typeof getDistinctCountriesSuccess | typeof addDistinctCountrySuccess | typeof deleteDistinctCountrySuccess | typeof failed
export type IDistinctCountriesActions = ReturnType<distinctCountriesListActionCreators>
    
