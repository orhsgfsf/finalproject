export interface IDistinctCountries{
    id:number
    country?:string
}

export interface IDistinctCountriesState{
    distinctCountriesList:IDistinctCountries[]
    // msg:string
}