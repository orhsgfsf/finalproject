import React from 'react'
import { Row, Col, Input, Label, Button } from 'reactstrap';
import 'react-dropzone-uploader/dist/styles.css'
import { IRootState, ThunkDispatch } from './store';
import { addFloorPlanToDB, addFloorPlan } from './addImage/thunks';
import { connect } from 'react-redux';
import { clearImage } from './addImage/actions';
import { push } from 'connected-react-router';
import { clearCoordinates } from './draggable/thunks';

interface FloorPlanUploadProps {
    addFloorPlan: (file: File) => void
    addFloorPlanToDB: (floorPlan: IFloorPlan) => void
    clearImage: () => void
    back: (id: number) => void
    clearCoordinates: (id: number) => void
    image: string

    match: {
        params: {
            id: string
        }
    }
}

interface FloorPlanUploadState {
    x: number
    file: File | null
}

export interface IFloorPlan {
    id: number

    image: string
}

class FloorPlanUpload extends React.Component<FloorPlanUploadProps, FloorPlanUploadState>{
    constructor(props: FloorPlanUploadProps) {
        super(props)

        this.state = {
            x: 100,
            file: null
        }


    }


    // private Preview = ({ meta }: IPreviewProps) => {
    //     const { name, percent, status } = meta
    //     return (
    //       <span style={{ alignSelf: 'flex-start', margin: '40px', fontFamily: 'Helvetica' }}>
    //         {name}, {Math.round(percent)}%, {status}
    //       </span>
    //     )
    //   }
    // private handleSubmit = () => {
    //     if (this.state.file){
    //         this.props.addFile(this.state.file)
    //     }
    // }


    private handleFileOnChange = (files: FileList) => {
        console.log(files[0])
        this.props.addFloorPlan(files[0])
    }
    //    private handleChangeStatus = (meta:any) => {
    //        this.setState({
    //            file:meta.file
    //        })
    //       }
    private saveFloorPlan = () => {
        const floorPlan: IFloorPlan = {
            id: parseInt(this.props.match.params.id),
            image: this.props.image
        }
        console.log(floorPlan)
        this.props.addFloorPlanToDB(floorPlan)
        this.props.clearCoordinates(parseInt(this.props.match.params.id))
    }

    private back = () => {
        this.props.back(parseInt(this.props.match.params.id))
    }



    public componentWillUnmount() {
        this.props.clearImage()
    }

    public render() {
        return (
            <div>
                <Row>

                    <Col md="12" class="test">



                        <div className="upload-block">



                            {this.props.image !== "" ? <img className="upload-image" alt="abc " src={this.props.image} /> :
                                <div className="upload-information">Please Upload a Floor Plan<br /> (Better to Fit the Size of 800 x 600) <br />(Floor Plan Will be deleted if click Save Changes without it)</div>

                            }
                        </div>


                        <Input type='file' id={"file"} name="pic" style={{ display: 'none' }} onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />
                        <br></br>
                        <div className="upload-image-button"><Label className='upload' htmlFor={"file"}>Upload Floor Plan</Label></div>




                    </Col>
                </Row>
                <Row>
                    <Col md="12">
                        <div className="floor-plan-upload-button">
                            <Button onClick={() => this.saveFloorPlan()}>Save Changes</Button>
                            <Button onClick={() => this.back()}>Back</Button>
                        </div>


                    </Col>
                </Row>

            </div>
        )
    }


}

const mapStateToProps = (state: IRootState) => ({
    image: state.image.image

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        addFloorPlanToDB: (floorPlan: IFloorPlan) => dispatch(addFloorPlanToDB(floorPlan)),
        addFloorPlan: (file: File) => dispatch(addFloorPlan(file)),
        clearImage: () => dispatch(clearImage()),
        back: (id: number) => dispatch(push(`/menu/floor_plan/details/${id}`)),
        clearCoordinates: (id: number) => dispatch(clearCoordinates(id))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(FloorPlanUpload)