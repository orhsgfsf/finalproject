import { IDistinctUnitsListState } from './state'
import { IDistinctUnitsListActions } from './actions'

const initialState: IDistinctUnitsListState = {
    distinctUnitsList: []
    // msg: ""
}

export function distinctUnitsListReducer(state: IDistinctUnitsListState = initialState, action: IDistinctUnitsListActions):IDistinctUnitsListState{
    switch (action.type) {

        case "GET_DISTINCT_UNITS_LIST_SUCCESS":
            console.log(action.distinctUnitsList)
            return {
                ...state,
                distinctUnitsList: action.distinctUnitsList
            }

        // case "ADD_DISTINCT_COUNTRY_SUCCESS":
        //     console.log(state.distinctCountriesList)
        //     for (let country of state.distinctCountriesList) {
        //         console.log(country.id)
        //         if (country.country === action.country.country) {
                 
        //             return state
        //         }
        //     }
        //     console.log(state.distinctCountriesList)
        //     console.log(action.country)
        //     return {
        //         ...state,
        //         distinctCountriesList: [...state.distinctCountriesList, action.country]
        //     }

        // case "DELETE_DISTINCT_COUNTRY_SUCCESS":
   
        //     console.log(typeof state.distinctCountriesList)
        //     console.log(typeof action.country.id)
        //     return {
        //         ...state,
        //         distinctCountriesList: state.distinctCountriesList.filter(country => country.country !== action.country.country)
        //     }



        // case "GET_DISTINCT_COUNTRIES_FAILED":
        //     return {
        //         ...state,
        //         msg: action.msg
        //     }
        default:
            return state
    }
}