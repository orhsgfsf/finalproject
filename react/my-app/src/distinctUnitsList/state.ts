export interface IDistinctUnitsList{
    id:number
    unit:string
}

export interface IDistinctUnitsListState{
    distinctUnitsList:IDistinctUnitsList[]
    // msg:string
}