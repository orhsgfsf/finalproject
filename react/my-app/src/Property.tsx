import React from 'react'
import { Button, Row, Col, Container, Modal,  ModalBody } from 'reactstrap';
import './property.css';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { getProperty } from './property/thunks';
import { deleteProperty } from './addPropertiesFail/thunks';
import { IUnit } from './unit/state';
import { getUnit, deleteUnit } from './unit/thunks';
import moment from 'moment'
import { IContractList } from './contractList/state';
import { getContractByStatus } from './contractList/thunks';
import { getContractByCode } from './contractDraft/thunks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHandHoldingUsd, faDoorOpen, faHome, faHourglassHalf } from '@fortawesome/free-solid-svg-icons';


interface IPropertyProps {


    property: {
        address: string
        country: string
        propertyType: string
        gross_floor_area: number
        saleable_area: number
        government_rent: number
        management_fee: number
        currency: string
        rates: number
        reminder: string
        country_id: number
    }

    units: IUnit[]
    contractList: IContractList[]

    getProperty: (id: number) => void
    gotoEditPage: (id: number) => void
    deleteProperty: (id: number, country_id: number, country: string) => void
    createUnit: (id: number) => void
    getUnit: (id: number) => void
    gotoUnitEditPage: (id: number) => void
    deleteUnit: (id: number, property_id: number) => void
    getContractByStatus: (id: number) => void
    reRentContract: (code: string) => void
    gotoReRentPage: (code: string) => void

    match: {
        params: {
            id: string
        }
    }
}

interface IPropertyState {
    modal: boolean

}

class Property extends React.Component<IPropertyProps, IPropertyState> {
    constructor(props: IPropertyProps) {
        super(props)
        this.state = {
            modal: false
        }

        this.toggle = this.toggle.bind(this)
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }))
    }

    private onEdit = () => {
        this.props.gotoEditPage(parseInt(this.props.match.params.id))
    }

    private onDelete = async () => {
        await this.setState(prevState => ({
            modal: !prevState.modal
        }))
        await this.props.deleteProperty(parseInt(this.props.match.params.id), this.props.property.country_id, this.props.property.country)
    }

    private createUnit = () => {
        this.props.createUnit(parseInt(this.props.match.params.id))
    }

    private reRentContract = (code: string) => {
        this.props.reRentContract(code)
        this.props.gotoReRentPage(code)
    }

    private onDeleteUnit = async (unitId:number, propertyId:number) => {
        await this.setState(prevState => ({
            modal: !prevState.modal
        }))
        await this.props.deleteUnit(unitId, propertyId)
    }





    public componentDidMount() {
        this.props.getProperty(parseInt(this.props.match.params.id))
        this.props.getUnit(parseInt(this.props.match.params.id))
        this.props.getContractByStatus(parseInt(this.props.match.params.id))
    }
    public render() {
        console.log(this.props.units)
        console.log(this.props.property.country_id)
        console.log(this.props.contractList)
        return (

            <div>
                <div>
                    <b>
                        <div className="primary"><div><h5>{this.props.property.address}, {this.props.property.country}</h5></div>

                            <div>Total Leasable Units: {this.props.units.length}</div><Button onClick={this.createUnit}>Add Unit</Button></div>
                    </b>
                    <br></br>
                    <div className="primary">
                        <Container>
                            <Row>
                                <Col md="2" className="fontawesome">
                                    <div>{<FontAwesomeIcon icon={faHome} size="3x" />}</div>
                                </Col>
                                <Col md="10" className="square">
                                    <Container>
                                        <Row>
                                            <Col md="3">
                                            </Col>
                                            <Col md="9">
                                                <br></br>
                                                <div>Types of Property: {this.props.property.propertyType}</div>
                                                <div>Saleable Area: {this.props.property.saleable_area} sq.ft</div>
                                                <div>Gross Floor Area: {this.props.property.gross_floor_area} sq.ft</div>
                                                <div>Management Fee: {this.props.property.management_fee} {this.props.property.currency}</div>
                                                <div>Government Rent: {this.props.property.government_rent} {this.props.property.currency}</div>
                                                <div>Rates: {this.props.property.rates} {this.props.property.currency}</div>
                                                <div>Reminder: {this.props.property.reminder ? <div className="reminder">{this.props.property.reminder}</div> : <div>No any reminder</div>}</div>
                                                <br></br>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Col>
                            </Row>
                        </Container>
                        {
                            this.props.units.length === 0 ?
                                <div>
                                    <div>
                                        <Button onClick={this.onEdit}>Edit Property</Button>
                                    </div>
                                    <div>
                                        <Button color="red" className='delete' onClick={this.toggle}>Delete Property</Button>
                                       
                                        <Modal isOpen={this.state.modal} toggle={this.toggle}>

                                            <ModalBody toggle={this.toggle}>
                                                Are You Sure You Want to DELETE this Property? You Can't Go Back If You Do this.
                                            </ModalBody>
                                            <div className='space-between'>
                                                <Button color="red" className='delete' onClick={this.onDelete}>YES!</Button>{' '}
                                                <Button onClick={this.toggle}>No</Button>

                                            </div>
                                        </Modal>
                                        
                                    </div>
                                </div>
                                :
                                ""
                        }
                    </div>





                    {this.props.units.map(unit => {
                        return (
                            <div><br></br>
                                <div className="primary">
                                    <div>
                                        <Container>
                                            <Row>
                                                <Col md="2" className="fontawesome">
                                                    <div>{<FontAwesomeIcon icon={faDoorOpen} size="3x" />}</div>
                                                </Col>
                                                <Col md="10" className="square">
                                                    <Container>
                                                        <Row>
                                                            <Col md="3">
                                                            </Col>
                                                            <Col md="9">

                                                                <br></br>
                                                                <div>Unit: {unit.unit}</div>
                                                                <div>Saleable Area: {unit.saleable_area}</div>
                                                                <div>Gross Floor Area: {unit.gross_floor_area}</div>
                                                                <div>Rental Status: {unit.rental_status ? "Rented" : "Vacant"} {unit.contract_pending ? <div color="red">(Waiting the contract to be confirmed)</div> : ""} </div>
                                                                <div>Reminder: {unit.reminder !== "" ? unit.reminder : "No any reminder"}</div>
                                                                <br></br>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                </Col>
                                            </Row>
                                            {
                                                !(unit.rental_status) &&

                                                <div>
                                                    {unit.contract_pending === false &&
                                                        <div className='space-between'>
                                                           
                                                            <div><Button onClick={() => this.props.gotoUnitEditPage(unit.id)}>Edit Unit</Button></div>
                                                            <div>
                                                                <Button color="red" className='delete' onClick={this.toggle}>Delete Unit</Button>
                                                                
                                                                <Modal isOpen={this.state.modal} toggle={this.toggle}>

                                                                    <ModalBody toggle={this.toggle}>
                                                                        Are You Sure You Want to DELETE this Unit? You Can't Go Back If You Do this.
                                                                    </ModalBody>
                                                                    <div className='space-between'>
                                                                        <Button color="red" className='delete' onClick={() => this.onDeleteUnit(unit.id, unit.property_id)}>YES!</Button>{' '}
                                                                        <Button onClick={this.toggle}>No</Button>

                                                                    </div>
                                                                </Modal>
                                                                
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            }
                                        </Container>
                                    </div>
                                    <br></br>
                                </div>

                                <div>
                                    {this.props.contractList.length !== 0 &&
                                        <div>{this.props.contractList.filter(contract => (
                                            contract.unit_id === unit.id
                                        )).map(activatedContract => {

                                            return (
                                                <div><div className="primary">
                                                    {
                                                        activatedContract.contract_status === true &&
                                                        <div>
                                                            <Container>
                                                                <Row>
                                                                    <Col md="2" className="fontawesome">
                                                                        <div>{<FontAwesomeIcon icon={faHandHoldingUsd} size="3x" />}</div>
                                                                    </Col>
                                                                    <Col md="10" className="square">
                                                                        <Container>
                                                                            <Row>
                                                                                <Col md="3">
                                                                                </Col>
                                                                                <Col md="9">
                                                                                    <br></br>
                                                                                    <div>Contract</div>
                                                                                    <div>Rent: {activatedContract.currency} {activatedContract.rent_price}</div>
                                                                                    <div>Tenant Name: {activatedContract.tenant_name}</div>
                                                                                    <div>Business Nature: {activatedContract.tenant_business_nature}</div>
                                                                                    <div>Contact Person: {activatedContract.contact_person ? activatedContract.contact_person : <div>No Contact Person</div>}</div>
                                                                                    <div>Contact Number: {activatedContract.contact_person ? activatedContract.contact_number : <div>{activatedContract.contact_number} (Tenant's Contact Number)</div>}</div>
                                                                                    <div>Rent Due Date: {moment(`2000-1-${activatedContract.rent_due_date}`).format("Do")} of month</div>
                                                                                    <div>Contract Starting Date: {moment(activatedContract.contract_starting_date).format('YYYY-MM-DD')}</div>
                                                                                    <div>Contract Expiry Date: {moment(activatedContract.contract_expiry_date).format('YYYY-MM-DD')}</div>
                                                                                    <div>Deposit: {activatedContract.currency} {activatedContract.deposit}</div>
                                                                                    <div>Grace Period: {activatedContract.grace_period} days</div>
                                                                                    <br></br>
                                                                                </Col>
                                                                            </Row>
                                                                        </Container>
                                                                    </Col>
                                                                </Row>

                                                                {moment(activatedContract.contract_expiry_date).diff(moment(), 'days') < 0 ?
                                                                    <div>
                                                                        <div color="red">Expired!</div>
                                                                        <Button onClick={() => this.reRentContract(activatedContract.code)}>Re-rent Contract</Button>
                                                                        {/* <Button onClick={() => <ContractReRent value={activatedContract} />}>Re-rent Contract</Button> */}
                                                                    </div> : ""}
                                                            </Container>
                                                        </div>
                                                    }

                                                    {
                                                        activatedContract.confirm_clicked === false &&
                                                        <div>
                                                            <div>
                                                                <Container>
                                                                    <Row>
                                                                        <Col md="2" className="fontawesome">
                                                                            <div>{<FontAwesomeIcon icon={faHourglassHalf} size="3x" />}</div>
                                                                        </Col>
                                                                        <Col md="10" className="square">
                                                                            <Container>
                                                                                <Row>
                                                                                    <Col md="3">
                                                                                    </Col>
                                                                                    <Col md="9">
                                                                                        <br></br>
                                                                                        <div>Contract</div>
                                                                                        <div>Rent: {activatedContract.currency} {activatedContract.rent_price}</div>
                                                                                        <div>Tenant Name: Work in Progress...</div>
                                                                                        <div>Business Nature: Work in Progress...</div>
                                                                                        <div>Contact Person: Work in Progress...</div>
                                                                                        <div>Contact Number: Work in Progress...</div>
                                                                                        <div>Rent Due Date: {moment(`2000-1-${activatedContract.rent_due_date}`).format("Do")} of month</div>
                                                                                        <div>Contract Starting Date: {moment(activatedContract.contract_starting_date).format('YYYY-MM-DD')}</div>
                                                                                        <div>Contract Expiry Date: {moment(activatedContract.contract_expiry_date).format('YYYY-MM-DD')}</div>
                                                                                        <div>Deposit: {activatedContract.currency} {activatedContract.deposit}</div>
                                                                                        <div>Grace Period: {activatedContract.grace_period} days</div>
                                                                                        <div color="red">Work in Progress...</div>
                                                                                        <br></br>
                                                                                    </Col>
                                                                                </Row>
                                                                            </Container>
                                                                        </Col>
                                                                    </Row>
                                                                </Container>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                                    <br></br>
                                                </div>
                                            )
                                        })} </div>
                                    }
                                </div>
                            </div>
                        )
                    })}



                </div>
            </div>

        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    property: state.property,
    units: state.unit.units,
    contractList: state.contractList.contractList
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    gotoEditPage: (id: number) => dispatch(push(`/menu/properties/edit_property/${id}`)),
    deleteProperty: (id: number, country_id: number, country: string) => dispatch(deleteProperty(id, country_id, country)),
    getProperty: (id: number) => dispatch(getProperty(id)),
    createUnit: (id: number) => dispatch(push(`/menu/properties/unit/${id}`)),
    getUnit: (id: number) => (dispatch(getUnit(id))),
    gotoUnitEditPage: (id: number) => dispatch(push(`/menu/properties/edit_unit/${id}`)),
    deleteUnit: (id: number, property_id: number) => dispatch(deleteUnit(id, property_id)),
    getContractByStatus: (id: number) => dispatch(getContractByStatus(id)),
    reRentContract: (code: string) => dispatch(getContractByCode(code)),
    gotoReRentPage: (code: string) => dispatch(push(`/menu/re_rent_contract/${code}`))
})

export default connect(mapStateToProps, mapDispatchToProps)(Property)