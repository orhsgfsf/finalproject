import React from 'react'
import { ThunkDispatch, IRootState } from './store';
import { getUnit } from './unit/thunks';
import { connect } from 'react-redux';
import { IPropertyState } from './property/state';
import { IUnit } from './unit/state';
import { getProperty } from './property/thunks';
import { Button, Row, Col } from 'reactstrap';
import { getContractByUnitId } from './floorPlanUnit/thunks';
import { IUnitDetails } from './floorPlanUnit/state';
import moment from 'moment'
import { push } from 'connected-react-router';
import { getCoordinates } from './draggable/thunks';
import { ICoordinates } from './draggable/state';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDoorOpen, faHourglassHalf, faSearchDollar, faCircle } from '@fortawesome/free-solid-svg-icons';
import { Container } from '@material-ui/core';
import List from '@material-ui/core/List';

interface IFloorPlanDetailsProps {
    getUnit: (id: number) => void
    getProperty: (id: number) => void
    changeUnit: (id: number) => void
    gotoUploadPage: (id: number) => void
    gotoDrag: (id: number) => void
    getCoordinates: (id: number) => void

    coordinates: ICoordinates[]
    property: IPropertyState
    units: IUnit[]
    unitDetails: IUnitDetails

    match: {
        params: {
            id: string
        }
    }
}



class FloorPlanDetails extends React.Component<IFloorPlanDetailsProps>{
    private gotoUpload = () => {
        console.log(this.props.match.params.id)
        this.props.gotoUploadPage(parseInt(this.props.match.params.id))
    }
    private gotoDrag = () => {
        this.props.gotoDrag(parseInt(this.props.match.params.id))

    }


    private changeUnit(id: number) {

        this.props.changeUnit(id)
    }

    public componentDidMount() {
        this.props.getProperty(parseInt(this.props.match.params.id))
        this.props.getUnit(parseInt(this.props.match.params.id))
        console.log("I am going to get squre ")
        this.props.getCoordinates(parseInt(this.props.match.params.id))

    }

    public render() {
        console.log(this.props.property)
        console.log(this.props.units)
        console.log(this.props.unitDetails)
        console.log(this.props.coordinates)
        return (
            <div>
                <br></br>
                <div className='primary4'>
                    <Row>
                        <Col md="12">
                            <div>
                                <h4>Address: {this.props.property.address}, {this.props.property.country}</h4>
                                <br></br>
                                <h6><div>Please Select an Unit to check the details</div></h6>

                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="12">
                             <div className="unit-style">Unit:</div>
                            <div>{this.props.units.length !== 0 ? this.props.units.map(unit => <Button onClick={() => this.changeUnit(unit.id)}>{unit.unit}</Button>) : "No Unit!"}</div>
                        </Col>
                    </Row>
                   
                       
                   
                    <Row>
                        <Col md="12">
                            
                            <div>Floor Plan:</div>
                            <Row>
                            <Col md="3">
                            </Col>
                            <Col md="6">
                                <List>
                                    <div className="indicator">
                                        <div className="lastLogin3">
                                            Indicators<br></br>

                                            {<FontAwesomeIcon icon={faCircle} size="1x" color='red' />} Vacant/Exclamation <br></br>
                                            {<FontAwesomeIcon icon={faCircle} size="1x" color='yellow' />} Pending <br></br>
                                            {<FontAwesomeIcon icon={faCircle} size="1x" color='green' />} Rented<br></br>

                                        </div>
                                    </div>
                                </List>
                            </Col>
                            <Col md="3">
                            </Col>
                        </Row>
                            {
                                this.props.property.floor_plan ?
                                    <div>
                                        {


                                            <div>
                                                <div className="drag-block">
                                                    {this.props.coordinates.map(coordinate =>
                                                        coordinate.x !== 0 && coordinate.y !== 0 &&
                                                        <div>
                                                            {coordinate.contract_pending === true &&
                                                                <button
                                                                    className="drag-button yellow"
                                                                    onClick={() => this.changeUnit(coordinate.id)}
                                                                    style={{ left: coordinate.x, top: coordinate.y, width: coordinate.width, height: coordinate.height }}>
                                                                        {coordinate.unit}

                                                                </button>
                                                            }
                                                            {coordinate.contract_pending === false && coordinate.rental_status === false &&
                                                                <button
                                                                    className="drag-button red"
                                                                    onClick={() => this.changeUnit(coordinate.id)}
                                                                    style={{ left: coordinate.x, top: coordinate.y, width: coordinate.width, height: coordinate.height }}>
                                                                        {coordinate.unit}

                                                                </button>
                                                            }
                                                            {coordinate.rental_status === true &&
                                                                <button
                                                                    className="drag-button green-color"
                                                                    onClick={() => this.changeUnit(coordinate.id)}
                                                                    style={{ left: coordinate.x, top: coordinate.y, width: coordinate.width, height: coordinate.height }}>
                                                                        {coordinate.unit}

                                                                </button>
                                                            }

                                                        </div>
                                                    )}
                                                    <img className="image" src={this.props.property.floor_plan} alt="abc" />
                                                </div>
                                            </div>

                                        }



                                        <div className='space-between'>
                                            <Button onClick={() => this.gotoUpload()}>Re-Upload My Floor Plan</Button>

                                            <Button onClick={() => this.gotoDrag()}>Drag Area</Button>
                                        </div>
                                    </div>
                                    :
                                    <div>
                                        <div>No Floor Plan Yet!</div>
                                        <Button onClick={() => this.gotoUpload()}>Upload My Floor Plan</Button>
                                    </div>
                            }

                        </Col>
                    </Row>
                    <Row>
                        <Col md="8">
                            {this.props.unitDetails.unit !== "" ?
                                <div>
                                    <br></br>
                                    {
                                        this.props.unitDetails.rental_status === false && this.props.unitDetails.contract_pending === false &&
                                        <div className='pri2'>
                                            <Container>
                                                <Row>
                                                    <Col md="3" className="fontawesome">
                                                        <div>{<FontAwesomeIcon icon={faDoorOpen} size="3x" />}</div>
                                                    </Col>
                                                    <Col md="9" className="square">
                                                        <div>

                                                            <div>Unit: {this.props.unitDetails.unit}</div>
                                                            <div>Rental Status: Vacant</div>
                                                            <div>Gross Floor Area: {this.props.unitDetails.gross_floor_area} sq.ft</div>
                                                            <div>Saleable Floor Area: {this.props.unitDetails.saleable_area} sq.ft</div>
                                                            <div>No Contract Currently</div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Container>
                                        </div>
                                    }
                                    {
                                        this.props.unitDetails.rental_status === false && this.props.unitDetails.contract_pending === true &&
                                        <div className='pri2'>
                                            <Container>
                                                <Row>
                                                    <Col md="3" className="fontawesome">
                                                        <div>{<FontAwesomeIcon icon={faHourglassHalf} size="3x" />}</div>
                                                    </Col>
                                                    <Col md="9" className="square">
                                                        <div>
                                                            <div color="red">Work In Progress...</div>

                                                            <div>Unit: {this.props.unitDetails.unit}</div>
                                                            <div>Rental Status: Vacant</div>
                                                            <div>Gross Floor Area: {this.props.unitDetails.gross_floor_area} sq.ft</div>
                                                            <div>Saleable Floor Area: {this.props.unitDetails.saleable_area} sq.ft</div>
                                                            <div>Rent Price: {this.props.unitDetails.rent_price} {this.props.unitDetails.currency}</div>
                                                            <div>Rent Due Date: {moment(`2000-1-${this.props.unitDetails.rent_due_date}`).format("Do")} of Month</div>
                                                            <div>Starting Date: {moment(this.props.unitDetails.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                            <div>Expiry Date: {moment(this.props.unitDetails.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                            <div>Deposit: {this.props.unitDetails.deposit} {this.props.unitDetails.currency}</div>
                                                            <div>Grace Period: {this.props.unitDetails.grace_period} days</div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Container>
                                        </div>
                                    }
                                    {
                                        this.props.unitDetails.rental_status === true &&
                                        <div className='pri2'>
                                            <Container>
                                                <Row>
                                                    <Col md="3" className="fontawesome">
                                                        <div>{<FontAwesomeIcon icon={faDoorOpen} size="3x" />}</div>
                                                    </Col>
                                                    <Col md="9" className="square">
                                                        <div>
                                                            <div>Unit: {this.props.unitDetails.unit}</div>
                                                            <div>Rental Status: Rented</div>
                                                            <div>Gross Floor Area: {this.props.unitDetails.gross_floor_area} sq.ft</div>
                                                            <div>Saleable Floor Area: {this.props.unitDetails.saleable_area} sq.ft</div>
                                                            <div>Rent Price: {this.props.unitDetails.rent_price} {this.props.unitDetails.currency}</div>
                                                            <div>Rent Due Date: {moment(`2000-1-${this.props.unitDetails.rent_due_date}`).format("Do")} of Month</div>
                                                            <div>Starting Date: {moment(this.props.unitDetails.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                            <div>Expiry Date: {moment(this.props.unitDetails.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                            <div>Deposit: {this.props.unitDetails.deposit} {this.props.unitDetails.currency}</div>
                                                            <div>Grace Period: {this.props.unitDetails.grace_period} days</div>
                                                            <div>Tenant Name: {this.props.unitDetails.tenant_name}</div>
                                                            <div>Business Nature: {this.props.unitDetails.tenant_business_nature}</div>
                                                            <div>Contact Person: {this.props.unitDetails.contact_person ? this.props.unitDetails.contact_person : <div>No Contact Person</div>}</div>
                                                            <div>Contact Number: {this.props.unitDetails.contact_person ? this.props.unitDetails.contact_number : <div>{this.props.unitDetails.contact_number} (Tenant's Contact Number)</div>}</div>
                                                            {moment(this.props.unitDetails.contract_expiry_date).diff(moment(), 'days') < 0 ? <div> <div color="red">Expired!</div></div> : ""}
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Container>
                                        </div>
                                    }
                                </div>
                                :
                                ""
                            }

                        </Col>
                        <Col md="4">
                            {this.props.unitDetails.rental_status ?
                                <div>
                                    <br></br>
                                    <div className='pri2'>
                                        <div className='searchdollar'>{<FontAwesomeIcon icon={faSearchDollar} size="3x" />}</div>
                                        <br></br>
                                        <div>Price Per Feet: {(this.props.unitDetails.rent_price / this.props.unitDetails.gross_floor_area).toFixed(2)} {this.props.unitDetails.currency}/sq.ft</div>
                                        <div>Contract Period: {moment(this.props.unitDetails.contract_expiry_date).diff(moment(), 'days')} days</div>
                                    </div>
                                </div>
                                :
                               ""
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}


const mapStateToProps = (state: IRootState) => ({
    coordinates: state.coordinates.coordinates,
    property: state.property,
    units: state.unit.units,
    unitDetails: state.unitDetails.unitDetails
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getProperty: (id: number) => dispatch(getProperty(id)),

        getUnit: (id: number) => dispatch(getUnit(id)),

        changeUnit: (id: number) => dispatch(getContractByUnitId(id)),

        gotoUploadPage: (id: number) => dispatch(push(`/menu/floor_plan_upload/${id}`)),

        gotoDrag: (id: number) => dispatch(push(`/menu/floor_plan_draggable/${id}`)),

        getCoordinates: (id: number) => dispatch(getCoordinates(id))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(FloorPlanDetails)
