
import React from 'react'
import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import './property.css';
import { Col, Row, InputGroupAddon } from 'reactstrap';
import { getProperty } from './property/thunks';
import { addUnit } from './addUnitFail/thunks';
import { resetUnitFailed } from './addUnitFail/actions';



interface IUnitCreateProps {
    getProperty: (id: number) => void
    addUnit: (unitInformation:IUnitInformation) => void
    resetUnitFailed: () => void


    property: {
        address: string
        country: string
        propertyType: string
        gross_floor_area: number
        saleable_area: number
        government_rent: number
        management_fee: number
        currency: string
        rates: number
        reminder: string
        country_id: number
    }

    unit:{
        unit: string
        saleable_area: number
        gross_floor_area: number
        reminder: string
    }

    addUnitFail :{
        unit: string
        saleableArea: string
        grossFloorArea: string
        reminder: string 
    }

    match:{
        params:{
            id:number
        }
    }
}

export interface IAddUnitFailed {
    unit: string
    saleableArea: string
    grossFloorArea: string
    reminder: string
}

export interface IUnitCreateState {
    unit: string
    rentalStatus: boolean
    saleableArea: string 
    grossFloorArea: string 
    reminder: string
    property_id: number

    
    unitMsg:string
    saleableAreaMsg: string
    grossFloorAreaMsg: string 
}

export interface IUnitInformation {
   
    unit: string
    rental_status: boolean
    contract_pending: boolean
    saleable_area: number 
    gross_floor_area: number 
    reminder: string
    property_id: number
}

class UnitCreate extends React.Component<IUnitCreateProps, IUnitCreateState> {
    constructor(props: IUnitCreateProps) {
        super(props)

        this.state = {
            unit: "",
            rentalStatus: true,
          
            saleableArea: "",
            grossFloorArea: "",
            reminder: "",
            property_id: 0,


            unitMsg:"",
            saleableAreaMsg: "",
            grossFloorAreaMsg: ""

        }
    }

    private onAddClick = async () => {
        const unitInformation: IUnitInformation = {
            unit: this.state.unit,
            rental_status: true,
            contract_pending: false,
            saleable_area: parseInt(this.state.saleableArea),
            gross_floor_area: parseInt(this.state.grossFloorArea),
            reminder: this.state.reminder,
            property_id: this.props.match.params.id
        }

       
        if (this.state.unit.trim() === ""){
            await this.setState({
                unitMsg: "Unit can't be empty"
            })  
        }else{
            await this.setState({
                unitMsg: ""
            }) 
        }

        if(this.state.saleableArea.match(/\D/) !== null){
            await this.setState({
                saleableAreaMsg: "Saleable Area can only be integer"
            })      
        }
        if (this.state.saleableArea.match(/\D/) === null){
            await this.setState({
                saleableAreaMsg: ""
            }) 
        }
        if(this.state.saleableArea.trim() === ""){
            await this.setState({
                saleableAreaMsg: "Saleable Area can't be empty, if no Saleable area, input 0"
            })
        }


        if(this.state.grossFloorArea.match(/\D/) !== null){
            await this.setState({
                grossFloorAreaMsg: "Gross Floor Area can only be integer"
            })      
        }
        if (this.state.grossFloorArea.match(/\D/) === null){
            await this.setState({
                grossFloorAreaMsg: ""
            }) 
            console.log("WTF")
        }
        if(this.state.grossFloorArea.trim() === ""){
            await this.setState({
                grossFloorAreaMsg: "Gross Floor Area can't be empty, if no Saleable area, input 0"
            })
        }



        

        
        const {unitMsg, saleableAreaMsg, grossFloorAreaMsg} = this.state
        if(unitMsg === "" && saleableAreaMsg === "" && grossFloorAreaMsg === ""){
            this.props.addUnit(unitInformation)
        }
       
    }

    private onChange = (field: "unit" | "saleableArea" | "grossFloorArea" | "reminder", e: React.FormEvent<HTMLInputElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value

        this.setState(state)
    }

    public componentDidMount() {
        this.props.getProperty(this.props.match.params.id)
    }

    public componentWillUnmount() {
        this.props.resetUnitFailed()
    }

    public render() {
        console.log(this.props.property)

        return (
            <div className = 'primary'>
                <div> 
        
                    
                    <div>{this.props.property.address}, {this.props.property.country}</div>                  
                    <div>Types of Property: {this.props.property.propertyType}</div>
                    <div>Saleable Area: {this.props.property.saleable_area} sq.ft</div>
                    <div>Gross Floor Area: {this.props.property.gross_floor_area} sq.ft</div>
                    <div>Management Fee: {this.props.property.management_fee} {this.props.property.currency}</div>
                     <div>Government Rent: {this.props.property.government_rent} {this.props.property.currency}</div>
                     <div>Rates: {this.props.property.rates} {this.props.property.currency}</div>
        <div>Reminder: {this.props.property.reminder ? <div className="reminder">{this.props.property.reminder}</div> : <div>No any reminder</div>}</div></div>
                <Row>
                    <Col md="6">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Unit:</label>
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.state.unit}
                                onChange={this.onChange.bind(this, "unit")}
                            />
                        </div>
                        <div className="error-msg">{this.state.unitMsg !== "" && `*${this.state.unitMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">
                        
                            <label htmlFor="formGroupExampleInput">Saleable Area:</label>
                            <div className="form-group currency">
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.state.saleableArea || ""}
                                onChange={this.onChange.bind(this, "saleableArea")}
                            />
                             <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{this.state.saleableAreaMsg !== "" && `*${this.state.saleableAreaMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">
                    
                            <label htmlFor="formGroupExampleInput">Gross Floor Area:</label>
                            <div className="form-group currency">
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.state.grossFloorArea || ""}
                                onChange={this.onChange.bind(this, "grossFloorArea")}
                            />
                             <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{this.state.grossFloorAreaMsg !== "" && `*${this.state.grossFloorAreaMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Reminder:</label>
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.state.reminder}
                                onChange={this.onChange.bind(this, "reminder")}
                            />
                        </div>
                        <div className="error-msg">{this.props.addUnitFail.reminder ? `*${this.props.addUnitFail.reminder}` : ""}</div>
                    </Col>
                </Row>

                <Button onClick={this.onAddClick}>Add Unit</Button>

            </div>
        
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    property: state.property,
    addUnitFail: state.addUnitFail.addUnitFail
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getProperty: (id:number) => dispatch(getProperty(id)),
        addUnit: (unitInformation:IUnitInformation) => dispatch(addUnit(unitInformation)),
        resetUnitFailed:() => dispatch(resetUnitFailed())

    });
}

export default connect(mapStateToProps, mapDispatchToProps)(UnitCreate)