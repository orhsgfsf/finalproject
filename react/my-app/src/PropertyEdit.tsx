import React from 'react'


import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { Col, Row, InputGroupAddon } from 'reactstrap';
import { getCountriesList, getCurrenciesList, getPropertyTypesList } from './list/thunks';
import { updateProperty } from './addPropertiesFail/thunks';
import { getPropertyForEdit } from './editPropertyState/thunks';
import { changeValue } from './editPropertyState/actions';
import { resetPropertyFailed } from './addPropertiesFail/actions';



interface IPropertyEditProps {
    getCountriesList: () => void
    getPropertyTypesList: () => void
    getCurrenciesList: () => void
    change: (x: any) => void
    resetPropertyFailed: () => void



    countriesList: {
        country: string
    }[]

    propertyTypesList: {
        propertyType: string
    }[]

    currenciesList: {
        currency: string
    }[]

    addFail: {
        address: string
        country: string
        propertyType: string
        grossFloorArea: string
        saleableArea: string
        governmentRent: string
        managementFee: string
        currency: string
        rates: string
        deposit: string
        reminder: string
    }

    propertyForEdit: {
        address: string
        country: string
        propertyType: string
        gross_floor_area: number
        saleable_area: number
        government_rent: number
        management_fee: number
        currency: string
        rates: number
        deposit: number
        reminder: string
    }

    updateProperty: (id: number, propertyInformation: IPropertyInformation) => void
    getPropertyForEdit: (id: number) => void

    match: {
        params: {
            id: string
        }
    }

}

interface IPropertyEditState {
    addressMsg: string
    countryMsg: string
    propertyTypeMsg: string
    currencyMsg: string
    grossFloorAreaMsg: string
    saleableAreaMsg: string
    governmentRentMsg: string
    managementFeeMsg: string
    ratesMsg: string
    depositMsg: string
}



export interface IPropertyInformation {
    id: number
    address: string
    country: string
    propertyType: string
    grossFloorArea: number
    saleableArea: number
    governmentRent: number
    managementFee: number
    currency: string
    rates: number
    deposit: number
    reminder: string
}



class PropertyEdit extends React.Component<IPropertyEditProps, IPropertyEditState> {
    constructor(props: IPropertyEditProps) {
        super(props)
        this.state = {
            addressMsg: "",
            countryMsg: "",
            propertyTypeMsg: "",
            currencyMsg: "",
            grossFloorAreaMsg: "",
            saleableAreaMsg: "",
            governmentRentMsg: "",
            managementFeeMsg: "",
            ratesMsg: "",
            depositMsg: ""
        }


    }

    private onUpdateClick = async () => {
        const propertyInformation: IPropertyInformation = {
            id: parseInt(this.props.match.params.id),
            address: this.props.propertyForEdit.address,
            country: this.props.propertyForEdit.country,
            propertyType: this.props.propertyForEdit.propertyType,
            grossFloorArea: this.props.propertyForEdit.gross_floor_area,
            saleableArea: this.props.propertyForEdit.saleable_area,
            governmentRent: this.props.propertyForEdit.government_rent,
            managementFee: this.props.propertyForEdit.management_fee,
            currency: this.props.propertyForEdit.currency,
            rates: this.props.propertyForEdit.rates,
            deposit: this.props.propertyForEdit.deposit,
            reminder: this.props.propertyForEdit.reminder
        }
        console.log(propertyInformation)

        let stateMsg: any = {}

        if (this.props.propertyForEdit.address.trim() === "") {

            stateMsg.addressMsg = "Address can't be empty"

        } else {

            stateMsg.addressMsg = ""

        }

        if (this.props.propertyForEdit.country === "--Country--") {

            stateMsg.countryMsg = "Please select Country"

        } else {

            stateMsg.countryMsg = ""

        }

        if (this.props.propertyForEdit.propertyType === "--Property Type--") {

            stateMsg.propertyTypeMsg = "Please select Property Type"

        } else {

            stateMsg.propertyTypeMsg = ""

        }

        if (this.props.propertyForEdit.currency === "--Currency--") {

            stateMsg.currencyMsg = "Please select Currency"

        } else {

            stateMsg.currencyMsg = ""

        }

        if (this.props.propertyForEdit.saleable_area.toString().match(/\D/) !== null) {

            stateMsg.saleableAreaMsg = "Saleable Area can only be integer"

        }
        if (this.props.propertyForEdit.saleable_area.toString().match(/\D/) === null) {

            stateMsg.saleableAreaMsg = ""

        }
        if (this.props.propertyForEdit.saleable_area.toString().trim() === "") {

            stateMsg.saleableAreaMsg = "Saleable Area can't be empty, if no Saleable area, input 0"

        }

        if (this.props.propertyForEdit.gross_floor_area.toString().match(/\D/) !== null) {

            stateMsg.grossFloorAreaMsg = "Gross Floor Area can only be integer"

        }
        if (this.props.propertyForEdit.gross_floor_area.toString().match(/\D/) === null) {

            stateMsg.grossFloorAreaMsg = ""

        }
        if (this.props.propertyForEdit.gross_floor_area.toString().trim() === "") {

            stateMsg.grossFloorAreaMsg = "Gross Floor Area can't be empty, if no Gross Floor Area, input 0"
        }


        if (this.props.propertyForEdit.government_rent.toString().match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.governmentRentMsg = "Government Rent can only be numbers"

        }
        if (this.props.propertyForEdit.government_rent.toString().match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.governmentRentMsg = ""

        }
        if (this.props.propertyForEdit.government_rent.toString().trim() === "") {

            stateMsg.governmentRentMsg = "Government Rent can't be empty, if no Government Rent, input 0"

        }

        if (this.props.propertyForEdit.management_fee.toString().match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.managementFeeMsg = "Management Fee can only be numbers"

        }
        if (this.props.propertyForEdit.management_fee.toString().match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.managementFeeMsg = ""

        }
        if (this.props.propertyForEdit.management_fee.toString().trim() === "") {

            stateMsg.managementFeeMsg = "Management Fee can't be empty, if no Management Fee, input 0"

        }

        if (this.props.propertyForEdit.rates.toString().match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.ratesMsg = "Rates can only be numbers"

        }
        if (this.props.propertyForEdit.rates.toString().match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.ratesMsg = ""

        }
        if (this.props.propertyForEdit.rates.toString().trim() === "") {

            stateMsg.ratesMsg = "Rates can't be empty, if no Rates, input 0"

        }



        await this.setState({
            addressMsg: stateMsg.addressMsg,
            countryMsg: stateMsg.countryMsg,
            propertyTypeMsg: stateMsg.propertyTypeMsg,
            currencyMsg: stateMsg.currencyMsg,
            grossFloorAreaMsg: stateMsg.grossFloorAreaMsg,
            saleableAreaMsg: stateMsg.saleableAreaMsg,
            governmentRentMsg: stateMsg.governmentRentMsg,
            managementFeeMsg: stateMsg.managementFeeMsg,
            ratesMsg: stateMsg.ratesMsg,

        })

        const { addressMsg,

            propertyTypeMsg,
            currencyMsg,
            grossFloorAreaMsg,
            saleableAreaMsg,
            governmentRentMsg,
            managementFeeMsg,
            ratesMsg
        } = this.state
        console.log(this.state)
        if (addressMsg === "" &&

            propertyTypeMsg === "" &&
            currencyMsg === "" &&
            grossFloorAreaMsg === "" &&
            saleableAreaMsg === "" &&
            governmentRentMsg === "" &&
            managementFeeMsg === "" &&
            ratesMsg === "") {
            console.log("DDD")
            this.props.updateProperty(parseInt(this.props.match.params.id), propertyInformation)
        }



    }

    private onChange = (field: "address" | "gross_floor_area" | "saleable_area" | "government_rent" | "management_fee" | "rates" | "reminder" | "deposit", e: React.FormEvent<HTMLInputElement>) => {
        const x: any = {}
        x[field] = e.currentTarget.value
        console.log(x)
        this.props.change(x)
    }

    private onChangeCountry = (field: "country", e: React.FormEvent<HTMLSelectElement>) => {
        const x: any = {}
        x[field] = e.currentTarget.value
        this.props.change(x)
    }

    private onChangePropertyType = (field: "propertyType", e: React.FormEvent<HTMLSelectElement>) => {
        const x: any = {}
        x[field] = e.currentTarget.value
        this.props.change(x)
    }

    private onChangeCurrency = (field: "currency", e: React.FormEvent<HTMLSelectElement>) => {
        const x: any = {}
        x[field] = e.currentTarget.value
        this.props.change(x)
    }



    public componentDidMount() {

        this.props.getCountriesList()
        this.props.getPropertyTypesList()
        this.props.getCurrenciesList()
        this.props.getPropertyForEdit(parseInt(this.props.match.params.id))


    }

    public componentWillUnmount() {
        this.props.resetPropertyFailed()
    }

    public render() {
        console.log(this.props.propertyForEdit)
        const { addressMsg,

            propertyTypeMsg,
            currencyMsg,
            grossFloorAreaMsg,
            saleableAreaMsg,
            governmentRentMsg,
            managementFeeMsg,
            ratesMsg
        } = this.state
        return (
            <div className='primary'>

                <Row>
                    <Col md="12">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Address:</label>
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.props.propertyForEdit.address}
                                onChange={this.onChange.bind(this, "address")}
                            />
                        </div>
                        <div className="error-msg">{addressMsg && `*${addressMsg}`}</div>
                    </Col>

                    <Col md="6">
                        <div>
                            <div>Country: <b><u>{this.props.propertyForEdit.country}</u></b></div>
                            {/* <select className="browser-default custom-select" onChange={this.onChangeCountry.bind(this, "country")} value={this.props.propertyForEdit.country}>
                                    <option>--Country--</option>
                                    {this.props.countriesList.map((country, idx) => <option key={idx}>{country.country}</option>)}
                                </select> */}
                                
                        </div>
                        <br />
                        {/* <div className="error-msg">{this.props.addFail.country ? `*${this.props.addFail.country}` : ""}</div> */}
                   



                   
                        <div>
                            <label htmlFor="formGroupExampleInput">Property Type:</label>

                            <select className="browser-default custom-select" onChange={this.onChangePropertyType.bind(this, "propertyType")} value={this.props.propertyForEdit.propertyType}>
                                <option>--Property type--</option>
                                {this.props.propertyTypesList.map((propertyType, idx) => <option key={idx}>{propertyType.propertyType}</option>)}
                            </select>
                        </div>
                        <div className="error-msg">{propertyTypeMsg && `*${propertyTypeMsg}`}</div>
                    </Col>


                    <Col md="8">
                        <div className="form-group">

                            <label htmlFor="formGroupExampleInput">Gross Floor Area:</label>
                            <div className="form-group currency">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    // placeholder="Last Name"
                                    value={this.props.propertyForEdit.gross_floor_area}
                                    onChange={this.onChange.bind(this, "gross_floor_area")}
                                />
                                <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                            </div>
                        </div>
                        <div className="error-msg">{grossFloorAreaMsg && `*${grossFloorAreaMsg}`}</div>
                    </Col>


                    {/* 
                        <Col md="8">
                            Date of Birth:
                        <div className="xxxx">
                                <DatePicker className="date" onChange={this.onChangeDate} />
                            </div>
                            <div className="error-msg">{this.props.msg.date_of_birth ? `*${this.props.msg.date_of_birth}` : ""}</div>
                        </Col>
                        </Row> */}




                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Saleable Area:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.props.propertyForEdit.saleable_area}
                                onChange={this.onChange.bind(this, "saleable_area")}
                            />
                            <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{saleableAreaMsg && `*${saleableAreaMsg}`}</div>
                    </Col>




                    <Col md="8">
                        <div>
                            <label htmlFor="formGroupExampleInput">Currency:</label>

                            <select className="browser-default custom-select" onChange={this.onChangeCurrency.bind(this, "currency")} value={this.props.propertyForEdit.currency}>
                                <option>--Currency--</option>
                                {this.props.currenciesList.map((currency, idx) => <option key={idx}>{currency.currency}</option>)}
                            </select>
                        </div>
                        <div className="error-msg">{currencyMsg && `*${currencyMsg}`}</div>
                    </Col>





                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Government Rent:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.props.propertyForEdit.government_rent || ""}
                                onChange={this.onChange.bind(this, "government_rent")}
                            />
                            {this.props.propertyForEdit.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.props.propertyForEdit.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{governmentRentMsg && `*${governmentRentMsg}`}</div>
                    </Col>



                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Management Fee:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.props.propertyForEdit.management_fee || ""}
                                onChange={this.onChange.bind(this, "management_fee")}
                            />
                            {this.props.propertyForEdit.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.props.propertyForEdit.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{managementFeeMsg && `*${managementFeeMsg}`}</div>
                    </Col>



                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Rates:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.props.propertyForEdit.rates || ""}
                                onChange={this.onChange.bind(this, "rates")}
                            />
                            {this.props.propertyForEdit.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.props.propertyForEdit.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{ratesMsg && `*${ratesMsg}`}</div>
                    </Col>

                    {/* <Col md="12">
                                <label htmlFor="formGroupExampleInput">Deposit:</label>
                            </Col>
                            <Col md="8">
                                <div className="form-group currency">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.props.propertyForEdit.deposit || ""}
                                        onChange={this.onChange.bind(this, "deposit")}
                                    />
                                    {this.props.propertyForEdit.currency != "--Currency--" ? <InputGroupAddon addonType="append">{this.props.propertyForEdit.currency}</InputGroupAddon> : ""}
                                </div>
                                <div className="error-msg">{this.props.addFail.deposit ? `*${this.props.addFail.deposit}` : ""}</div>
                            </Col> */}





                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Reminder:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group">
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.props.propertyForEdit.reminder}
                                onChange={this.onChange.bind(this, "reminder")}
                            />
                        </div>
                    </Col>

                    <Col>
                        <Button className='btn-1' onClick={this.onUpdateClick}>Update</Button>
                    </Col>

                </Row>


            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    countriesList: state.countriesList.countriesList,
    currenciesList: state.currenciesList.currenciesList,
    propertyTypesList: state.propertyTypesList.propertyTypesList,
    propertyForEdit: state.propertyForEdit,
    addFail: state.addPropertyFail.addFail

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getCountriesList: () => dispatch(getCountriesList()),
        getPropertyTypesList: () => dispatch(getPropertyTypesList()),
        getCurrenciesList: () => dispatch(getCurrenciesList()),
        updateProperty: (id: number, propertyInformation: IPropertyInformation) => dispatch(updateProperty(id, propertyInformation)),
        getPropertyForEdit: (id: number) => dispatch(getPropertyForEdit(id)),
        change: (x: any) => dispatch(changeValue(x)),
        resetPropertyFailed: () => dispatch(resetPropertyFailed())

    });
}

export default connect(mapStateToProps, mapDispatchToProps)(PropertyEdit)