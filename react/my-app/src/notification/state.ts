export interface INotification {
   
        rent_due_date: Date | null
        rent_price: number
        currency: string
        unit: string
        full_address: string
        country: string
   
  
}

export interface INotificationState {
    notification:INotification[]
    msg:string
}

