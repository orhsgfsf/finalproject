import { INotificationState } from './state'
import { INotificationActions } from './actions'


const initialState: INotificationState = {
    notification: [],
    msg: ""
}

export function notificationReducer(state: INotificationState = initialState, action: INotificationActions):INotificationState{
    switch (action.type) {
        case "GET_BALANCE_SUCCESS":
            return {
                ...state,
                notification: action.notification
            }
        case "GET_BALANCE_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        default:
            return state
    }
}
