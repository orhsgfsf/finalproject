import { INotification } from "./state";


export function getBalanceToCheckSuccess(
    notification:INotification[]) {
    return {
        type: "GET_BALANCE_SUCCESS" as "GET_BALANCE_SUCCESS",
        notification
    }
}



type FAILED =  "GET_BALANCE_FAILED"

export function failed(type: FAILED, msg: string) {
    return {
        type,
        msg
    }
}

type notificationActionCreators =  typeof getBalanceToCheckSuccess | typeof failed

export type INotificationActions = ReturnType<notificationActionCreators>