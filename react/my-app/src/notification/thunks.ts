import { Dispatch } from "redux";
import { INotificationActions } from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";
import { failed, getBalanceToCheckSuccess } from "./actions";


export function getBalanceToCheck() {
    return async (dispatch: Dispatch<INotificationActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/home/dayDifference`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })

        const result = await res.json()
        console.log({"fdasdasdfd":result.data})
        console.log(result.data)
        if (res.status !== 200) {
            dispatch(failed("GET_BALANCE_FAILED", result.msg))
        } else {
            
            if (result.data.length > 0) {
                dispatch(getBalanceToCheckSuccess(result.data))
            }
        }
    }
}