import React from 'react'
import { ThunkDispatch } from './store'
import { Button } from 'reactstrap';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { pdf } from './contractDraft/thunks';
import {
  
    MDBCardBody,
    
  } from "mdbreact";
  import sakura from "./Image/sakura1.png";
// import sakura from "./Image/sakura1.png";

interface ISignContractSuccessProps {
    match: {
        params: {
            id: string
        }
    }


    viewContract: (code: string) => void
    downloadContract: (code: string) => void
}






class SignContractSuccess extends React.Component<ISignContractSuccessProps>{

    private viewContract = () => {
        this.props.viewContract(this.props.match.params.id)
    }

    private downloadContract = () => {
        this.props.downloadContract(this.props.match.params.id)
    }



    public render() {
        return (
            <div>

                <div className='Heros'> Congratulations! You had passed your contract to landlord  </div>
                <br></br>

                <div className='space-between'>
                    <Button color='red' className='delete' onClick={this.viewContract}>View Contract</Button>
                    <Button color='red' className='delete' onClick={this.downloadContract}>Download Contract</Button>
                </div>

                <MDBCardBody className="mx-4" >
                <div className='Hero'>
                  weVital
                <img className="sakura" src={sakura} alt="abc" />
                </div>
              </MDBCardBody>

               
            </div>


        )
    }
}


const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        viewContract: (code: string) => dispatch(push(`/contract_preview/${code}`)),
        downloadContract: (code: string) => dispatch(pdf(code))
    })

}

export default connect(null, mapDispatchToProps)(SignContractSuccess)