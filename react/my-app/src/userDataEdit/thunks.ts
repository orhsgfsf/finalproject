import { Dispatch } from "redux";
import { getUserDataAllSuccess, IUserDataEditActions } from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";

export function getUserDataAll(){
    console.log("IAMOFOOUEBG")
    return async (dispatch:Dispatch<IUserDataEditActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/user/userDataAll`,{
            headers:{
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json()
        if (res.status !== 200){
            console.error('error')
        }else{
            dispatch(getUserDataAllSuccess(result.data[0]))
        }
    }
}