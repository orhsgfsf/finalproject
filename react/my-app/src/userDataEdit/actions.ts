import  {IUserDataEditState} from './state'

export function getUserDataAllSuccess(userData:IUserDataEditState) {
    return {
        type: "GET_USER_DATA_ALL_SUCCESS" as "GET_USER_DATA_ALL_SUCCESS",
        userData
    }
}

export function changePersonalInfoSuccess(userData:IUserDataEditState){
    return {
        type: "CHANGE_USER_DATA_SUCCESS" as "CHANGE_USER_DATA_SUCCESS",
        userData
    }
}

export function changeImageSuccess (image:string){
    return {
        type: "CHANGE_IMAGE_SUCCESS" as "CHANGE_IMAGE_SUCCESS",
        image
    }
}
export function deleteIconSuccess (){
    return {
        type: "DELETE_ICON_SUCCESS" as "DELETE_ICON_SUCCESS"
    }
}

type userDataEditActionCreators = typeof getUserDataAllSuccess | typeof changePersonalInfoSuccess | typeof deleteIconSuccess |typeof changeImageSuccess
export type IUserDataEditActions = ReturnType<userDataEditActionCreators>