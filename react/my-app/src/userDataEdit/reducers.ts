import { IUserDataEditState } from './state'
import { IUserDataEditActions } from './actions'

const initialState: IUserDataEditState = {
    first_name: "",
    last_name: "",
    gender: "",
    date_of_birth: new Date(),
    contact_number: 0,
    company: "",
    profile_picture: ""

}

export function userDataEditReducer(state: IUserDataEditState = initialState, action: IUserDataEditActions): IUserDataEditState {
    switch (action.type) {
        case "GET_USER_DATA_ALL_SUCCESS":
            return {
                ...state,
                ...action.userData
            }
        case "CHANGE_USER_DATA_SUCCESS":
            console.log(action.userData)
            return {
                ...state,
                ...action.userData
            }
        case "CHANGE_IMAGE_SUCCESS":
            return {
                ...state,
                profile_picture: action.image
            }
            
        case "DELETE_ICON_SUCCESS":
            return {
                ...state,
                profile_picture: ""
            }

        default:
            return state
    }
}