export interface IUserDataEditState {
    first_name: string
    last_name: string
    gender:string
    date_of_birth: Date
    contact_number: number
    company: string
    profile_picture: string
}