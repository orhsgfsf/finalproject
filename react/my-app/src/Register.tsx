import * as React from 'react';
import { Alert } from 'reactstrap';
import { IRootState } from './store';
import { ThunkDispatch } from './store';
import { register } from './register/thunks'
import { connect } from 'react-redux';
import './login.css';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBModalFooter,
  MDBBtn,
  MDBInput
} from "mdbreact";
import { Link } from 'react-router-dom';


interface IRegisterState {
  username: string
  password: string
  confirmPassword: string
  email: string

}

interface IRegisterProps {
  register: (username: string, password: string, confirmPassword: string, email: string) => void
  msg: { username?: string, password?: string, email?: string }
}

class Register extends React.Component<IRegisterProps, IRegisterState> {
  constructor(props: IRegisterProps) {
    super(props)
    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: ""

    }
  }

  private onChange = (field: "username" | "password" | "confirmPassword" | "email", e: React.FormEvent<HTMLInputElement>) => {
    const state: any = {}
    state[field] = e.currentTarget.value

    this.setState(state)
  }

  private register = () => {
    const { username, password, confirmPassword, email } = this.state
    if (username && password && confirmPassword && email) {
      this.props.register(username, password, confirmPassword, email)
    }
  }



  public render() {
    console.log(this.props.msg)
    return (
      <div>
      <MDBContainer>
        <MDBRow>
          <MDBCol md="6">

          </MDBCol>
          <MDBCol md="6">
            
            <MDBCard>
              <MDBCardBody className="mx-4">
                <div className="text-center">
                  <h3 className="dark-grey-text mb-5">
                    <strong>Register</strong>
                  </h3>
                </div>
                <MDBInput
                  label="Username"
                  className="class"
                  group
                  type="text"
                  validate
                  error="wrong"
                  success="right"
                  value={this.state.username}
                  onChange={this.onChange.bind(this, "username")}
                />
                {this.props.msg.username ?
                  <Alert color="danger">
                    {this.props.msg.username}
                  </Alert> : ""}
                <MDBInput
                  label="Password"
                  className="class"
                  group
                  type="password"
                  validate
                  containerClass="mb-0"
                  value={this.state.password}
                  onChange={this.onChange.bind(this, "password")}
                />
                {this.props.msg.password ?
                  <Alert color="danger">
                    {this.props.msg.password}
                  </Alert> : ""}

                <MDBInput
                  label="Confirm Password"
                  className="class"
                  group
                  type="password"
                  validate
                  containerClass="mb-0"
                  value={this.state.confirmPassword}
                  onChange={this.onChange.bind(this, "confirmPassword")}

                />
                <MDBInput
                  label="Email"
                  className="class"
                  group
                  type="email"
                  validate
                  containerClass="mb-0"
                  value={this.state.email}
                  onChange={this.onChange.bind(this, "email")}
                />
                {this.props.msg.email ?
                  <Alert color="danger">
                    {this.props.msg.email}
                  </Alert> : ""}
                {/* <p className="font-small blue-text d-flex justify-content-end pb-3">
                    Forgot
                    <a href="#!" className="blue-text ml-1">
    
                      Password?
                    </a>
                  </p> */}
                <div className="text-center mb-3">
                  <MDBBtn
                    type="button"
                    gradient="blue"
                    rounded
                    className="btn-block z-depth-1a"
                    onClick={this.register}
                  >

                    Register
                    </MDBBtn>

                </div>



              </MDBCardBody>
              <MDBModalFooter className="mx-5 pt-3 mb-1">
                <Link to="/login" className="blue-text ml-1">Back</Link>
              </MDBModalFooter>
            </MDBCard>
          </MDBCol>
        </MDBRow>
       </MDBContainer>
      </div>
    );


  };
}

// return (


//     <Form>
//         <FormGroup>
//             <Label for="username">Username</Label>
//             <Input type="email"
//             value={this.state.username}
//             onChange={this.onChange.bind(this, "username")}
//             placeholder="Username"
//             />
//         </FormGroup>
//         <FormGroup>
//             <Label for="password">Password</Label>
//             <Input type="password"
//             value={this.state.password}
//             onChange={this.onChange.bind(this, "password")}
//             placeholder="Password"
//             />
//         </FormGroup>
//         <FormGroup>
//             <ReactFacebookLogin
//             appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
//             fields="name, email, picture"
//             onClick={this.fBOnClick}
//             callback={this.fBCallback}
//             />
//         </FormGroup>
//         {this.props.msg ? <Alert color="danger">
//             {this.props.msg}
//         </Alert>:""
//     }
//     <Button onClick={this.login}>Login</Button>
//     </Form>
// )



const mapStateToProps = (state: IRootState) => ({
  msg: state.register.msg
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  register: (username: string, password: string, confirmPassword: string, email: string) => dispatch(register(username, password, confirmPassword, email))
})

export default connect(mapStateToProps, mapDispatchToProps)(Register)