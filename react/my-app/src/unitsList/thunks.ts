import { Dispatch } from "redux";

import { CallHistoryMethodAction } from "connected-react-router";
import { getUnitsListSuccess, IUnitsListActions } from "./actions";



export function getUnitsList(){
    return async (dispatch:Dispatch<IUnitsListActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/unitsList`, {           
            headers: {               
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },           
        })
        const result = await res.json()
        console.log(result.data)
        if(res.status !== 200){
            return
        }else{
            dispatch(getUnitsListSuccess(result.data))
        }
    }
}