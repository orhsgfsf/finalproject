export interface IUnitsList{
    
id:number
unit:string
address:string
country:string
first_name:string
last_name:string
br_number?:number
id_number:number
contract_pending:boolean
rental_status:boolean
}

export interface IUnitsListState{
    unitsList: IUnitsList[]
}