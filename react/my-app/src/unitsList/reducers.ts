import { IUnitsListState } from './state'
import { IUnitsListActions } from './actions'

const initialState: IUnitsListState = {
    unitsList: []
}

export function unitsListReducer(state: IUnitsListState = initialState, action: IUnitsListActions): IUnitsListState {
    switch (action.type) {
        case "GET_UNITS_LIST_SUCCESS":
            return {
                ...state,
                unitsList: action.unitsList
            }

        default:
            return state
    }
}