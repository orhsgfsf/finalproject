import {IUnitsList} from './state'

export function getUnitsListSuccess(unitsList:IUnitsList[]){
    return {
        type: "GET_UNITS_LIST_SUCCESS" as "GET_UNITS_LIST_SUCCESS",
        unitsList
    }
}

type unitsListActionCreators = typeof getUnitsListSuccess
export type IUnitsListActions = ReturnType<unitsListActionCreators>
