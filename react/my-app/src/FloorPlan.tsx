import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import FloorPlanList from './FloorPlanList';
import FloorPlanDetails from './FloorPlanDetails';
import { Switch, Route } from 'react-router-dom';
export default class FloorPlan extends React.Component{

   

    public render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col md="4">
                            <FloorPlanList />
                        </Col>
                   
                   
                        <Col md="8">
                            <Switch>
                                <Route path="/menu/floor_plan/details/:id" component={FloorPlanDetails}/>
                               
                            </Switch>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}