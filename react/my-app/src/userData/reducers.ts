import { IUserDataActions } from './actions'
import { IUserDataState } from './state'

const initialState = {
    userData: {
        login_time: null,
        profile_picture: "",
        first_name: "",
        gender: ""
    },
    msg: ""
}

export function userDataReducer(state: IUserDataState = initialState, action: IUserDataActions) {
    switch (action.type) {
        case "GET_USER_DATA_SUCCESS":
            return {
                ...state,
                userData: action.userData
            }
        case "GET_USER_DATA_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        case "UPDATE_PERSONAL_INFO_SUCCESS": 
        return {
            ...state,
            userData:{
                ...state.userData,
                profile_picture: action.personalInfo.profile_picture,
                first_name: action.personalInfo.first_name
            }
        }
       
        default:
            return state
    }
}