export interface IUserDataState {
    userData: {
        login_time?: string | null
        first_name: string | null
        profile_picture: string 
        gender:string
    }
    msg: string
}



