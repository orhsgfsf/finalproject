import { Dispatch } from "redux";
import { IUserDataActions, failed, getUserDataSuccess, updatePersonalInfoSuccess } from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";
import { IUpdatePersonalInfo } from "../PersonalInfoEdit";
import { ThunkDispatch } from "../store";

export function getUserData() {
    return async (dispatch: Dispatch<IUserDataActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/home/userData`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json()

        if (res.status !== 200) {
            dispatch(failed("GET_USER_DATA_FAILED", result.msg))
        } else {
            console.log("get user status success!!")
            console.log(result.data)
            dispatch(getUserDataSuccess(result.data))
        }
    }
}

export function updatePersonalInfo(personalInfo:IUpdatePersonalInfo) {
    return async (dispatch: Dispatch<IUserDataActions | CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/user/userData`, {
            method: "PATCH",
            headers:{
                "Content-type":"application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(personalInfo)
        })
        await res.json()
        if(res.status !== 200){
            console.error("This is an error")
        }else{
            dispatch(updatePersonalInfoSuccess(personalInfo))
         
        }

        
    }
}

export function updateLoginTime(loginTime:string){
    return async(dispatch:ThunkDispatch) => {
        await fetch (`${process.env.REACT_APP_API_SERVER}/user/loginTime`,{
            method: "PATCH",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({login_time:loginTime})
        })
    }
}

