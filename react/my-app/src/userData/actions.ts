import { IUpdatePersonalInfo } from "../PersonalInfoEdit";

export function getUserDataSuccess(userData: {
    login_time?: string | null
    first_name: string
    profile_picture: string
    gender: string
}) {
    return {
        type: "GET_USER_DATA_SUCCESS" as "GET_USER_DATA_SUCCESS",
        userData
    }
}

export function updatePersonalInfoSuccess (personalInfo:IUpdatePersonalInfo){
    return {
        type: "UPDATE_PERSONAL_INFO_SUCCESS" as "UPDATE_PERSONAL_INFO_SUCCESS",
        personalInfo
    }
}





type FAILED = "GET_USER_DATA_FAILED" 

export function failed(type: FAILED, msg: string) {
    return {
        type,
        msg
    }
}
type userDataActionCreators = typeof getUserDataSuccess | typeof updatePersonalInfoSuccess |typeof failed

export type IUserDataActions = ReturnType<userDataActionCreators>