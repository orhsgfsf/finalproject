
export interface ISingleUnitState{

        id:number
        unit:string
        rental_status: boolean      
        saleable_area:number 
        gross_floor_area:number 
        reminder:string 
        property_id:number
}



