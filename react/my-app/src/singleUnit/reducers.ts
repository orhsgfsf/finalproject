import { ISingleUnitState } from './state'
import { ISingleUnitActions } from './actions';


const initialState: ISingleUnitState = {
  
        id:0,
        unit:"",
        rental_status: true,     
        saleable_area: 0,
        gross_floor_area: 0,
        reminder:"" ,
        property_id:0
    
    
       
}

export function singleUnitReducer(state: ISingleUnitState = initialState, action: ISingleUnitActions):ISingleUnitState {
    switch (action.type) {
        case "GET_UNIT_BY_ID_SUCCESS":
            console.log(action.unit)
            return {
                ...state,
                ...action.unit
            }

        case "CHANGE_VALUE":
            return {
                ...state,
                ...action.state
            }
        default:
            return state
    }
}