import { ISingleUnitState } from "./state";

export function getUnitByUnitIdSuccess(unit: ISingleUnitState){
    return {
        type: "GET_UNIT_BY_ID_SUCCESS" as "GET_UNIT_BY_ID_SUCCESS",
        unit
    }
}

export function changeUnitValue(state:any){
    return{
        type: "CHANGE_VALUE" as "CHANGE_VALUE",
        state
    }
}



type singleUnitActionCreators = typeof getUnitByUnitIdSuccess | typeof changeUnitValue
export type ISingleUnitActions = ReturnType<singleUnitActionCreators>