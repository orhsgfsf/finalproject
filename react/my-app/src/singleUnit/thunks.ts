import { Dispatch } from "redux";

import { CallHistoryMethodAction } from "connected-react-router";


import {  ISingleUnitActions, getUnitByUnitIdSuccess } from "./actions";





export function getUnitByUnitId(id:number){
    return async (dispatch:Dispatch<ISingleUnitActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/unitByUnitId`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        const result = await res.json()
        console.log("getunitbyid")
        console.log(result.data)
        if(res.status !== 200){
            return
        }else{
            dispatch(getUnitByUnitIdSuccess(result.data[0]))
        }
    }
}

export function getUnitList(){
    return async (dispatch:Dispatch<ISingleUnitActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/unitsList`, {           
            headers: {               
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },           
        })
        const result = await res.json()
        console.log(result.data)
        if(res.status !== 200){
            return
        }else{
            dispatch(getUnitByUnitIdSuccess(result.data))
        }
    }
}