import React from 'react'
import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import './property.css';
import { connect } from 'react-redux';
import { Col, Row, InputGroupAddon } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { getCountriesList, getCurrenciesList, getPropertyTypesList } from './list/thunks';
import { addProperty } from './addPropertiesFail/thunks';
import { resetPropertyFailed } from './addPropertiesFail/actions';



interface ICreatePropertyProps {
    getCountriesList: () => void
    getPropertyTypesList: () => void
    getCurrenciesList: () => void
    addProperty: (propertyInformation: IPropertyInformation) => void
    resetPropertyFailed: () => void

    countriesList: {
        country: string
    }[]

    propertyTypesList: {
        propertyType: string
    }[]

    currenciesList: {
        currency: string
    }[]

    addFail: {
        address: string
        country: string
        propertyType: string
        grossFloorArea: string
        saleableArea: string
        governmentRent: string
        managementFee: string
        currency: string
        rates: string
        deposit: string
        reminder: string
    }

}

interface ICreatePropertyState {
    address: string
    country: string
    propertyType: string
    grossFloorArea: string
    saleableArea: string
    governmentRent: string
    managementFee: string
    currency: string
    rates: string
    deposit: string
    reminder: string


    addressMsg: string
    countryMsg: string
    propertyTypeMsg: string
    currencyMsg: string
    grossFloorAreaMsg: string
    saleableAreaMsg: string
    governmentRentMsg: string
    managementFeeMsg: string
    ratesMsg: string
    depositMsg: string



}

export interface IPropertyInformation {
    address: string
    country: string
    propertyType: string
    grossFloorArea: number
    saleableArea: number | null
    governmentRent: number | null
    managementFee: number | null
    currency: string
    rates: number | null
    deposit: number | null
    reminder: string
}



class CreateProperty extends React.Component<ICreatePropertyProps, ICreatePropertyState> {
    constructor(props: ICreatePropertyProps) {
        super(props)

        this.state = {
            address: "",
            country: "--Country--",
            propertyType: "--Property Type--",
            grossFloorArea: "",
            saleableArea: "",
            governmentRent: "",
            managementFee: "",
            currency: "--Currency--",
            rates: "",
            deposit: "",
            reminder: "",



            addressMsg: "",
            countryMsg: "",
            propertyTypeMsg: "",
            currencyMsg: "",
            grossFloorAreaMsg: "",
            saleableAreaMsg: "",
            governmentRentMsg: "",
            managementFeeMsg: "",
            ratesMsg: "",
            depositMsg: ""


        }
    }

    private onAddClick = async () => {
        const propertyInformation: IPropertyInformation = {
            address: this.state.address,
            country: this.state.country,
            propertyType: this.state.propertyType,
            grossFloorArea: parseInt(this.state.grossFloorArea),
            saleableArea: parseInt(this.state.saleableArea),
            governmentRent: parseInt(this.state.governmentRent),
            managementFee: parseInt(this.state.managementFee),
            currency: this.state.currency,
            rates: parseInt(this.state.rates),
            deposit: parseInt(this.state.deposit),
            reminder: this.state.reminder
        }

        console.log(this.state.grossFloorArea)

        let stateMsg: any = {}

        if (this.state.address.trim() === "") {

            stateMsg.addressMsg = "Address can't be empty"

        } else {

            stateMsg.addressMsg = ""

        }

        if (this.state.country === "--Country--") {

            stateMsg.countryMsg = "Please select Country"

        } else {

            stateMsg.countryMsg = ""

        }

        if (this.state.propertyType === "--Property Type--") {

            stateMsg.propertyTypeMsg = "Please select Property Type"

        } else {

            stateMsg.propertyTypeMsg = ""

        }

        if (this.state.currency === "--Currency--") {

            stateMsg.currencyMsg = "Please select Currency"

        } else {

            stateMsg.currencyMsg = ""

        }

        if (this.state.saleableArea.match(/\D/) !== null) {

            stateMsg.saleableAreaMsg = "Saleable Area can only be integer"

        }

        if (this.state.saleableArea.match(/\D/) === null) {

            stateMsg.saleableAreaMsg = ""

        }
        if (this.state.saleableArea.trim() === "") {

            stateMsg.saleableAreaMsg = "Saleable Area can't be empty, if no Saleable area, input 0"

        }


        if (this.state.grossFloorArea.match(/\D/) !== null) {

            stateMsg.grossFloorAreaMsg = "Gross Floor Area can only be integer"

        }
        if (this.state.grossFloorArea.match(/\D/) === null) {

            stateMsg.grossFloorAreaMsg = ""

        }
        if (this.state.grossFloorArea.trim() === "") {

            stateMsg.grossFloorAreaMsg = "Gross Floor Area can't be empty, if no Gross Floor Area, input 0"

        }

        if (this.state.governmentRent.match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.governmentRentMsg = "Government Rent can only be numbers"

        }
        if (this.state.governmentRent.match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.governmentRentMsg = ""

        }
        if (this.state.governmentRent.trim() === "") {

            stateMsg.governmentRentMsg = "Government Rent can't be empty, if no Government Rent, input 0"

        }

        if (this.state.managementFee.match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.managementFeeMsg = "Management Fee can only be numbers"

        }
        if (this.state.managementFee.match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.managementFeeMsg = ""

        }
        if (this.state.managementFee.trim() === "") {

            stateMsg.managementFeeMsg = "Management Fee can't be empty, if no Management Fee, input 0"

        }

        if (this.state.rates.match(/^[0-9]+(\.[0-9]+)?$/) === null) {

            stateMsg.ratesMsg = "Rates can only be numbers"

        }
        if (this.state.rates.match(/^[0-9]+(\.[0-9]+)?$/) !== null) {

            stateMsg.ratesMsg = ""

        }
        if (this.state.rates.trim() === "") {

            stateMsg.ratesMsg = "Rates can't be empty, if no Rates, input 0"

        }

        if (this.state.deposit.match(/\D/) !== null) {

            stateMsg.depositMsg = "Saleable Area can only be numbers"

        }
        if (this.state.deposit.match(/\D/) === null) {

            stateMsg.depositMsg = ""

        }
        if (this.state.deposit.trim() === "") {

            stateMsg.depositMsg = "Saleable Area can't be empty, if no Saleable area, input 0"

        }


        this.setState({
            addressMsg: stateMsg.addressMsg,
            countryMsg: stateMsg.countryMsg,
            propertyTypeMsg: stateMsg.propertyTypeMsg,
            currencyMsg: stateMsg.currencyMsg,
            grossFloorAreaMsg: stateMsg.grossFloorAreaMsg,
            saleableAreaMsg: stateMsg.saleableAreaMsg,
            governmentRentMsg: stateMsg.governmentRentMsg,
            managementFeeMsg: stateMsg.managementFeeMsg,
            ratesMsg: stateMsg.ratesMsg,

        })



        if (stateMsg.addressMsg === "" &&
            stateMsg.countryMsg === "" &&
            stateMsg.propertyTypeMsg === "" &&
            stateMsg.currencyMsg === "" &&
            stateMsg.grossFloorAreaMsg === "" &&
            stateMsg.saleableAreaMsg === "" &&
            stateMsg.governmentRentMsg === "" &&
            stateMsg.managementFeeMsg === "" &&
            stateMsg.ratesMsg === "") {

            this.props.addProperty(propertyInformation)
        }



    }

    private onChange = (field: "address" | "grossFloorArea" | "saleableArea" | "governmentRent" | "managementFee" | "rates" | "reminder" | "deposit", e: React.FormEvent<HTMLInputElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value

        this.setState(state)
    }

    private onChangeCountry = (field: "country", e: React.FormEvent<HTMLSelectElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }

    private onChangePropertyType = (field: "propertyType", e: React.FormEvent<HTMLSelectElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value

        this.setState(state)
    }

    private onChangeCurrency = (field: "currency", e: React.FormEvent<HTMLSelectElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }



    public componentDidMount() {
        this.props.getCountriesList()
        this.props.getPropertyTypesList()
        this.props.getCurrenciesList()
        console.log("DIDMOUNT")

    }

    public componentWillUnmount() {
        this.props.resetPropertyFailed()
    }

    public render() {

        const { addressMsg,
            countryMsg,
            propertyTypeMsg,
            currencyMsg,
            grossFloorAreaMsg,
            saleableAreaMsg,
            governmentRentMsg,
            managementFeeMsg,
            ratesMsg
        } = this.state
        return (
            <div className='primary'>

                <Row>
                    <Col md="12">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Address:</label>
                            <input
                                type="textarea"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.state.address}
                                onChange={this.onChange.bind(this, "address")}
                            />
                        </div>
                        <div className="error-msg">{addressMsg && `*${addressMsg}`}</div>
                    </Col>

                    <Col md="6">
                        <div>
                            <label htmlFor="formGroupExampleInput">Country:</label>

                            <select className="browser-default custom-select" onChange={this.onChangeCountry.bind(this, "country")} value={this.state.country}>
                                <option>--Country--</option>
                                {this.props.countriesList.map((country, idx) => <option key={idx}>{country.country}</option>)}
                            </select>
                        </div>
                        <div className="error-msg">{countryMsg && `*${countryMsg}`}</div>



                        <div>
                            <label htmlFor="formGroupExampleInput">Property Type:</label>

                            <select className="browser-default custom-select" onChange={this.onChangePropertyType.bind(this, "propertyType")} value={this.state.propertyType}>
                                <option>--Property Type--</option>
                                {this.props.propertyTypesList.map((propertyType, idx) => <option key={idx}>{propertyType.propertyType}</option>)}
                            </select>
                        </div>
                        <div className="error-msg">{propertyTypeMsg && `*${propertyTypeMsg}`}</div>
                    </Col>

                    <Col md="8">
                        <div className="form-group">

                            <label htmlFor="formGroupExampleInput">Gross Floor Area:</label>
                            <div className="form-group currency">
                                <input
                                    type="text"
                                    className="form-control1"
                                    id="formGroupExampleInput"
                                    // placeholder="Last Name"
                                    value={this.state.grossFloorArea || ""}
                                    onChange={this.onChange.bind(this, "grossFloorArea")}
                                />
                                <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                            </div>
                        </div>
                        <div className="error-msg">{grossFloorAreaMsg && `*${grossFloorAreaMsg}`}</div>
                    </Col>


                    {/* 
                        <Col md="8">
                            Date of Birth:
                        <div className="xxxx">
                                <DatePicker className="date" onChange={this.onChangeDate} />
                            </div>
                            <div className="error-msg">{this.props.msg.date_of_birth ? `*${this.props.msg.date_of_birth}` : ""}</div>
                        </Col>
                        </Row> */}




                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Saleable Area:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.state.saleableArea || ""}
                                onChange={this.onChange.bind(this, "saleableArea")}
                            />
                            <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{saleableAreaMsg && `*${saleableAreaMsg}`}</div>
                    </Col>




                    <Col md="8">
                        <div>
                            <label htmlFor="formGroupExampleInput">Currency:</label>

                            <select className="browser-default custom-select" onChange={this.onChangeCurrency.bind(this, "currency")} value={this.state.currency}>
                                <option>--Currency--</option>
                                {this.props.currenciesList.map((currency, idx) => <option key={idx}>{currency.currency}</option>)}
                            </select>
                        </div>
                        <div className="error-msg">{currencyMsg && `*${currencyMsg}`}</div>
                    </Col>





                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Government Rent:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.state.governmentRent || ""}
                                onChange={this.onChange.bind(this, "governmentRent")}
                            />
                            {this.state.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.state.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{governmentRentMsg && `*${governmentRentMsg}`}</div>
                    </Col>



                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Management Fee:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.state.managementFee || ""}
                                onChange={this.onChange.bind(this, "managementFee")}
                            />
                            {this.state.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.state.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{managementFeeMsg && `*${managementFeeMsg}`}</div>
                    </Col>



                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Rates:</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group currency">
                            <input
                                type="text"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.state.rates || ""}
                                onChange={this.onChange.bind(this, "rates")}
                            />
                            {this.state.currency !== "--Currency--" ? <InputGroupAddon addonType="append">{this.state.currency}</InputGroupAddon> : ""}
                        </div>
                        <div className="error-msg">{ratesMsg && `*${ratesMsg}`}</div>
                    </Col>

                    {/* <Col md="12">
                                <label htmlFor="formGroupExampleInput">Deposit:</label>
                            </Col>
                            <Col md="8">
                                <div className="form-group currency">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.deposit || ""}
                                        onChange={this.onChange.bind(this, "deposit")}
                                    />
                                    {this.state.currency != "--Currency--" ? <InputGroupAddon addonType="append">{this.state.currency}</InputGroupAddon> : ""}
                                </div>
                                <div className="error-msg">{this.props.addFail.deposit ? `*${this.props.addFail.deposit}` : ""}</div>
                            </Col>
           */}


                    <Col md="12">
                        <label htmlFor="formGroupExampleInput">Reminder(Optional):</label>
                    </Col>
                    <Col md="8">
                        <div className="form-group">
                            <input
                                type="textarea"
                                className="form-control1"
                                id="formGroupExampleInput"
                                // placeholder="Last Name"
                                value={this.state.reminder}
                                onChange={this.onChange.bind(this, "reminder")}
                            />
                        </div>
                    </Col>

                    <Col>
                        <Button className="btn-1" onClick={this.onAddClick}>{<FontAwesomeIcon icon={faCheck} size="3x" />}</Button>
                    </Col>

                </Row>


            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    countriesList: state.countriesList.countriesList,
    currenciesList: state.currenciesList.currenciesList,
    propertyTypesList: state.propertyTypesList.propertyTypesList,
    addFail: state.addPropertyFail.addFail

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getCountriesList: () => dispatch(getCountriesList()),
        getPropertyTypesList: () => dispatch(getPropertyTypesList()),
        getCurrenciesList: () => dispatch(getCurrenciesList()),
        addProperty: (propertyInformation: IPropertyInformation) => dispatch(addProperty(propertyInformation)),
        resetPropertyFailed: () => dispatch(resetPropertyFailed())

    });
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProperty)