import React from 'react'
import { Row, Col, Container, InputGroupAddon, Button } from 'reactstrap';
import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
import { IRootState, ThunkDispatch } from './store';
import { getUnitsList } from './unitsList/thunks';
import { IUnitsList } from './unitsList/state';
import { connect } from 'react-redux';
import { IContractDetailsState } from './contract/state';
import { getUnitForContract } from './contract/thunks';
import { addContractDraft } from './contractDraft/thunks';
import { push } from 'connected-react-router';
// import { createBrowserHistory } from 'history'



const SignatureCanvas = require('react-signature-canvas').default

interface IContractProps{
    getUnitsList: () => void
    getUnitForContract: (id:number) => void
    addContractDraft: (contractInformation:IContractLandLord) => void
    goBack: () => void
    history:any

    unitsList:IUnitsList[]
    contractDetails:IContractDetailsState
}


interface IContractState{
    
    
    rentDueDate: string | null
    startGracePeriod: any
    endGracePeriod: any
    tenantName: string
    startContract: any
    contactPerson: string | null
    contactNumber: string
    businessNature: string
    endContract: any
    rentPrice: string | null
    deposit: string | null
    keys: string | null
    IdOrBrNumber: string
    gracePeriod: number



   
}

interface IContractLandLord{
    rentDueDate: number | null
    gracePeriod: number | null
    startContract: string
    endContract: string
    rentPrice: number
    deposit: number 
    keys: number
    IdOrBrNumber: string
    currency:string
    landlordSignature: string
    code:string
    unitId:number
    landlordName:string
    startGracePeriod: string
    endGracePeriod: string
}

class ContractCreate extends React.Component <IContractProps,IContractState> {
    constructor(props:IContractProps) {
        super(props);
        
        this.state = {
           
            rentDueDate: "",
            startGracePeriod: "",
            endGracePeriod: "",
            tenantName: "",
            contactPerson: "",
            contactNumber: "",
            businessNature: "",
            startContract: "",
            endContract: "",
            rentPrice: "",
            deposit: "",
            keys: "",
            IdOrBrNumber: "",
            gracePeriod: 0
          
        };
       
      }
    
      sigPadForLandLord:any = {}
      sigPad2:any = {}
      clearSignature = () => {       
        this.sigPadForLandLord.clear()
      }
    
    //   clear2 = () => {
    //     console.log(this.sigPad2.toDataURL('image/png'))
    //   }

    
    private onChange = (field: "rentDueDate" | "rentPrice" | "deposit" | "keys" |  "IdOrBrNumber"| "tenantName", e: React.FormEvent<HTMLInputElement>) => {
        let state:any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }
    private handleChange = (field:"startDate" | "startGracePeriod" | "endGracePeriod"| "startContract" | "endContract", date:any) => {
        let state:any = {}
        state[field] = date
        this.setState(
          state
        );
    }
    private onChangeAddress = (e:React.FormEvent<HTMLSelectElement>) => {  
        
        if (!isNaN(parseInt(e.currentTarget.value))){
            this.props.getUnitForContract(parseInt(e.currentTarget.value))

        }else{
            console.log(e.currentTarget.value)
        }
        
    }

    private onConfirmByLandLord = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 50; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
       
        const contractInformation:IContractLandLord = {
           
            rentDueDate: (this.state.rentDueDate != null ? parseInt(this.state.rentDueDate): 0),
            gracePeriod: moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days'),
            startContract: this.state.startContract,
            endContract: this.state.endContract,
            rentPrice: (this.state.rentPrice != null ? parseInt(this.state.rentPrice): 0),
            deposit: (this.state.deposit != null ? parseInt(this.state.deposit): 0),
            keys: (this.state.keys != null ? parseInt(this.state.keys): 0),
            IdOrBrNumber: this.state.IdOrBrNumber,
            landlordName: `${this.props.contractDetails.first_name} ${this.props.contractDetails.last_name}`,
            currency: this.props.contractDetails.currency,
            landlordSignature: this.sigPadForLandLord.toDataURL('image/png'),
            code: result,
            unitId: this.props.contractDetails.id,
            startGracePeriod: this.state.startGracePeriod,
            endGracePeriod: this.state.endGracePeriod
        }
        console.log(contractInformation)
        this.props.addContractDraft(contractInformation)
    }

    private back = () => {
        this.props.goBack()
    }

    
    
    public componentDidMount(){
        this.props.getUnitsList()
        
    }
    

    public render() {
        console.log(this.props.unitsList)
       console.log(this.props.contractDetails)
        return (
            <div>
            
                <br></br>
                <div className='primary'>
                <h6>Disclaimer</h6>
                  <br></br>

The material contained in this web site has been produced by we-vital in accordance with its current practices and policies and with the benefit of information currently available to it, and all reasonable efforts have been made to ensure the accuracy of the contents of the pages of the web site at the time of preparation. we-vital regularly reviews the web site and where appropriate will update pages to reflect changed circumstances.<br></br>
<br></br>
Notwithstanding all efforts made by we-vital to ensure the accuracy of the web site, no responsibility or liability is accepted by we-vital in respect of any use or reference to the web site in connection with any matter or action taken following such use or reference or for any inaccuracies, omissions, mis-statements or errors in the said material, or for any economic or other loss which may be directly or indirectly sustained by any visitor to the web site or other person who obtains access to the material on the web site. <br></br>
<br></br>
The material on this web site is for general information only and nothing in this web site contains professional advice or an offer for sale or any binding commitment upon we-vital in relation to the availability of property or the description of property. All visitors should take advice from a suitably qualified professional in relation to any specific query or problem that they may have or in relation to any property included in this web site, and we-vital takes no responsibility for any loss caused as a result of reading the contents of this web site.<br></br>
<br></br>
No claims, actions or legal proceedings in connection with this web site brought by any visitor or other person having reference to the material on this web site will be entertained by we-vital.<br></br>
<br></br>

                <Container className="contract-background">
                    <br></br>
                    <div>
                        <h2>Tenancy Agreement</h2>
                    <br></br>
                        {/* <div>
                            <Nav>
                                <NavItem>
                                    <NavLink href="#">Active Date</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#">Terms</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#ttt">Schedule I</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#ttt">Signature</NavLink>
                                </NavItem>
                            </Nav>
                        </div> */}
                        <div className="contract">
                            
                            <Row>
                                <Col md="12">
                                    <div>
                                        <h3>Activate Date</h3>
                                        The Landlord and the Tenant as more particularly declared in <b>Schedule I</b> that an Agreement made the date of  {moment(this.state.startContract).format("YYYY-MM-DD") !== "Invalid date" ? <b><u>{moment(this.state.startContract).format("YYYY-MM-DD")}</u></b> : "______________"}
                                       <br />
                                        {/* 業主和租客<b>(詳細資料詳列於附表一)</b>於<DatePicker selected={this.state.startDate} onChange={this.handleChange.bind(this, "startDate")}  dateFormat="yyyy/MM/dd" value={this.state.startDate}/>訂立本合約。 */}
                                               
                                    </div>
                                </Col>
                            </Row>
                            
                            <Row>
                                <Col md="12">
                                    <div>
                                        <br></br>
                                        <div>
                                    <h3>Terms</h3>
                                    <b>The Landlord and the Tenant shall follow the Terms and the Rent of the Premises as more particularly
                                described in Schedule I.
                                Both , the Landlord and the Tenant, agree to observe and perform the terms and
                                conditions as
                                below:</b>
                                <br />
                                {/* <b>業主和租客雙方須遵從詳列於<b>附表一</b>的租賃期限及租賃金額租入和租出該物業, 並同意遵守和履行下列條款。</b> */}
                                <br />
                                <b>1) </b>The Tenant shall pay to the Landlord the Rent in advance on the {this.state.rentDueDate ? <b><u>{this.state.rentDueDate}</u></b> : "_________"} day of each and every calendar month during the Term.</div>
                                {/* <b>1) </b>租客須在租期內每月份第<input type="text" value={this.state.rentDueDate} onChange={this.onChange.bind(this, "rentDueDate")}/>天上期繳付指定的租金予業主。 */}
                                <br />
                                <b>2) </b>The Tenant shall not make any additions or alteration to the Premises unless the Landlords acquire and accept the written consent.
                                {/* <b>2) </b>除非業主獲悉並同意書面通知, 不然租客不能對該物業作任何加建或改動。 */}
                                <br />

                                <b>3) </b>The Tenant shall not assign, transfer, sublet or part with the possession of the Premises or any part thereof to any other person. This tenancy shall be personal to the Tenant named herein. 
                                <br />                       
                                {/* <b>3) </b>租客不得轉讓、轉租或分租該物業或其他任何部分或將該物業或其他任何部分的佔用權讓予任何其他人等。此租約權益將為租客個人擁有。 */}
                                <br />
                                <b>4) </b>The Tenant shall comply with all ordinances, regulations and rules of Hong Kong and
                            shall
                            observe and perform the covenants, terms and conditions of the Deed fo Mutual Covenant
                            and
                            Sub-Deed of Mutual Covenant (if any) relating to the Premsises. The Tenant shall not
                            contravene any negative or restrictive covenants contained in the Government Lease(s)
                            under
                            which the Premises are held from the Government.
                            <br />
                                {/* <b>4) </b>租客須遵守香港一干法律條例和規則及該物業所屬的大廈有關的公契內的條款。租客亦不可違反屬該物業地段內的官批地契上的任何制約性條款。 */}
                                <br />
                                <b>5) </b>The Tenant shall pay to the Landlord the Security Deposit set out in Schedule I for
                            the due observance and performance of the terms and
                            conditions herein contained and on his part to be observed and performed. If the Rent and/
                            or any changes payable by the Tenant hereunder or any part
                            thereof shall be unpaid for seven (7) days after the same shall become payable (whether
                            legally demanded or not) or if the Tenant shall commit a breach of
                            any of the terms and conditions herein contained, it shall be lawful for the Landlord at any
                            time thereafter to rn-enter the Premises whereupon this Agreement shall
                            absolutely determine and the Landlord may deduct any loss or damage suffered by the Landlord
                            as a result of the Tenant's breach from the Security.
                            Deposit without prejudice to any other right of action or any remedy of the Landlord in
                            respect of such breach of the Tenant.
                            <br />
                                {/* <b>5) </b>租客須交予業主保証 (金額如附表一所列) 作為保証租客遵守及履行此租約上租客所需遵守及履行的條款的按金。若租客拖欠根據本合約需要支付的租金及/ 或其他款項超過七天
                            (無論有否以法律行動追討)
                            或若租客違反此合約內任何條款，業主可合法收回該物業而此租約將會被終止;
                            業主可從保証金內扣除因租客違約而業主所受的損失，而此項權利將不會影響業主因租客違約而可採取的其他合法行動的權利。
                            <br /> */}
                                <b>6) </b>the Tenant shall have rights to hold and enjoy the Premises during the Term
                            without any interruption by the Landlord.
                            <br />
                                {/* <b>6) </b>業主不可在租約期內干擾租客享用該物業。 */}
                                <br />
                                <b>7 </b>)The Landlord shall keep and maintain the structural parts of the Premises.
                            Provided that the Landlord's liability
                            shall not be incurred unless and until written notice of any defect or want of repair
                            has been given by the Tenant to the Landlord and the Landlord shall have failed to take
                            reasonable steps to repair and remedy the same after the lapse of a reasonable time from
                            the date of service of such notice.
                            <br />
                                {/* <b>7) </b>業主須保養及適當維修該物業內各主要結構部分。唯業主須在收到租客的書面要求後才會有責任在合理時限內將有關損壞維修妥當。 */}
                                <br />
                                <b>8) </b>The Stamp Duty payable on this Agreement in duplicate shall be shared in equal
                            shares by both the Landlord and the Tenant.
                            <br />
                                {/* <b>8) </b>此合約為一式兩份, 業主和租客共同負責印花稅一半費用。 */}
                                <br />
                                <b>9) </b>The Landlord and the Tenant agree to be bound by the additional terms and
                                
                            conditions contained in Appendix (if any)
                            <br />
                                {/* <b>9) </b>業主及租客雙方同意遵守附加條款 (如有的話) 。 */}
                                <br />
                                <b>10) </b>The Tenant shall not use or permit to be used the Premises or any part there of for {this.props.contractDetails.propertyType ? this.props.contractDetails.propertyType : "_________"} purpose only.
                                <br /> 
                                {/* <b>10) </b> 租客除將該物業作XXXXX用途外, 不可將該物業或其任何部分作其他用途。 */}
                                <br /> 
                                <b>11) </b>The Tenant shall be entitled to a rent free period of <b>{isNaN(moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')) ? "_________" : moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')}</b> {moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days') > 1 ? "days" : "day"} from {this.state.startGracePeriod? <b><u>{moment(this.state.startGracePeriod).format("YYYY-MM-DD")}</u></b> : "_________"} to {this.state.endGracePeriod? <b><u>{moment(this.state.endGracePeriod).format("YYYY-MM-DD")}</u></b> : "_________"} (both days inclusive) provided that the
                            Tenant shall be responsible for the charges of water, electricity, gas, telephone and other
                            outgoings payable in respect of Premises during such rent free period.
                            <br />
                            {/* <b>11) </b>租客可享有<b>{moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')}</b> 天免租期由
                            <DatePicker selected={this.state.startGracePeriod} onChange={this.handleChange.bind(this, "startGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.startGracePeriod}/> 
                                至
                            <DatePicker selected={this.state.endGracePeriod} onChange={this.handleChange.bind(this, "endGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.endGracePeriod}/> 

                            (包括首尾兩天)
                            但租客仍需負責繳付免租期內一切水、電、煤氣及電話費用及其他一切雜費支出。
                            除XXXXXXX用途外, 不可將該物業或其任何部分作其他用途。 */}
                            <br />
                            <b>12) </b>If there is any contradiction between the English version and the Chinese version
                            in this Agreement, the Chinese version shall prevail.
                            <br />
                            </div>
                            {/* <b>12) </b>此合約內私英文文本與中文文本存有差異時, 將以中文文本為準。 */}
                                </Col>
                            </Row>
                            <br></br>
                            <h3>Schedule I</h3>
                                    <br></br>
                            <div className='pri-sign'>
                            <Row>
                                <Col md="12">
                                    
                                    
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Premise</InputGroupAddon>
                                    <select className="browser-default custom-select" onChange={this.onChangeAddress.bind(null)}>
                                        <option>--- Please select address ---</option>

                                        {this.props.unitsList.map(unit => (unit.contract_pending === false && unit.rental_status === false) && <option value={unit.id}>{unit.unit}, {unit.address}, {unit.country}</option>)}
                                    </select>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Full Name of the Landlord</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={`${this.props.contractDetails.first_name} ${this.props.contractDetails.last_name}`} disabled={true}/>
                                    </div>
                                    <br />
                                    {/* <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Date of Birth</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={moment(this.props.contractDetails.date_of_birth).format('DD/MM/YYYY')} disabled={true}/>
                                    </div> */}
                                    <br />
                                    {/* <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Full Name of the Tenant</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "tenantName")} value={this.state.tenantName}/>
                                    </div>
                                    <br /> */}
                                   
                                </Col>
                                <Col md="6">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Rent Due Date</InputGroupAddon>
                                    <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.rentDueDate || ""} onChange={this.onChange.bind(this, "rentDueDate")}/>
                                    </div>
                                    </div>
                                    <br />
                                </Col>
                                <Col md="12">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Term (both days inclusive):</InputGroupAddon>
                                    <InputGroupAddon addonType="prepend">From</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.startContract} onChange={this.handleChange.bind(this, "startContract")}  dateFormat="yyyy/MM/dd" value={this.state.startContract}/>
                                    <InputGroupAddon addonType="prepend">To</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.endContract} onChange={this.handleChange.bind(this, "endContract")}  dateFormat="yyyy/MM/dd" value={this.state.endContract}/>
                                    </div>
                                    </div>
                                    <br />
                                </Col>
                                <Col md="12">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Grace Period:</InputGroupAddon>
                                    <InputGroupAddon addonType="prepend">From</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.startGracePeriod} onChange={this.handleChange.bind(this, "startGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.startGracePeriod}/>
                                    <InputGroupAddon addonType="prepend">To</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.endGracePeriod} onChange={this.handleChange.bind(this, "endGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.endGracePeriod}/>
                                    </div>
                                    </div>
                                </Col>
                                <Col md="8">
                                    <div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Rent Per Month</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "rentPrice" )} value={this.state.rentPrice || ""}/>
                                    {this.props.contractDetails.currency ? <InputGroupAddon addonType="append">{this.props.contractDetails.currency}</InputGroupAddon> : ""}
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Management Fees</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDetails.management_fee} disabled={true}/>
                                        {this.props.contractDetails.currency ? <InputGroupAddon addonType="append">{this.props.contractDetails.currency}</InputGroupAddon> : ""}
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Government Rents</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDetails.government_rent} disabled={true}/>
                                        {this.props.contractDetails.currency ? <InputGroupAddon addonType="append">{this.props.contractDetails.currency}</InputGroupAddon> : ""}
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Government Rates</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDetails.rates} disabled={true}/>
                                        {this.props.contractDetails.currency ? <InputGroupAddon addonType="append">{this.props.contractDetails.currency}</InputGroupAddon> : ""}
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Security Deposit</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "deposit" )} value={this.state.deposit || ""}/>
                                        {this.props.contractDetails.currency ? <InputGroupAddon addonType="append">{this.props.contractDetails.currency}</InputGroupAddon> : ""}
                                    </div>
                                    </div>
                                </Col>
                                <Col md="12">
                                    <div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Number of key(s) of the Premises by the Tenant:</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "keys" )} value={this.state.keys || ""}/>
                                        <InputGroupAddon addonType="append">key(s)</InputGroupAddon>
                                    </div>
                                  
                                    </div>
                                </Col>
                            </Row>
                            </div>

                            <Row>
                                <Col md="12">
                                    <br></br>
                                    <div>
                                    <div>
                                        <h3>Signature</h3>
                                    </div>
                                    <div>                          
                                        <b><div className="contract-text"> Confirmed and Accepted all the terms and Conditions contained herein by the Landlord:</div></b>
                                            <br />
                                        {/* JSX Library */}
                                        <div className="landlord-area">
                                        <div className="signature-pad">
                                       
                                            <Row>
                                                <Col md="12">
                                                    <div className="pri-sign">
                                                    <div className="contract-text">
                                                        Signature of LandLord:
                                                    </div>
                                                    <br />
                                                    <div className="landlord-area">
                                                        <SignatureCanvas canvasProps={{width: 425, height: 250, className: "sigCanvas"}}
                                                        ref={(ref:any) => { return this.sigPadForLandLord = ref }}/>
                                                    </div>
                                                    <div className="landlord-area">
                                                    <Button onClick={this.clearSignature}>Re-Sign</Button>
                                                    </div>
                                                    <br />                         
                                                        ID/BR number of LandLord:
                                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "IdOrBrNumber")} value={this.state.IdOrBrNumber}/>
                                                        </div>
                                                </Col>
                                        
                                            </Row>
                                            </div>
                                        </div>
                                        <div className="landlord-area">
                                            <Button onClick={this.onConfirmByLandLord}>Confirm By LandLord</Button>
                                            <Button onClick={() => this.props.history.goBack()}>Back</Button>
                                        </div>
                                    </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Container>
                </div>
                    </div>
                    )
                }
}


const mapStateToProps = (state:IRootState) => {
    return ({
        contractDetails: state.contractDetails,
        unitsList: state.unitsList.unitsList
    })
}

const mapDispatchToProps = (dispatch:ThunkDispatch) => {
    return ({

        getUnitsList: () => dispatch(getUnitsList()),
        getUnitForContract: (id:number) => dispatch((getUnitForContract(id))),
        addContractDraft: (contractInformation:IContractLandLord) => dispatch(addContractDraft(contractInformation)),
        goBack: () => dispatch(push('/menu/contract/contract_interface'))
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ContractCreate)