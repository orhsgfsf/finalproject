import React from 'react'


import {  Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { Col, Row, Container, Input, Label } from 'reactstrap';
// import { getCountriesList, getCurrenciesList, getPropertyTypesList } from './list/thunks';
// import {  updateProperty } from './addPropertiesFail/thunks';
// import { getPropertyForEdit } from './editPropertyState/thunks';
// import { changeValue } from './editPropertyState/actions';
// import { resetPropertyFailed } from './addPropertiesFail/actions';
import { IUserDataEditState } from './userDataEdit/state';
import { getUserDataAll } from './userDataEdit/thunks';
import { addImage } from './addImage/thunks';
import DatePicker from "react-datepicker";
import moment from 'moment'
import { changePersonalInfoSuccess, deleteIconSuccess, changeImageSuccess } from './userDataEdit/actions';
import { updatePersonalInfo } from './userData/thunks';
import { push } from 'connected-react-router';



interface IPersonalInfoEditProps {
    getUserDataAll: () => void
   
    change: (props:any) => void
    changeImageSuccess: (x:string) => void
    updatePersonalInfo: (result:IUpdatePersonalInfo) => void
    deleteIcon: () => void
    addImage: (file: File) => void

    goBack: () => void
    userDataEdit:IUserDataEditState
   

    msg: {
        first_name: string,
        last_name: string,
        gender: string,
        id_number: string,
        date_of_birth: string,
        district_number: string,
        contact_number: string
    }
}





export interface IUpdatePersonalInfo {
    first_name: string,
    last_name: string,
    gender: string,
   
    date_of_birth: Date,
   
    contact_number: number,
    company: string,
   
    profile_picture: string

}

export interface IPersonalInfoEditState{
    file: File | null
}



class PersonalInfoEdit extends React.Component<IPersonalInfoEditProps,IPersonalInfoEditState> {
    constructor(props:IPersonalInfoEditProps){
        super(props)
        this.state = {
            file : null
        }
    }

   

    private reduxChange = (field: "first_name" | "last_name" | "contact_number" | "company" , e: React.FormEvent<HTMLInputElement>) => {
        const props: any = {}
        props[field] = e.currentTarget.value
        console.log(props)
         this.props.change(props)
    }
    private reduxChangeGender = (field: "gender", e: React.FormEvent<HTMLSelectElement>) => {
        const props: any = {}
        props[field] = e.currentTarget.value

        console.log(props)
        this.props.change(props)
    }

    private handleFileOnChange = (files: FileList) => {
        
        console.log(files)
        if (files.length !== 0){
            const x = URL.createObjectURL(files[0])
            console.log(x)
       
            this.props.changeImageSuccess(x)
            this.setState({
                file:files[0]
            })
           
        }
       
    } 

    private reduxChangeDate = (field: "date_of_birth", date:any) => {
       
        const props: any = {}
        props[field] = date
        console.log(props)
        this.props.change(props)
    }

    private back = () => {
        this.props.goBack()
    }

    private onConfirm = () => {
        const result:IUpdatePersonalInfo = {
            first_name: this.props.userDataEdit.first_name,
            last_name: this.props.userDataEdit.last_name,
            gender: this.props.userDataEdit.gender,
           
            date_of_birth: this.props.userDataEdit.date_of_birth,
           
            contact_number: this.props.userDataEdit.contact_number,
            company: this.props.userDataEdit.company,
           
            profile_picture: this.props.userDataEdit.profile_picture 
        }
       
        console.log(result)
        this.props.updatePersonalInfo(result)
        if (this.state.file !== null){
            this.props.addImage(this.state.file)
        }

        this.props.goBack()

    }

    private deleteIcon = () => {

        this.props.deleteIcon()
    }


    public componentDidMount() {
       
        this.props.getUserDataAll()
    }

   

    public render() {
        
        return (
            <div>
                <br></br>
            <div className='primary'>
            <Container>
                <Row>
                    <Col md="4">
                        <div className="icon-zone">
                            <div className="icon-block">
                                {this.props.userDataEdit.profile_picture === "" || this.props.userDataEdit.profile_picture === null ? <div>Profile Picture</div> : <img className="icon" src={this.props.userDataEdit.profile_picture} alt=''/>}
                            </div>
                        </div>
                        <br></br>
                        <div className="upload-click">
                           
                            <Input type='file' id={"file"} name="pic" style={{ display: 'none' }} onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />

                            <div className="upload-image-button="><Label htmlFor={"file"} className="upload">Upload Profile Picture</Label></div>
                        </div>
                        <br></br>
                            <Button onClick={this.deleteIcon}>Empty profile picture</Button>
                    </Col>


                    <Col md="8">
                        <Row>
                        <Col md="6">
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">First Name:</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    // placeholder="First name"
                                    value={ this.props.userDataEdit.first_name }
                                    onChange={this.reduxChange.bind(this, "first_name")}
                                />
                            </div>
                            <div className="error-msg">{this.props.msg.first_name ? `*${this.props.msg.first_name}` : ""}</div>
                        </Col>
                        <Col md="6">
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">Last Name:</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    // placeholder="Last Name"
                                    value={this.props.userDataEdit.last_name }
                                    onChange={this.reduxChange.bind(this, "last_name")}
                                />
                            </div>
                            <div className="error-msg">{this.props.msg.last_name ? `*${this.props.msg.last_name}` : ""}</div>
                        </Col>
                        </Row>
                        <Row>
                        <Col md="6">
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">Gender:</label>

                                <select className="browser-default custom-select" onChange={this.reduxChangeGender.bind(this, "gender")} value={this.props.userDataEdit.gender }>
                                    <option>--Gender--</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div className="error-msg">{this.props.msg.gender ? `*${this.props.msg.gender}` : ""}</div>
                        </Col>
                        </Row>
                   
                        <Row>
                        <Col md="8">
                       
                        <label htmlFor="formGroupExampleInput">Date of Birth:</label>
                        <div className="form-group">
                        <DatePicker className="form-control"  onChange={this.reduxChangeDate.bind(this, "date_of_birth")}  dateFormat="yyyy/MM/dd" value={moment(this.props.userDataEdit.date_of_birth).format("DD/MM/YYYY")}/>
                            </div>
                            <div className="error-msg">{this.props.msg.date_of_birth ? `*${this.props.msg.date_of_birth}` : ""}</div>
                        </Col>
                        </Row>

                        <Row>
                        <Col md="8">
                            <label htmlFor="formGroupExampleInput">Contact Number:</label>
                       
                 
                      
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    // placeholder="Last Name"
                                    value={this.props.userDataEdit.contact_number }
                                    onChange={this.reduxChange.bind(this, "contact_number") }
                                />
                            </div>
                            <div className="error-msg">{this.props.msg.contact_number ? `*${this.props.msg.contact_number}` : ""}</div>
                        </Col>
                        </Row>


                        <Row>

                        <Col md="12">
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">Company(Optional):</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="formGroupExampleInput"
                                    // placeholder="Last Name"
                                    value={this.props.userDataEdit.company}
                                    onChange={this.reduxChange.bind(this, "company")}
                                />
                            </div>
                        </Col>
                        </Row>
                      
                        <Row>
                        <Col md="12" className="personal-information-buttons">
                            <Button onClick={this.onConfirm}>Change Information</Button>
                            <Button onClick={this.back}>Back</Button>
                        </Col>
                        </Row>
                      

                    </Col>

                </Row>
            </Container >
        </div >
        </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    userDataEdit: state.userDataEdit,
    msg: state.personalInfo.msg

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({      
        getUserDataAll: () =>  dispatch(getUserDataAll()),
        // addImage: (file: File, name:string) => dispatch(addImage(file, name)),
        change: (props:any) => dispatch(changePersonalInfoSuccess(props)),
        changeImageSuccess: (x:string) => dispatch(changeImageSuccess(x)),
        updatePersonalInfo: (result:IUpdatePersonalInfo) => dispatch(updatePersonalInfo(result)),
        deleteIcon: () => dispatch(deleteIconSuccess()),
        addImage: (file: File) => dispatch(addImage(file)),
        goBack: () => dispatch(push('/menu/properties'))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfoEdit)