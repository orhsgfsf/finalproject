import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import moment from 'moment';
import ContractProperties from './ContractProperties';
import ContractList from './ContractList';
import { Switch, Route } from 'react-router-dom';
import ContractInterface from './ContractInterface';


export default class Contracts extends React.Component {
    public render() {
        return (
            <div>
                <Container>
                    <Row>

                    </Row>
                    <br></br>
                    <Row>
                        <Col md="12" className="primary1">

                        Date: {moment().format("DD-MM-YYYY")}

                        </Col>
                    </Row>
                    <br></br>
                    <Row>
                        <Col md="4">
                            <ContractProperties />
                        </Col>
                        <Col md="8">
                            <Switch>
                                <Route path="/menu/contract/contract_interface" component={ContractInterface} />
                                <Route path="/menu/contract/contract_list/:id" component={ContractList} />
                            </Switch>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}