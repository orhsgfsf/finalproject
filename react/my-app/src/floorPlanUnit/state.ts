export interface IFloorPlanUnitState {
    unitDetails:{
    rent_due_date: number
    grace_period: number
    contract_starting_date: Date
    contract_expiry_date: Date
    rent_price: number
    deposit: number
    grace_period_expiry_date: Date
    grace_period_starting_date: Date
    unit: string
    unit_id: number
    gross_floor_area: number
    tenant_name: string | null
    contact_person: string | null
    contact_number: number | null
    rental_status: boolean
    contract_pending: boolean
    saleable_area:number
    currency:string
    tenant_business_nature:string
    }

}

export interface IUnitDetails {
    rent_due_date: number
    grace_period: number
    contract_starting_date: Date
    contract_expiry_date: Date
    rent_price: number
    deposit: number
    grace_period_expiry_date: Date
    grace_period_starting_date: Date
    unit: string
    unit_id: number
    gross_floor_area: number
    saleable_area:number
    tenant_name: string | null
    contact_person: string | null
    contact_number: number | null
    rental_status: boolean
    contract_pending: boolean
    currency:string
    tenant_business_nature:string
}