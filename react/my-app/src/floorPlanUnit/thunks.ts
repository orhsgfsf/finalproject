import { getContractByUnitIdSuccess } from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";
import { Dispatch } from 'redux'
import { IFloorPlanUnitActions } from "./actions";


export function getContractByUnitId(id:number) {
    console.log(id)
    return async (dispatch:Dispatch<IFloorPlanUnitActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/toGetContractByUnitId`, {
            method:"POST",
            headers: {
                "Content-type": "application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({id})
        
        })
        const result = await res.json()
        console.log({unitContract:result.data})
        dispatch(getContractByUnitIdSuccess(result.data[0]))
       
    }
}