import {IFloorPlanUnitState} from './state'
import {IFloorPlanUnitActions} from './actions'

const initialState:IFloorPlanUnitState = {
    unitDetails:{
    rent_due_date: 0,
    grace_period: 0,
    contract_starting_date: new Date(),
    contract_expiry_date: new Date(),
    rent_price: 0,
    deposit: 0,
    grace_period_expiry_date: new Date(),
    grace_period_starting_date: new Date(),
    unit: "",
    unit_id: 0,
    gross_floor_area: 0,
    saleable_area:0,
    tenant_name: "",
    contact_person: "",
    contact_number: 0,
    rental_status: false,
    contract_pending: false,
    currency:"",
    tenant_business_nature:""
    }
}
export function floorPlanUnitReducer(state:IFloorPlanUnitState = initialState, action:IFloorPlanUnitActions): IFloorPlanUnitState{
    switch(action.type){
        case "GET_FLOOR_PLAN_UNIT_SUCCESS":
            return {
                ...state,
                unitDetails: action.unitDetails
            }
        case "CLEAR_VALUE":
            return {
                ...state,
                unitDetails: initialState.unitDetails
            }
        default:
            return state
    }
}
