import {IUnitDetails} from './state'

export function getContractByUnitIdSuccess (unitDetails:IUnitDetails){
    return {
        type: "GET_FLOOR_PLAN_UNIT_SUCCESS" as "GET_FLOOR_PLAN_UNIT_SUCCESS",
        unitDetails
    }
}

export function clearDetailsSuccess (){
    return {
        type: "CLEAR_VALUE" as "CLEAR_VALUE"
    }
}

type floorPlanUnitActionCreators = typeof getContractByUnitIdSuccess | typeof clearDetailsSuccess
export type IFloorPlanUnitActions = ReturnType<floorPlanUnitActionCreators>