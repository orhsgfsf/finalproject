import { ICountriesList, ICurrenciesList, IPropertyTypesList, IBusinessNaturesList } from "./state";

export function getCountriesListSuccess(countriesList:ICountriesList[]){
    return {
        type: "GET_COUNTRIES_LIST_SUCCESS" as "GET_COUNTRIES_LIST_SUCCESS",
        countriesList
    }
}

export function getCurrenciesListSuccess(currenciesList:ICurrenciesList[]){
    return {
        type: "GET_CURRENCIES_LIST_SUCCESS" as "GET_CURRENCIES_LIST_SUCCESS",
        currenciesList
    }
}

export function getPropertyTypesListSuccess (propertyTypesList:IPropertyTypesList[]){
    return {
        type: "GET_PROPERTY_TYPES_LIST_SUCCESS" as "GET_PROPERTY_TYPES_LIST_SUCCESS",
        propertyTypesList
    }
}

export function getBusinessNaturesListSuccess (businessNaturesList:IBusinessNaturesList[]){
    console.log(businessNaturesList)
    return {
        type: "GET_BUSINESS_NATURES_LIST_SUCCESS" as "GET_BUSINESS_NATURES_LIST_SUCCESS",
        businessNaturesList
    }
}


type FAILED = "GET_COUNTRIES_LIST_FAILED" | "GET_CURRENCIES_LIST_FAILED" | "GET_PROPERTY_TYPES_LIST_FAILED"

export function failed(type:FAILED, msg:string){
    return{
        type,
        msg
    }
}

type countriesListActionCreators = typeof getCountriesListSuccess | typeof failed
export type ICountriesListActions = ReturnType<countriesListActionCreators>

type currenciesListActionCreators = typeof getCurrenciesListSuccess | typeof failed 
export type ICurrenciesListActions = ReturnType<currenciesListActionCreators>

type propertyTypesListActionCreators = typeof getPropertyTypesListSuccess | typeof failed
export type IPropertyTypesListActions = ReturnType<propertyTypesListActionCreators>

type businessNaturesActionCreators = typeof getBusinessNaturesListSuccess
export type IBusinessNaturesListActions = ReturnType<businessNaturesActionCreators>
