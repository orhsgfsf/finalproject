import { ICountriesListState, ICurrenciesListState, IPropertyTypesListState, IBusinessNaturesListState } from './state'
import { ICountriesListActions, ICurrenciesListActions, IPropertyTypesListActions, IBusinessNaturesListActions } from './actions'

const initialStateOfCountriesList: ICountriesListState = {
    countriesList: [],
    msg: ""
}

const initialStateOfCurrenciesList: ICurrenciesListState = {
    currenciesList: [],
    msg: ""
}

const initialStateOfPropertyTypesList: IPropertyTypesListState = {
    propertyTypesList: [],
    msg: ""
}

const initialStateOfBusinessNaturesList: IBusinessNaturesListState = {
    businessNaturesList: []
}

export function countriesListReducer(state: ICountriesListState = initialStateOfCountriesList, action: ICountriesListActions): ICountriesListState{
    switch (action.type) {
        case "GET_COUNTRIES_LIST_SUCCESS":
            return {
                ...state,
                countriesList: action.countriesList
            }
        case "GET_COUNTRIES_LIST_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        default:
            return state
    }
}

export function currenciesListReducer(state: ICurrenciesListState = initialStateOfCurrenciesList, action: ICurrenciesListActions):ICurrenciesListState{
    switch (action.type) {
        case "GET_CURRENCIES_LIST_SUCCESS":

            return {
                ...state,
                currenciesList: action.currenciesList
            }
        case "GET_CURRENCIES_LIST_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        default:
            return state
    }
}

export function propertyTypesListReducer(state: IPropertyTypesListState = initialStateOfPropertyTypesList, action: IPropertyTypesListActions):IPropertyTypesListState {
    switch (action.type) {
        case "GET_PROPERTY_TYPES_LIST_SUCCESS":
            console.log(action.propertyTypesList)
            return {
                ...state,
                propertyTypesList: action.propertyTypesList
            }
        case "GET_PROPERTY_TYPES_LIST_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        default:
            return state
    }
}

export function businessNaturesListReducer(state:IBusinessNaturesListState = initialStateOfBusinessNaturesList, action: IBusinessNaturesListActions):IBusinessNaturesListState{
    switch (action.type){
        case "GET_BUSINESS_NATURES_LIST_SUCCESS":
            return {
                ...state,
                businessNaturesList: action.businessNaturesList
            }
            default:
                    return state
    }
}

