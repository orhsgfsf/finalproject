import {Dispatch} from 'redux'
import {CallHistoryMethodAction} from 'connected-react-router'
import { getCurrenciesListSuccess, getPropertyTypesListSuccess, failed, getCountriesListSuccess, ICurrenciesListActions, ICountriesListActions, IPropertyTypesListActions, IBusinessNaturesListActions, getBusinessNaturesListSuccess } from './actions';

export function getCountriesList() {
    return async (dispatch: Dispatch<ICountriesListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/countriesList`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })

        const result = await res.json()

        if (res.status !== 200){
            dispatch(failed("GET_COUNTRIES_LIST_FAILED",result.msg))
        }else{
            dispatch(getCountriesListSuccess(result.data))
        }
    }
}

export function getCurrenciesList() {
    return async (dispatch: Dispatch<ICurrenciesListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/currenciesList`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })

        const result = await res.json()

        if (res.status !== 200){
            dispatch(failed("GET_CURRENCIES_LIST_FAILED",result.msg))
        }else{
            dispatch(getCurrenciesListSuccess(result.data))
        }
    }
}

export function getPropertyTypesList() {
    return async (dispatch: Dispatch<IPropertyTypesListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/propertyTypesList`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })

        const result = await res.json()

        if (res.status !== 200){
            dispatch(failed("GET_PROPERTY_TYPES_LIST_FAILED", result.msg))
        } else {
            dispatch(getPropertyTypesListSuccess(result.data))
        }
    }
}

export function getBusinessNaturesList() {
    return async (dispatch: Dispatch<IBusinessNaturesListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/tenantContracts/businessNaturesList`)
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
           return ("not good")
        } else {
            dispatch(getBusinessNaturesListSuccess(result.data))
        }
    }
}