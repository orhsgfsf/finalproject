export interface ICountriesList{
    id:number
    country:string
}

export interface ICurrenciesList{
    id:number
    currency:string
}

export interface IPropertyTypesList{
    id:number
    propertyType:string
}

export interface IBusinessNaturesList{
    id:number
    businessNature:string
}

export interface ICountriesListState{
    countriesList:ICountriesList[]
    msg:string
}

export interface ICurrenciesListState{
    currenciesList:ICurrenciesList[]
    msg:string
}

export interface IPropertyTypesListState{
    propertyTypesList:IPropertyTypesList[]
    msg:string
}

export interface IBusinessNaturesListState{
    businessNaturesList:IBusinessNaturesList[]
}