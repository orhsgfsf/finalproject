import { IContractListState } from './state'
import { IContractListActions } from './actions'

const initialState: IContractListState = {
    contractList: []
}

export function contractListReducer(state: IContractListState = initialState, action: IContractListActions): IContractListState {

    switch (action.type) {
        case "GET_CONTRACT_LIST_SUCCESS":

            return {
                ...state,
                contractList: action.contractList
            }
        case "CONFIRM_CONTRACT_SUCCESS":
            console.log(state.contractList)
            console.log(action.contract)
            return {
                ...state,
                contractList: state.contractList.map(contract => {
                    if(contract.id !== action.contract.id){
                        console.log(contract)
                        return contract
                    }else {
                       return {
                           ...contract,
                           confirm_clicked: true,
                           contract_status: true
                       }
                        // contract.confirm_clicked = true
                        // contract.contract_status = true
                        // return contract
                    }
                })
                   
                
            }
        case "DELETE_CONTRACT_SUCCESS":
            return {
                ...state,
                contractList: state.contractList.filter(contract => 
                    contract.id !== action.id)
            }






        default:
            return state
    }

}