export interface IContractList{
    id:number
    rent_due_date: number
    grace_period: number
    contract_starting_date: Date
    contract_expiry_date: Date
    rent_price: number
    deposit: number
    keys: number
    landlord_id_or_br: string
    currency: string
    landlordSignature: string
    code: string
    landlord_name: string
    grace_period_expiry_date: Date
    grace_period_starting_date: Date
    landlord_signature: string
    propertyType: string
    unit: string
    unit_id: number
    country: string
    address: string
    management_fee: number
    government_rent: number
    rates: number
    saleable_area: number
    gross_floor_area: number
    tenant_name: string | null
    contact_person: string | null
    contact_number: number | null
    tenant_id_or_br: string | null
    tenant_business_nature: string | null
    tenant_signature: string 
    contract_status: boolean
    confirm_clicked: boolean
    void_contract_date: Date | null
}

export interface IContractListState{
    contractList: IContractList[]
}