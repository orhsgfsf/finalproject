import { getContractListSuccess, IContractListActions, confirmContractSuccess, deleteContractSuccess } from "./actions";

import { CallHistoryMethodAction, push } from "connected-react-router";
import { Dispatch } from 'redux'
import { IContractList } from "./state";
import { ThunkDispatch } from "../store";


export function getContractByPropertyId(id:number) {
    return async (dispatch:Dispatch<IContractListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/toGetContract`, {
            method:"POST",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-type": "application/json"
                // "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({property_id:id})
        
        })
        const result = await res.json()
        console.log({contractList:result.data})
        dispatch(getContractListSuccess(result.data))
       
    }
}

export function deleteContract(id:number, unit_id:number) {
    return async (dispatch: Dispatch<IContractListActions | CallHistoryMethodAction>) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/contract`, {
            method:"DELETE",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,

                "Content-type": "application/json",
                // "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({id, unit_id})
        
        })
       
        dispatch(deleteContractSuccess(id))
    }
}

export function deleteContractByCode(code:string) {
    return async (dispatch: Dispatch<IContractListActions | CallHistoryMethodAction>) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/contract`, {
            method:"DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({code})
        
        })
       
        dispatch(push('/menu/contract'))
    }
}

export function confirmContract(contract:IContractList, unit_id:number){
    return async (dispatch: Dispatch<IContractListActions | CallHistoryMethodAction>) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/toConfirmContract`, {
            method:"PATCH",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,

                "Content-type": "application/json"
                // "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({contract_id:contract.id, unit_id})
        
        })
       
        dispatch(confirmContractSuccess({...contract, confirm_clicked:true, contract_status:true}))
    }
}

export function getContractByStatus(id:number){
    return async (dispatch: Dispatch<IContractListActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/toGetContractByStatus`, {
            method:"POST",
            headers: {
                "Content-type": "application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({property_id:id})
        
        })
        const result = await res.json()
        console.log({contractList2:result.data})
        dispatch(getContractListSuccess(result.data))
    }
}

export function activateContractPending(unit_id:number){
    return async (dispatch:ThunkDispatch) => {

        await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/toActiveContractPending`,{
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({unit_id})
        })
    }
}
    