import {IContractList} from './state'

export function getContractListSuccess(contractList:IContractList[]){
    return {
        type: "GET_CONTRACT_LIST_SUCCESS" as "GET_CONTRACT_LIST_SUCCESS",
        contractList
    }
}

export function deleteContractSuccess(id:number){
    return {
        type: "DELETE_CONTRACT_SUCCESS" as "DELETE_CONTRACT_SUCCESS",
        id
    }
}

export function confirmContractSuccess(contract:IContractList){
    console.log(contract)
    return {
    
        type: "CONFIRM_CONTRACT_SUCCESS" as "CONFIRM_CONTRACT_SUCCESS",
        contract
    }
}

type contractListActionCreators = typeof getContractListSuccess | typeof deleteContractSuccess | typeof confirmContractSuccess
export type IContractListActions = ReturnType<contractListActionCreators>