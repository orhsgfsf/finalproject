import Login from './login';
import Register from './Register'
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store, { history } from './store';
import { ConnectedRouter } from 'connected-react-router';
import { PrivateRoute } from './privateRoute';
import { Container } from 'reactstrap';

import Menu from './Menu';
// import Dashboard from './Dashboard';
import PersonalInfo from './PersonalInfo';
import Properties from './Properties';
import ContractCreate from './ContractCreate';

import Contracts from './Contracts';
import ContractReRent from './ContractReRent';
import FloorPlan from './FloorPlan';
import FloorPlanUpload from './FloorPlanUpload';
import Drag from './Drag';
import PersonalInfoEdit from './PersonalInfoEdit'
import SignContractSuccess from './SignContractSuccess'
import Home from './Home';
import ContractPreview from './ContractPreview';
import ContractConfirm from './ContractConfirm';


export default class App extends React.Component {


  public render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          
          <Switch>

            <Route path="/contract_preview/:id" component={ContractPreview} />
            <div>
              <Container>
                <video id="background-video" autoPlay loop>
                  <source src="https://cdn.flixel.com/flixel/tgqtzsqactco3i3stjqv.hd.mp4" type="video/mp4" />

                  Your browser does not support the video tag.
              </video>
                <br></br>
                <div>
                  <div>
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <Route path="/contract/:id" component={ContractConfirm} />
                    <Route path="/sign_contract_success/:id" component={SignContractSuccess} />

                    <PrivateRoute path="/" exact={true} component={Home} />
                    <PrivateRoute path="/home" component={Home} />

                    <PrivateRoute path="/personal_information" component={PersonalInfo} />

                    <PrivateRoute path="/menu" component={Menu} />
                    <PrivateRoute path="/menu/personal_information_edit" component={PersonalInfoEdit} />
                    <PrivateRoute path="/menu/properties" component={Properties} />

                    <PrivateRoute path="/menu/contract" component={Contracts} />
                    <PrivateRoute path="/menu/create_contract" component={ContractCreate} />
                    <PrivateRoute path="/menu/re_rent_contract/:id" component={ContractReRent} />

                    <PrivateRoute path="/menu/floor_plan" component={FloorPlan} />
                    <PrivateRoute path="/menu/floor_plan_upload/:id" component={FloorPlanUpload} />
                    <PrivateRoute path="/menu/floor_plan_draggable/:id" component={Drag} />
                  </div>
                </div>

              </Container>
            </div>
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}




