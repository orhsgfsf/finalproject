import React from 'react'
import { Button, Row, Col } from 'reactstrap';
import { ThunkDispatch, IRootState } from './store';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import ContractListCurrent from './ContractListCurrent';
import ContractListHistory from './ContractListHistory';
// import { createRipples } from 'react-ripples';
import { getUnit } from './unit/thunks';
import { IUnit } from './unit/state';


// const MyRipples = createRipples({
//     color: "rgba(255, 254, 254, 0.33)",
//     during: 1000,
// })

interface IContractListProps {
    match: {
        params: {
            id: string
        }
    }

    units: IUnit[]

    contractListCurrent: (id: string) => void
    contractListHistory: (id: string) => void
    getUnit: (id: number) => void
}





class ContractList extends React.Component<IContractListProps>{

    private contractListCurrent = () => {
        this.props.contractListCurrent(this.props.match.params.id)
    }

    private contractListHistory = () => {
        this.props.contractListHistory(this.props.match.params.id)
    }

    public componentDidMount() {
        this.props.getUnit(parseInt(this.props.match.params.id))
    }

    public render() {

        return (
            <div className='primary3'>
                {
                    this.props.units.length !== 0 ?
                        <Row>
                            <Col md="12" className='pri3' >
                                <Button className='contract-buttons' onClick={() => this.contractListCurrent()}>Contract Status</Button>
                                <Button className='contract-buttons' onClick={() => this.contractListHistory()}>History</Button>
                            </Col>
                        </Row>
                        :
                        <div>
                            <h5>No Unit Here! Please Create One!</h5>
                        </div>
                }




                <Switch>

                    <Route path="/menu/contract/contract_list/:id/current_contract" component={ContractListCurrent} />
                    <Route path="/menu/contract/contract_list/:id/history_contract" component={ContractListHistory} />
                </Switch>





            </div>

        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    units: state.unit.units
})



const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    contractListCurrent: (id: string) => dispatch(push(`/menu/contract/contract_list/${id}/current_contract`)),
    contractListHistory: (id: string) => dispatch(push(`/menu/contract/contract_list/${id}/history_contract`)),
    getUnit: (id: number) => (dispatch(getUnit(id)))
})

export default connect(mapStateToProps, mapDispatchToProps)(ContractList)
