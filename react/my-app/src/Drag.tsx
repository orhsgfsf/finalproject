import React from 'react'
import { ThunkDispatch, IRootState } from './store';
import { getProperty } from './property/thunks';
import { getUnit } from './unit/thunks';
import { connect } from 'react-redux';
import Draggable, { DraggableEvent } from 'react-draggable'
import { IPropertyState } from './property/state';
import { IUnit } from './unit/state';
import { Button } from 'reactstrap';
import {   getCoordinates, updateCoordinates } from './draggable/thunks';
import { ICoordinates } from './draggable/state';



interface IDragProps {
    getProperty: (id: number) => void
    getUnit: (id: number) => void
    addCoordinates: (result: ICoordinates[], unit_id: number) => void
    updateCoordinates: (result: ICoordinatesResult[], unit_id: number) => void
    getCoordinates:(id:number) => void
    coordinates: ICoordinates[]
    property: IPropertyState
    units: IUnit[]

    match: {
        params: {
            id: string
        }
    }
}

interface IDragState {


    deltaPosition: {

        x: number
        y: number
    }[]
}

export interface ICoordinatesResult {

    x: number
    y: number
 
    id: number
    


}

// type DraggableEventHandler = (e: Event, data: DraggableData) => void | false;
type DraggableData = {
    node: HTMLElement,
    // lastX + deltaX === x
    x: number, y: number,
    deltaX: number, deltaY: number,
    lastX: number, lastY: number
};

class Drag extends React.Component<IDragProps, IDragState>{
    constructor(props: IDragProps) {
        super(props)

        this.state = { deltaPosition: [] }

    }




    private eventLogger = (e: MouseEvent, data: Object) => {
        console.log('Event: ', e);
        console.log('Data: ', data);
    };


    private handleDrag = (i: number, e: DraggableEvent, ui: any) => {


        const newDeltaPosition = this.state.deltaPosition.map((axis, index) => {
            if (index === i) {
                return ({
                    x: axis.x + ui.deltaX,
                    y: axis.y + ui.deltaY
                })
            }
            return { x: axis.x, y: axis.y }
        })

        this.setState({
            deltaPosition: newDeltaPosition
        });
        console.log(this.state.deltaPosition)

    }

    private submit = () => {


        const result: ICoordinatesResult[] = this.props.coordinates.map((coordinate, index) => {
            return {
                ...this.state.deltaPosition[index], id: coordinate.id
            }
        })

        console.log(result)

        // if (this.props.coordinates.length === 0) {
        //     this.props.addCoordinates(result, parseInt(this.props.match.params.id))
        // } else {
            this.props.updateCoordinates(result, parseInt(this.props.match.params.id))
        // }


        }


    // private handleStop = (i: number, e: DraggableEvent, ui:any) => {
    //     const newDeltaPosition = this.state.deltaPosition.map((axis, index) => {
    //         if(index === i) {
    //             return ({
    //                 x: axis.x + ui.deltaX,
    //                 y: axis.y + ui.deltaY
    //             })
    //         }
    //         return { x: axis.x, y : axis.y }
    //     })

    //     this.setState({
    //       deltaPosition: newDeltaPosition
    //     });
    //     console.log(this.state.deltaPosition)

    // }

    public async componentDidMount() {
        await this.props.getProperty(parseInt(this.props.match.params.id))
        await this.props.getUnit(parseInt(this.props.match.params.id))
        await this.props.getCoordinates(parseInt(this.props.match.params.id))
        console.log(this.props.units);
        this.setState({
            deltaPosition: this.props.coordinates.map(coordinate => {
                return { x: coordinate.x, y: coordinate.y }
            })
        })
    }



    public render() {
        console.log(this.props.property)
        console.log(this.props.units)


        return (
            <div>
                <br></br>
            
            <div className='primary4'>
                <div className="drag-area-name"><h3>Drag Area</h3></div>
                <div className="global-drag-block">
                    <div className="drag-block">

                        <div style={{ height: '100%', width: '100%'}}>

                         

                            {
                                this.props.coordinates.map((coordinate, index) =>

                                    <Draggable
                                        axis="both"
                                        bounds="parent"
                                        defaultPosition={{ x: coordinate.x, y: coordinate.y }}
                                        defaultClassName='X'
                                        scale={1}
                                        // onStop={(e: DraggableEvent, data: DraggableData) =>  { this.handleStop(0, e, data) } }

                                        onDrag={(e: DraggableEvent, data: DraggableData) => { this.handleDrag(index, e, data) }}
                                    >

                                        <button className="drag-button">{coordinate.unit}</button>

                                    </Draggable>
                                )
                            }

                            {/* <Draggable
                                            axis="both"
                                            bounds="parent"
                                            defaultPosition={{ x: 0, y: 0 }}
                                            defaultClassName='X'
                                            scale={1}
                                            // onStop={(e: DraggableEvent, data: DraggableData) =>  { this.handleStop(0, e, data) } }

                                            onDrag={(e: DraggableEvent, data: DraggableData) => { this.handleDrag(0, e, data) }}
                                        >

                                            <button className="drag-button">XXX</button>

                                        </Draggable> */}



                        </div>
                        <img className="image" src={this.props.property.floor_plan} alt="abc" />
                    </div>

                </div>
                <div className="submit-button"><Button onClick={() => this.submit()}>Change</Button></div>
            </div>
            </div>
        )
    }

}



const mapStateToProps = (state: IRootState) => ({
    coordinates: state.coordinates.coordinates,
    property: state.property,
    units: state.unit.units,

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getProperty: (id: number) => dispatch(getProperty(id)),

        getUnit: (id: number) => dispatch(getUnit(id)),

 getCoordinates:(id:number) => dispatch(getCoordinates(id)),
        // addCoordinates: (result: ICoordinates[], unit_id: number) => dispatch(addCoordinates(result, unit_id)),

        updateCoordinates: (result: ICoordinatesResult[], property_id: number) => dispatch(updateCoordinates(result, property_id))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(Drag)