import { IUnit } from "./state";

export function getUnitSuccess(units: IUnit[]){
    return {
        type: "GET_UNIT_SUCCESS" as "GET_UNIT_SUCCESS",
        units
    }
}

export function addUnitSuccess(unit: IUnit){
    return {
        type: "ADD_UNIT_SUCCESS" as "ADD_UNIT_SUCCESS",
        unit
    }
}

export function updateUnitSuccess(unit: IUnit){
    return {
        type: "UPDATE_UNIT_SUCCESS" as "UPDATE_UNIT_SUCCESS",
        unit
    }
}

export function deleteUnitSuccess(id:number){
    return{
        type: "DELETE_UNIT_SUCCESS" as "DELETE_UNIT_SUCCESS",
        id
    }
}



type unitActionCreators = typeof getUnitSuccess | typeof addUnitSuccess | typeof updateUnitSuccess | typeof deleteUnitSuccess
export type IUnitActions = ReturnType<unitActionCreators>