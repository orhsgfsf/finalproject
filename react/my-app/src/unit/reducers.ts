import { IUnitState } from './state'
import { IUnitActions } from './actions';


const initialState: IUnitState = {

        units:[]
       
}

export function unitReducer(state: IUnitState = initialState, action: IUnitActions):IUnitState {
    switch (action.type) {
        case "GET_UNIT_SUCCESS":
            console.log(action.units)
            return {
                ...state,
                units: action.units
            }
        case "ADD_UNIT_SUCCESS":
            return {
                ...state,
                units: [...state.units, action.unit]
            }
        case "UPDATE_UNIT_SUCCESS":
            return {
                ...state,
                units: state.units.map(unit => {
                    if (unit.id !== action.unit.id){
                        return unit
                    }else{
                        return action.unit
                    }
                })
            }
        case "DELETE_UNIT_SUCCESS":
            return {
                ...state,
                units: state.units.filter(unit => unit.id !== action.id)
            }

        default:
            return state
    }
}