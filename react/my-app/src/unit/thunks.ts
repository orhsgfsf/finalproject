import { Dispatch } from "redux";

import { push, CallHistoryMethodAction } from "connected-react-router";

import { IUnitActions, getUnitSuccess, updateUnitSuccess, deleteUnitSuccess } from "./actions";
import { addUnitFailed, IAddUnitFailActions } from "../addUnitFail/actions";
import { IUnitEdit } from "../UnitEdit";


export function getUnit(id:number){
    return async (dispatch:Dispatch<IUnitActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/toGetUnits`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            return
        }else{
            dispatch(getUnitSuccess(result.data))
        }
    }
}



export function editUnit(unitEdit: IUnitEdit) {
    return async (dispatch: Dispatch<IUnitActions | IAddUnitFailActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/units/units`, {
            method: "PATCH",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(unitEdit)
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            dispatch(addUnitFailed(result.msg)) 
        } else {
            dispatch(updateUnitSuccess(unitEdit))
            
            dispatch(push(`/menu/properties/property/${unitEdit.property_id}`))
        }
    }
}


export function deleteUnit(id: number, property_id:number) {
    return async (dispatch: Dispatch<IUnitActions | IAddUnitFailActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/units/units`, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            return
        } else {
            dispatch(deleteUnitSuccess(id))
            
            dispatch(push(`/menu/properties/property/${property_id}`))
        }
    }
}



// export function getPropertyForEdit(id:number){
//     return async (dispatch:Dispatch<IPropertyActions|CallHistoryMethodAction>) => {
//         const res = await fetch (`${process.env.REACT_APP_API_SERVER}/properties/toGetProperties`, {
//             method: "POST",
//             headers: {
//                 "Content-type" : "application/json",
//                 "Authorization" : `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({id})
//         })
//         const result = await res.json()
//         if (res.status != 200) {
//             dispatch(failed('GET_PROPERTY_FAILED',result.msg))
//         }else{
//             // dispatch(addPropertySuccess({
//             //     id: result.data[0].id,
//             //     address: propertyInformation.address,
//             //     country: propertyInformation.country
//             // }))
//             // dispatch(addDistinctCountrySuccess({
//             //     country: propertyInformation.country
//             // }))
            
//             dispatch(getPropertySuccess(result.data[0]))
//             dispatch(push(`/menu/properties/edit_property/${result.data[0].id}`))
//         }
//     }
// }
