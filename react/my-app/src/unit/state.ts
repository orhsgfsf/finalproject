
export interface IUnit{
        id:number
        unit:string
        rental_status: boolean
       
        saleable_area:number | null
        gross_floor_area:number | null
        reminder:string 
        property_id:number
        contract_pending:boolean
}

export interface IUnitState{
        units: IUnit[]
}