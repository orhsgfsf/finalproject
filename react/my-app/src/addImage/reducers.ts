import {IAddImageState} from './state'
import {IAddImageActions} from './actions'

const initialState: IAddImageState = {
    
        image: ""

    
}

export function addImageReducer(state:IAddImageState = initialState, action:IAddImageActions):IAddImageState{
    switch(action.type){
       
        case "ADD_IMAGE_SUCCESS":
                console.log(action.image)
            return {
                ...state,
                image: action.image
            }

        case "CLEAR_IMAGE":
            return {
                ...state,
                image: initialState.image
            }
        default:
            return state
    }
}