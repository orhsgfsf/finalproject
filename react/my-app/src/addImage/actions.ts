export function addImageSuccess (image:string){
    console.log(image)
    return {
        type: "ADD_IMAGE_SUCCESS" as "ADD_IMAGE_SUCCESS",
        image
    }
}

export function clearImage () {
    return {
        type: "CLEAR_IMAGE" as "CLEAR_IMAGE"
    }
}

type addImageActionCreators = typeof addImageSuccess | typeof clearImage
export type IAddImageActions = ReturnType<addImageActionCreators>