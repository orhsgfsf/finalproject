import { Dispatch } from "redux";

import { push,CallHistoryMethodAction } from "connected-react-router";

import {IAddImageActions, addImageSuccess } from "./actions";
import { IPropertyActions } from "../property/actions";
import { IFloorPlan } from '../FloorPlanUpload'
import { changeImageSuccess, IUserDataEditActions } from "../userDataEdit/actions";



export function addImage(file:File) {
    return async (dispatch: Dispatch<IAddImageActions|CallHistoryMethodAction>) => {
        const formData = new FormData()
        formData.append('pic',file)
        console.log(file)
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/floorPlan`, {
            method: "POST",
            headers: {
               
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            console.error(result.msg)
        } else {
            console.log(result.location)
            dispatch(push('/menu/properties'))
        }
    }
}

export function addFloorPlan(file:File) {
    return async (dispatch: Dispatch<IAddImageActions|CallHistoryMethodAction>) => {
        const formData = new FormData()
        formData.append('pic',file)
        console.log(file)
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/floorPlan/floorPlan`, {
            method: "POST",
            headers: {
               
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            console.error(result.msg)
        } else {
            console.log(result.location)
            dispatch(addImageSuccess(result.data))
        }
    }
}

export function changeImage(file:File){
    return async (dispatch: Dispatch<IUserDataEditActions|CallHistoryMethodAction>) => {
        const formData = new FormData()
        formData.append('pic',file)
        console.log(file)
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/floorPlan`, {
            method: "POST",
            headers: {
               
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: formData
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            console.error(result.msg)
        } else {
            console.log(result.location)
            dispatch(changeImageSuccess(result.location))
        }
    }
}



export function addFloorPlanToDB(floorPlan:IFloorPlan){
    return async (dispatch:Dispatch<IPropertyActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/floorPlans`, {
            method: "PATCH",
            headers:{
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(floorPlan)
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200){
            console.error(result.msg)
        }else{
            dispatch(push(`/menu/floor_plan/details/${floorPlan.id}`))
        }
    }
}