import { Dispatch } from "redux";
import { push, CallHistoryMethodAction } from "connected-react-router";
import { IUnitInformation } from "../UnitCreate";
import { IAddUnitFailActions, addUnitFailed } from "./actions";
import { IUnitActions, addUnitSuccess } from "../unit/actions";



export function addUnit(unitInformation: IUnitInformation) {
    return async (dispatch: Dispatch<IAddUnitFailActions | IUnitActions |CallHistoryMethodAction>) => {
        console.log("WTF?")
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/units/units`, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(unitInformation)
        })
        const result = await res.json()
        console.log(result)
        if (res.status !== 200) {
            dispatch(addUnitFailed(result.msg))
        } else {
            const unitDetails = {...unitInformation, id:result.data.id}
            await dispatch(addUnitSuccess(unitDetails))           
            await dispatch(push(`/menu/properties/property/${unitInformation.property_id}`))
        }
    }
}

// export function deleteProperty(id:number, country_id:number, country:string){
//     return async (dispatch:ThunkDispatch) => {
//         await fetch(`${process.env.REACT_APP_API_SERVER}/properties/properties`,{
//             method: "DELETE",
//             headers:{
//                 "Content-type": "application/json",
//                 "Authorization": `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({id})
//         })
        
//         dispatch(deletePropertySuccess({id}))
//         dispatch(checkCountry(country_id, country))
//         dispatch(push('/menu/properties/property_interface'))
//     }
// }

// export function checkCountry(country_id:number, country:string){
//     return async (dispatch:Dispatch<IDistinctCountriesActions | CallHistoryMethodAction>) => {
//         const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/country`,{
//             method:"POST",
//             headers:{
//                 "Content-type": "application/json",
//                 "Authorization": `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({country_id})})
//         const result = await res.json()
           
//         if (result.data.length == 0){
//             dispatch(deleteDistinctCountrySuccess({
//                 id:country_id,
//                 country: country
//             }))

//         }
//         dispatch(push('/menu/properties/property_interface'))
//     }
// }