
export interface IAddUnitFailState {
        addUnitFail:{

                unit: string
                saleableArea: string
                grossFloorArea: string
                reminder: string
        }

}

export interface IInsideAddUnitFailState{
        unit: string
        saleableArea: string
        grossFloorArea: string
        reminder: string  
}