import { IAddUnitFailState } from './state'
import { IAddUnitFailActions } from './actions'

const initialState: IAddUnitFailState = {
    addUnitFail:{       
        unit: "",
        saleableArea: "",
        grossFloorArea: "",
        reminder: "",
    }
}

export function addUnitFailReducer(state: IAddUnitFailState = initialState, action: IAddUnitFailActions):IAddUnitFailState{
    switch (action.type) {
        case "ADD_UNIT_FAILED":
            return {
                ...state,
                addUnitFail: action.addUnitFail
            }
        case "RESET":
            return {
                ...state,
                addUnitFail: initialState.addUnitFail
            }
        default:
            return state
    }
}

