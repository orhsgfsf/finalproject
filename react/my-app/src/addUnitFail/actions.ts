import { IInsideAddUnitFailState } from "./state";

export function addUnitFailed(addUnitFail: IInsideAddUnitFailState){
    return {
        type: "ADD_UNIT_FAILED" as "ADD_UNIT_FAILED",
        addUnitFail
    }
}

export function resetUnitFailed(){
    return {
        type: "RESET" as "RESET"
    }
}



type AddUnitFailActionCreators = typeof addUnitFailed | typeof resetUnitFailed
export type IAddUnitFailActions = ReturnType<AddUnitFailActionCreators>