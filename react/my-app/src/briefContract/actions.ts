import {IBriefContracts} from './state'


export function getBriefContractsSuccess(briefContracts :IBriefContracts[]){
    return {
        type: "GET_CONTRACT_PROPERTIES_SUCCESS" as "GET_CONTRACT_PROPERTIES_SUCCESS",
        briefContracts
    }
}

type briefContractsActionCreator = typeof getBriefContractsSuccess
export type IBriefContractsActions = ReturnType<briefContractsActionCreator>