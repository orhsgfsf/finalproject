import {IBriefContractsState} from './state'
import {IBriefContractsActions} from './actions'

const initialState:IBriefContractsState = {
    briefContracts:[]
}

export function briefContractsReducer(state:IBriefContractsState = initialState, action:IBriefContractsActions):IBriefContractsState{
    switch(action.type){
        case "GET_CONTRACT_PROPERTIES_SUCCESS":
            return {
                ...state,
                briefContracts: action.briefContracts
            }
        default:
            return state
    }
}