import { Dispatch } from 'redux'
import { CallHistoryMethodAction } from "connected-react-router"
import { IBriefContractsActions, getBriefContractsSuccess } from './actions';

export function getBriefContracts() {
    return async (dispatch: Dispatch<IBriefContractsActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/generalContract`, {        
            headers: {
                "Content-type": "application/json",           
            }          
        })
        const result = await res.json()
        console.log(result)
        if (res.status != 200){
            return 
        }else{
            dispatch(getBriefContractsSuccess(result.data))
        }
    }
}

