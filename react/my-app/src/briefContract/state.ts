export interface IBriefContracts{
    unit:string
    address:string
    country:string
}

export interface IBriefContractsState{
    briefContracts: IBriefContracts[]
}
