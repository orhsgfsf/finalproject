import React from 'react'
import {Row, Col, Container, Button} from 'reactstrap';
// import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
import { IRootState, ThunkDispatch } from './store';
// import { getUnitsList } from './unitsList/thunks';
// import { IUnitsList } from './unitsList/state';
import { connect } from 'react-redux';
// import { IContractDetailsState } from './contract/state';
// import { getUnitForContract } from './contract/thunks';
import { getContractByCode, confirmContractByTenant, pdf } from './contractDraft/thunks';
import { IContractDraftState } from './contractDraft/state';
import { getBusinessNaturesList } from './list/thunks';
import './property.css'


const SignatureCanvas = require('react-signature-canvas').default

interface IContractConfirmProps {
    match: {
        params: {
            id: string
        }
    }

    contractDraft: IContractDraftState
    businessNaturesList: {
        businessNature: string
    }[]

    getContractByCode: (id: string) => void
    getBusinessNaturesList: () => void
    confirmContractByTenant: (contractUpdate: ITenantUpdate, id:string) => void
    pdf: (code:string) => void
}


interface IContractConfirmState {
    tenantName: string
    tenantIdOrBr: string
    contactPerson: string
    contactNumber: string
    businessNature: string


}

export interface ITenantUpdate {
    
    tenantName: string
    tenantIdOrBr: string
    contactPerson: string
    contactNumber: number
    businessNature: string
    tenantSignature: string
    code: string
}



class ContractConfirm extends React.Component<IContractConfirmProps, IContractConfirmState> {
    constructor(props: IContractConfirmProps) {
        super(props);

        this.state = {
            tenantName: "",
            tenantIdOrBr: "",
            contactPerson: "",
            contactNumber: "",
            businessNature: ""
        };



    }
    sigPadForTenant: any = {}
    clearSignature = () => {       
        this.sigPadForTenant.clear()
      }

    private onChange = (field: "tenantName" | "tenantIdOrBr" | "contactPerson" | "contactNumber" | "IdOrBrNumber" | "tenantName", e: React.FormEvent<HTMLInputElement>) => {
        let state: any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }

    private onChangeBusinessNature = (field: "businessNature", e: React.FormEvent<HTMLSelectElement>) => {
        let state: any = {}
        state[field] = e.currentTarget.value
        console.log(e.currentTarget.value)
        this.setState(state);
        console.log(state)
    }


    private onConfirmByTenant = () => {

        const contractUpdate: ITenantUpdate = {
            tenantName: (this.props.contractDraft.tenant_name ? this.props.contractDraft.tenant_name : this.state.tenantName),
            tenantIdOrBr: (this.props.contractDraft.tenant_id_or_br ? this.props.contractDraft.tenant_id_or_br : this.state.tenantIdOrBr),
            contactPerson: this.state.contactPerson,
            contactNumber: parseInt(this.state.contactNumber),
            businessNature: (this.props.contractDraft.tenant_business_nature ? this.props.contractDraft.tenant_business_nature : this.state.businessNature),
            tenantSignature: this.sigPadForTenant.toDataURL('image/png'),
            code: this.props.contractDraft.code
        }
        this.props.confirmContractByTenant(contractUpdate, this.props.match.params.id)
    }

    private generatePdf = () => {
        this.props.pdf(this.props.contractDraft.code)
    }



    public componentDidMount() {
        this.props.getContractByCode(this.props.match.params.id)
        this.props.getBusinessNaturesList()

    }


    public render() {
        console.log(this.props.contractDraft)
        return (
            <div>
                <br></br>
                <div className='primary'>
                <h6>Disclaimer</h6>
                  <br></br>

The material contained in this web site has been produced by we-vital in accordance with its current practices and policies and with the benefit of information currently available to it, and all reasonable efforts have been made to ensure the accuracy of the contents of the pages of the web site at the time of preparation. we-vital regularly reviews the web site and where appropriate will update pages to reflect changed circumstances.<br></br>
<br></br>
Notwithstanding all efforts made by we-vital to ensure the accuracy of the web site, no responsibility or liability is accepted by we-vital in respect of any use or reference to the web site in connection with any matter or action taken following such use or reference or for any inaccuracies, omissions, mis-statements or errors in the said material, or for any economic or other loss which may be directly or indirectly sustained by any visitor to the web site or other person who obtains access to the material on the web site. <br></br>
<br></br>
The material on this web site is for general information only and nothing in this web site contains professional advice or an offer for sale or any binding commitment upon we-vital in relation to the availability of property or the description of property. All visitors should take advice from a suitably qualified professional in relation to any specific query or problem that they may have or in relation to any property included in this web site, and we-vital takes no responsibility for any loss caused as a result of reading the contents of this web site.<br></br>
<br></br>
No claims, actions or legal proceedings in connection with this web site brought by any visitor or other person having reference to the material on this web site will be entertained by we-vital.<br></br>
<br></br>
                <Container className="contract-background">
                
                    <br></br>
                    <h2>Tenancy Agreement</h2>
                    <div className="contract">
                        <Row>
                            <Col md="12">
                                <div>
                                    <h3>Activate Date</h3>
                                    The Landlord and the Tenant as more particularly declared in <b>Schedule I</b> that an Agreement made the date of  {moment(this.props.contractDraft.contract_starting_date).format("YYYY-MM-DD")}
                                    <br />
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <br></br>
                                <div>
                                    <h3>Terms</h3>
                                    <b>
                                        The Landlord and the Tenant shall follow the Terms and the Rent of the Premises as more particularly
                                        described in Schedule I.
                                        Both , the Landlord and the Tenant, agree to observe and perform the terms and
                                        conditions as
                                        below:
                                    </b>
                                    <br />

                                    <b>1) </b>The Tenant shall pay to the Landlord the Rent in advance on the
                                    {this.props.contractDraft.rent_due_date} day of each and every calendar month during the Term.
                                    <br />
                                    <b>2) </b>The Tenant shall not make any additions or
                                    alteration to the Premises unless the Landlords acquire and accept the written consent.
                                    <br />

                                    <b>3) </b>The Tenant shall not assign,
                                    transfer, sublet or part with the possession of the Premises or any part thereof to any other person. This tenancy shall be personal to the Tenant named herein.
                                    <br />
                                    <b>4) </b>
                                    The Tenant shall comply with all ordinances, regulations and rules of Hong Kong and
                                    shall
                                    observe and perform the covenants, terms and conditions of the Deed fo Mutual Covenant
                                    and
                                    Sub-Deed of Mutual Covenant (if any) relating to the Premises. The Tenant shall not
                                    contravene any negative or restrictive covenants contained in the Government Lease(s)
                                    under
                                    which the Premises are held from the Government.
                                    <br />

                                    <b>5) </b>
                                    The Tenant shall pay to the Landlord the Security Deposit set out in Schedule I for
                                    the due observance and performance of the terms and
                                    conditions herein contained and on his part to be observed and performed. If the Rent and/
                                    or any changes payable by the Tenant hereunder or any part
                                    thereof shall be unpaid for seven (7) days after the same shall become payable (whether
                                    legally demanded or not) or if the Tenant shall commit a breach of
                                    any of the terms and conditions herein contained, it shall be lawful for the Landlord at any
                                    time thereafter to rn-enter the Premises whereupon this Agreement shall
                                    absolutely determine and the Landlord may deduct any loss or damage suffered by the Landlord
                                    as a result of the Tenant's breach from the Security.
                                    Deposit without prejudice to any other right of action or any remedy of the Landlord in
                                    respect of such breach of the Tenant.
                                    <br />

                                    <b>6) </b>
                                    The Tenant shall have rights to hold and enjoy the Premises during the Term
                                    without any interruption by the Landlord.
                                    <br />

                                    <b>7 </b>
                                    The Landlord shall keep and maintain the structural parts of the Premises.
                                    Provided that the Landlord's liability
                                    shall not be incurred unless and until written notice of any defect or want of repair
                                    has been given by the Tenant to the Landlord and the Landlord shall have failed to take
                                    reasonable steps to repair and remedy the same after the lapse of a reasonable time from
                                    the date of service of such notice.
                                    <br />

                                    <b>8) </b>
                                    The Stamp Duty payable on this Agreement in duplicate shall be shared in equal
                                    shares by both the Landlord and the Tenant.
                                    <br />

                                    <b>9) </b>
                                    The Landlord and the Tenant agree to be bound by the additional terms and
                                    conditions contained in Appendix (if any)
                                    <br />
                                    <b>10) </b>
                                    The Tenant shall not use or permit to be used the Premises or any part there of for
                                    {this.props.contractDraft.propertyType} purpose only.
                                    <br />

                                    <b>11) </b>
                                    The Tenant shall be entitled to a rent free period of
                                    <b><u>{this.props.contractDraft.grace_period}</u></b>

                                    from

                                    <b><u>{moment(this.props.contractDraft.grace_period_starting_date).format("YYYY-MM-DD")}</u></b>
                                    to
                                    <b><u>{moment(this.props.contractDraft.grace_period_expiry_date).format("YYYY-MM-DD")}</u></b>
                                    (both days inclusive) provided that the
                                    Tenant shall be responsible for the charges of water, electricity, gas, telephone and other
                                    outgoings payable in respect of Premises during such rent free period.
                                    <br />


                                    <b>12) </b>
                                    If there is any contradiction between the English version and the Chinese version
                                    in this Agreement, the Chinese version shall prevail.
                                    <br />

                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <br></br>
                                <div className="contract-details">
                                   
                                    <h3>Contract Details</h3>
                                    <div className = 'pri'>
                                        <br></br>
                                        <div>
                                    Address: <b><u>Unit {this.props.contractDraft.unit}, {this.props.contractDraft.address}, {this.props.contractDraft.country}</u></b>
                                    <br />
                                    Term: From <b><u>{moment(this.props.contractDraft.contract_starting_date).format("YYYY-MM-DD")}</u></b> to <b><u>{moment(this.props.contractDraft.contract_expiry_date).format("YYYY-MM-DD")}</u></b>
                                    <br />
                                    Grace Period: From <b><u>{moment(this.props.contractDraft.grace_period_starting_date).format("YYYY-MM-DD")}</u></b> to <b><u>{moment(this.props.contractDraft.grace_period_expiry_date).format("YYYY-MM-DD")}</u></b>, <b><u>{this.props.contractDraft.grace_period} day(s)</u></b>
                                    <br />
                                    Rent Due Date: <b><u>{moment(`2000-1-${this.props.contractDraft.rent_due_date}`).format("Do")} of month</u></b>
                                    <br />
                                    Rent Per Month: <b><u>{this.props.contractDraft.currency} {this.props.contractDraft.rent_price}</u></b>
                                    <br />
                                    Management Fees: <b><u>{this.props.contractDraft.currency} {this.props.contractDraft.management_fee}</u></b>
                                    <br />
                                    Government Rent: <b><u>{this.props.contractDraft.currency} {this.props.contractDraft.government_rent}</u></b>
                                    <br />
                                    Rates:  <b><u>{this.props.contractDraft.currency} {this.props.contractDraft.rates}</u></b>
                                    <br />
                                    Deposit:  <b><u>{this.props.contractDraft.currency} {this.props.contractDraft.deposit}</u></b>
                                    <br />
                                    Number of key(s) of the Premises received by the Tenant: <b><u>{this.props.contractDraft.keys}</u></b>
                                    </div>
                                        <br></br>
                                    </div>
                                </div>
                            </Col>
                        </Row>


                        <div className="signature-details">
                            <br></br>
                            <h3>Signature</h3>
                            <div>
                                <div className='contract-text'>Confirmed and Accepted all the terms and Conditions contained herein by the Landlord and the Tenant:</div>
                                <br></br>
                                <Row>
                                    <Col md="6">
                                        
                                        <div className='contract-text'>Signature of Landlord:</div>
                                        <br />
                                        <img src={this.props.contractDraft.landlord_signature} alt=""/>
                                        <br />
                                        <br></br>
                                        Name of Landlord: <b><u>{this.props.contractDraft.landlord_name}</u></b>
                                        <br />
                                        ID / BR Number: <b><u>{this.props.contractDraft.landlord_id_or_br}</u></b>
                                        <br />
                                    </Col>

                                    <div className = 'pri-sign'>
                                    <Col md="6">
                                        {this.props.contractDraft.tenant_signature ?
                                            <div>
                                                <div className='contract-text'>Signature of Tenant:</div>
                                                <br />
                                                <img src={this.props.contractDraft.tenant_signature} alt=""/>
                                                <br />
                                                <br></br>
                                                Name of Tenant: <b><u>{this.props.contractDraft.tenant_name}</u></b>
                                                <br />
                                                ID / BR Number: <b><u>{this.props.contractDraft.tenant_id_or_br}</u></b>
                                        <br />
                                        Contact Person: <b><u>{this.props.contractDraft.contact_person ? this.props.contractDraft.contact_person : "No Contact Person"}</u></b>
                                        <br />
                                        Contact Number: <b><u>{this.props.contractDraft.contact_number}</u></b>
                                        <br />
                                        Business Nature of Tenant: <b><u>{this.props.contractDraft.tenant_business_nature}</u></b>
                                        <br />
                                    
                                            </div>
                                            :
                                            <div>
                                                <div className="contract-text">Signature of Tenant: (Please sign in the area below)</div>

                                                <div>
                                                    <SignatureCanvas canvasProps={{ width: 425, height: 250, className: "sigCanvas" }}
                                                        ref={(ref: any) => { return this.sigPadForTenant = ref }} />
                                                </div>
                                                <div className="landlord-area">
                                                <Button onClick={this.clearSignature}>Re-Sign</Button>
                                                </div>
                                                <br />
                                                Name of Tenant: {this.props.contractDraft.tenant_name ? <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.tenant_name} disabled={true}/>: <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.tenantName || ""} onChange={this.onChange.bind(this, "tenantName")} />}
                                                <br />
                                                ID / BR Number: {this.props.contractDraft.tenant_id_or_br ? <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.tenant_id_or_br} disabled={true}/> : <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.tenantIdOrBr || ""} onChange={this.onChange.bind(this, "tenantIdOrBr")} />}
                                                <br />
                                                Contact Person (Optional): <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.contactPerson || ""} onChange={this.onChange.bind(this, "contactPerson")} />
                                                <br />
                                                Contact Number: <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.contactNumber || ""} onChange={this.onChange.bind(this, "contactNumber")} />
                                                <br />
                                                Business Nature of Tenant:
                                                {this.props.contractDraft.tenant_business_nature ? <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.tenant_business_nature} disabled={true}/> : 
                                                <div>
                                                    <select className="browser-default custom-select" onChange={this.onChangeBusinessNature.bind(this, "businessNature")}>
                                                    <option>--- Please select Business Nature ---</option>
                                                    {this.props.businessNaturesList.map(nature => <option>{nature.businessNature}</option>)}
                                                </select>
                                                </div>
                                                }
                                                <br />
                                                <div className="landlord-area">
                                                <Button onClick={this.onConfirmByTenant}>Confirm By Tenant</Button>
                                                </div>
                                             
                                                
                                               
                                            </div>
                                        }



                                    </Col>
                                    </div>
                                    
                                </Row>
                               

                            </div>
                            <br></br>


                        </div>


                    </div>
                </Container>
            </div>
        </div>
        )
    }
}


const mapStateToProps = (state: IRootState) => {
    return ({
        contractDraft: state.contractDraft,
        unitsList: state.unitsList.unitsList,
        businessNaturesList: state.businessNaturesList.businessNaturesList
    })
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getBusinessNaturesList: () => dispatch(getBusinessNaturesList()),
        getContractByCode: (id: string) => dispatch(getContractByCode(id)),
        confirmContractByTenant: (contractUpdate: ITenantUpdate, id:string) => dispatch(confirmContractByTenant(contractUpdate, id)),
        pdf: (code:string) => dispatch(pdf(code))
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ContractConfirm)