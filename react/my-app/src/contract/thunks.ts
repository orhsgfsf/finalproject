import { Dispatch } from 'redux'
import { CallHistoryMethodAction } from "connected-react-router"
import { getUnitForContractSuccess, IContractDetailsActions } from './actions'


export function getUnitForContract(unitId:number) {
    return async (dispatch: Dispatch<IContractDetailsActions | CallHistoryMethodAction>) => {
        console.log(unitId)
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/units/unitForContract`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({unitId})
        })
        const result = await res.json()
        console.log(result)

        if (res.status !== 200) {
            return 
        }
        else {
            dispatch(getUnitForContractSuccess(result.data[0]))
        }
    }
}

