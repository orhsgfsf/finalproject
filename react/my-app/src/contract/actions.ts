import {IContractDetailsState} from './state'

export function getUnitForContractSuccess(contractDetails:IContractDetailsState){
    return {
        type: "GET_UNIT_FOR_CONTRACT_SUCCESS" as "GET_UNIT_FOR_CONTRACT_SUCCESS",
        contractDetails
    }
}

type contractDetailActionCreators = typeof getUnitForContractSuccess
export type IContractDetailsActions = ReturnType<contractDetailActionCreators>