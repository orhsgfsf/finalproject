export interface IContractDetailsState{
    id:number
    unit:string
    address:string
    management_fee:number
    government_rent:number
    rates:number
    country:string
    propertyType:string
    first_name:string
    last_name:string
    br_number?:string
    id_number:string
    currency:string
    date_of_birth: Date
}