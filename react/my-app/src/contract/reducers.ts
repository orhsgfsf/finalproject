import { IContractDetailsState } from './state'
import { IContractDetailsActions } from './actions'

const initialState: IContractDetailsState = {
    id: 0,
    unit: "",
    address: "",
    management_fee: 0,
    government_rent: 0,
    propertyType: "",
    rates: 0,
    country: "",
    first_name: "",
    last_name: "",
    br_number: "",
    id_number: "",
    currency:"",
    date_of_birth: new Date()
}

export function contractDetailsReducer(state: IContractDetailsState = initialState, action: IContractDetailsActions):IContractDetailsState{
    switch (action.type) {
        case "GET_UNIT_FOR_CONTRACT_SUCCESS":
            return {
                ...state,
                ...action.contractDetails
            }
        default:
            return state
        }
    }