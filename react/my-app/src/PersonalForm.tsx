import React from "react"
import { Container, Row, Col, Input, Label } from "reactstrap";
import {  Button } from '@material-ui/core';
// import "react-datepicker/dist/react-datepicker.css";
// import { MDBSelect } from "mdbreact";
import '@y0c/react-datepicker/assets/styles/calendar.scss';
import { IRootState, ThunkDispatch } from "./store";
import { connect } from "react-redux";
import { uploadInfo } from './personalInfo/thunks';
import { addImage } from "./addImage/thunks";
import DatePicker from "react-datepicker";



interface IPersonalFormProps {
    image: string
    

    

    msg: {
        first_name: string,
        last_name: string,
        gender: string,
        id_number: string,
        date_of_birth: string,
        district_number: string,
        contact_number: string
    }

    uploadInfo: (personalInfo: {}) => void
    addImage: (file: File) => void
    
}




interface IPersonalFormState {
    firstName: string
    lastName: string
    gender: string
    idNumber: any
    dateOfBirth: any
    districtNumber: any
    contactNumber: string

    company: string
    brNumber: any
    preview: string

    file: File | null
}

interface IResult {
    first_name: string,
    last_name: string,
    gender: string,
    id_number: any,
    date_of_birth: string,
    district_number: any,
    contact_number: string,
    company: string,
    br_number: any,
   
   

}

class PersonalForm extends React.Component<IPersonalFormProps, IPersonalFormState>{

    constructor(props: IPersonalFormProps) {
        super(props)
        this.state = {
            firstName: "",
            lastName: "",
            gender: "",
            idNumber: "",
            dateOfBirth: "",
            districtNumber: "",
            contactNumber: "",

            company: "",
            brNumber: "",
            preview:"",

            file: null
        }
    }

    private onChange = (field: "firstName" | "lastName" | "idNumber" | "districtNumber" | "contactNumber" | "email" | "company" | "brNumber", e: React.FormEvent<HTMLInputElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }

   

    private onChangeGender = (field: "gender", e: React.FormEvent<HTMLSelectElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value

        this.setState(state)
        console.log(this.state.gender)
        console.log(state)
    }

    

    // private onChangeDate = (field: "dateOfBirth", e: any) => {
    //     const state: any = {}
    //     state[field] = e.currentTarget.value
    //     this.setState(state)
    //     console.log(this.state.dateOfBirth)
    //     console.log(state)
    // }
    private handleChangeDate = (field:"dateOfBirth", date:any) => {
        let state:any = {}
        state[field] = date
        this.setState(
          state
        );
    }

    private onConfirm = async () => {
        const personalInfo: IResult = {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            gender: this.state.gender,
            id_number: 0,
            date_of_birth: this.state.dateOfBirth,
            district_number: 0,
            contact_number: this.state.contactNumber,
            company: this.state.company,
            br_number: 0,
           
        }
       
        
        if (this.state.file !== null){
            await this.props.addImage(this.state.file)
        }

        await this.props.uploadInfo(personalInfo)
    }


    private handleFileOnChange = (files: FileList) => {

        if (files.length !== 0){
            const x = URL.createObjectURL(files[0])
            console.log(x)
            this.setState({
                preview: x,
                file: files[0]
            })
        }
        
       
            // this.props.addImage(files[0])
            
    } 

    private deleteIcon = () => {
        this.setState({
            preview: "",
            file: null
        })
    }

    
    public render() {
       console.log(this.state.preview)
        return (
            <div className = 'primary'>
                <Container>
                    <Row>
                        <Col md="4">
                            <div className="icon-zone">
                                <div className="icon-block">
                                    {this.state.preview === "" ? <div>Profile Picture</div> : <img className="icon" src={this.state.preview} alt=''/>}
                                </div>
                            </div>
                            <br></br>
                            <div className="upload-click">
                               
                                <Input type='file' id={"file"} name="pic" style={{ display: 'none' }} onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />
                                
                                <div className=""><Label htmlFor={"file"} className="upload">Upload Profile Picture</Label></div>
                            </div>
                            <br></br>
                            <Button onClick={() => this.deleteIcon()}>Empty Profile Picture</Button>
                        </Col>


                        <Col md="8">
                            <Row>
                            <Col md="6">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">First Name:</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="First name"
                                        value={ this.state.firstName}
                                        onChange={ this.onChange.bind(this, "firstName")}
                                    />
                                </div>
                                <div className="error-msg">{this.props.msg.first_name ? `*${this.props.msg.first_name}` : ""}</div>
                            </Col>
                            <Col md="6">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">Last Name:</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.lastName}
                                        onChange={this.onChange.bind(this, "lastName")}
                                    />
                                </div>
                                <div className="error-msg">{this.props.msg.last_name ? `*${this.props.msg.last_name}` : ""}</div>
                            </Col>
                            </Row>
                            <Row>
                            <Col md="6">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">Gender:</label>

                                    <select className="browser-default custom-select" onChange={this.onChangeGender.bind(this, "gender")} value={this.state.gender}>
                                        <option>--Gender--</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                                <div className="error-msg">{this.props.msg.gender ? `*${this.props.msg.gender}` : ""}</div>
                            </Col>
                            </Row>
                            {/* <Col md="12">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">ID number:</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.idNumber}
                                        onChange={this.onChange.bind(this, "idNumber")}
                                    />
                                </div>
                                <div className="error-msg">{this.props.msg.id_number ? `*${this.props.msg.id_number}` : ""}</div>
                            </Col> */}
                            <Row>
                            <Col md="8">
                           
                            <label htmlFor="formGroupExampleInput">Date of Birth:</label>
                            <div className="form-group">
                            <DatePicker className="form-control"  selected={this.state.dateOfBirth} onChange={this.handleChangeDate.bind(this, "dateOfBirth")}  dateFormat="yyyy/MM/dd" value={this.state.dateOfBirth}/>
                                </div>
                                <div className="error-msg">{this.props.msg.date_of_birth ? `*${this.props.msg.date_of_birth}` : ""}</div>
                            </Col>
                            </Row>

                            <Row>
                            <Col md="8">
                                <label htmlFor="formGroupExampleInput">Contact Number:</label>
                           
                            {/* <Col md="3">

                                <div className="form-group">

                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.districtNumber}
                                        onChange={this.onChange.bind(this, "districtNumber")}
                                    />
                                </div>

                            </Col> */}
                            {/* <div className="-w"><div>-</div></div> */}
                          
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.contactNumber}
                                        onChange={this.onChange.bind(this, "contactNumber")}
                                    />
                                </div>
                                <div className="error-msg">{this.props.msg.contact_number ? `*${this.props.msg.contact_number}` : ""}</div>
                            </Col>
                            </Row>


                            <Row>

                            <Col md="12">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">Company(Optional):</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.company}
                                        onChange={this.onChange.bind(this, "company")}
                                    />
                                </div>
                            </Col>
                            </Row>
                            {/* <Col md="12">
                                <div className="form-group">
                                    <label htmlFor="formGroupExampleInput">Br Number(Optional):</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="formGroupExampleInput"
                                        // placeholder="Last Name"
                                        value={this.state.brNumber}
                                        onChange={this.onChange.bind(this, "brNumber")}
                                    />
                                </div>
                            </Col> */}
                            <Row>
                            <Col md="12">
                                <Button onClick={this.onConfirm}>Confirm</Button>
                            </Col>
                            </Row>
                          

                        </Col>

                    </Row>
                </Container >
            </div >
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
   
    msg: state.personalInfo.msg,
    image: state.image.image
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({

    uploadInfo: (personalInfo: {}) => dispatch(uploadInfo(personalInfo)),
    addImage: (file: File) => dispatch(addImage(file)),
   
    // goTo: () => dispatch(push('/home'))
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonalForm)

// import React, { Component } from "react"
// import { Form, FormGroup, Label, Input, Alert, Button, Col, Container, Row } from 'reactstrap';
// import clsx from 'clsx';
// import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/styles';
// import MenuItem from '@material-ui/core/MenuItem';
// import TextField from '@material-ui/core/TextField';

// // import classes from "*.module.css";

// const styles = createStyles({

// })


// interface Props {
//     classes: any
// }

// interface IPersonalFormState {
//     firstName: string
//     lastName: string
//     gender: string
//     idNumber: string
//     dateOfBirth: string
//     phoneNumber: string
//     email: string
//     company: string
//     brNumber: string

// }




// class PersonalForm extends React.Component<Props, IPersonalFormState>{

//     constructor(props: Props) {
//         super(props)
//         this.state = {
//             firstName: "",
//             lastName: "",
//             gender: "",
//             idNumber: "",
//             dateOfBirth: "",
//             phoneNumber: "",
//             email: "",
//             company: "",
//             brNumber: ""

//         }
//     }

//     private onChange = (field: "firstName" | "lastName" | "idNumber" | "dateOfBirth" | "phoneNumber" | "email", e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
//         const state: any = {}
//         state[field] = e.currentTarget.value
//         this.setState(state)
//         console.log(state)
//         console.log(this.state)
//     }

//     private onChangeGender = (field: "gender", e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => {
//         const state: any = {}
//         state[field] = e.currentTarget.value
//         this.setState(state)
//         console.log(this.state.gender)
//         console.log(state)
//     }

//     private onSubmit = (e: React.FormEvent<HTMLFormElement>) => {

//     }


//     public render() {
//         const { classes } = this.props;
//         console.log(classes)
//         // console.log(this.state)
//         return (
//             <div>
//                 <Container>
//                     <Row>
//                         <Col md="6">
//                             <form className={classes.root} noValidate autoComplete="off">
//                                 <TextField
//                                     id="standard-name"
//                                     label="First Name"
//                                     className={classes.textField}
//                                     value={this.state.firstName}

//                                     onChange={(event) => this.onChange("firstName", event)}
//                                     margin="normal"
//                                 />
//                             </form>
//                         </Col>
//                         <Col md="6">
//                             <form className={classes.root} noValidate autoComplete="off">
//                                 <TextField
//                                     id="standard-name"
//                                     label="Last Name"
//                                     className={classes.textField}
//                                     value={this.state.lastName}

//                                     onChange={(event) => this.onChange("lastName", event)}
//                                     margin="normal"
//                                 />
//                             </form>
//                         </Col>
//                     </Row>
//                     <Row>
//                         <Col md="6">
//                             <TextField
//                                 id="standard-select-currency"
//                                 select
//                                 label="Gender"
//                                 className={classes.textField}
//                                 value={this.state.gender}
//                                 onChange={(event) => this.onChangeGender("gender", event)}
//                                 SelectProps={{
//                                     MenuProps: {
//                                         className: classes.menu,
//                                     },
//                                 }}
//                                 helperText="Please select your gender"
//                                 margin="normal"

//                             >
//                                 <option key={this.state.gender} value={this.state.gender}>Male</option>
//                                 <option key={this.state.gender} value={this.state.gender}>Female</option>
//                             </TextField>

//                         </Col>
//                     </Row>
//                 </Container>
//             </div>
//         )
//     }
// }

// export default withStyles(styles)(PersonalForm);


