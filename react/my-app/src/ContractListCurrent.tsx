import React from 'react'
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
// import { IDistinctUnitsList } from './distinctUnitsList/state';
import { IUnit } from './unit/state';
import { getUnit } from './unit/thunks';
import { getContractByPropertyId, confirmContract, deleteContract } from './contractList/thunks';
// import { IBriefContracts } from './briefContract/state';
import { IContractList } from './contractList/state';
import { Col, Row, Button, Modal, ModalBody, Container } from 'reactstrap';
import { push } from 'connected-react-router';
import { pdf } from './contractDraft/thunks';
import moment from 'moment'

interface IContractListCurrentProps {
    match: {
        params: {
            id: string
        }
    }

    contractList: IContractList[]
    units: IUnit[]


    getUnit: (property_id: number) => void
    getContractByPropertyId: (property_id: number) => void

    viewContract: (code: string) => void
    downloadContract: (code: string) => void

    deleteContract: (id: number, unit_id: number) => void
    confirmContract: (contract: IContractList, unit_id: number) => void

    // voidContract:(contractId:number) => void
    // reRentContract:(contractId:number) => void
}

interface IContractListCurrentState {
    modal: boolean
}


class ContractListCurrent extends React.Component<IContractListCurrentProps, IContractListCurrentState>{
    constructor(props: IContractListCurrentProps) {
        super(props)
        this.state = {
            modal: false
        }

        this.toggle = this.toggle.bind(this)

    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }))
    }

    private confirmContract = (contract: IContractList, unit_id: number) => {

        this.props.confirmContract(contract, unit_id)
    }

    private deleteContract = (id: number, unit_id: number) => {
        this.toggle()
        this.props.deleteContract(id, unit_id)
    }

    private viewContract = (code: string) => {
        this.props.viewContract(code)
    }

    private downloadContract = (code: string) => {
        this.props.downloadContract(code)
    }

    public componentDidMount() {
        this.props.getUnit(parseInt(this.props.match.params.id))
        this.props.getContractByPropertyId(parseInt(this.props.match.params.id))
        console.log(this.props.match.params.id)

    }

    public render() {

        console.log(this.props.contractList)
        console.log(this.props.units)
        return (
            <div>{this.props.units.length === 0 && <div>No Unit here!</div>}
                <div>{this.props.units.map(unit =>
                    <div className="contract-list-block">
                        <Row>

                            <Col md="12">

                                Unit: {unit.unit}
                            </Col>

                            {this.props.contractList.filter(contract => (contract.unit_id === unit.id && (!(contract.confirm_clicked === true && contract.contract_status === false)))).length === 0 ? <div><Col md="12">No Contract</Col></div> :
                                this.props.contractList.filter(contract => (
                                    contract.unit_id === unit.id)).map(filteredContract =>
                                        <Col md="12">

                                            {
                                                filteredContract.contract_status === false &&
                                                filteredContract.tenant_signature !== null &&
                                                filteredContract.confirm_clicked === false &&
                                                <div>

                                                    <div className='primary2'>Pending (To be confirmed)</div>
                                                    <div>Tenant Name: {filteredContract.tenant_name}</div>
                                                    <div>Contract Starting Date: {moment(filteredContract.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                    <div>Contract Expiry Date: {moment(filteredContract.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                    {/* <div>Contract URL: {`https://localhost:3000/contract/${filteredContract.code}`}</div> */}
                                                    <div className='primary5'>Contract URL: {`https://we-vital.com/contract/${filteredContract.code}`}</div>
                                                    <div>
                                                        <Container>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button onClick={() => this.confirmContract(filteredContract, filteredContract.unit_id)} color="green">Confirm Contract</Button>
                                                            
                                                                <Button className='delete' onClick={this.toggle} color="red">Delete Contract</Button>
                                                                <Modal isOpen={this.state.modal} toggle={this.toggle}>

                                                                    <ModalBody toggle={this.toggle}>
                                                                        Are You Sure You Want to DELETE this Contract? You Can't Go Back If You Do this.
                                                                    </ModalBody>
                                                                    <div className='space-between'>
                                                                        <Button color="red" className='delete' onClick={() => this.deleteContract(filteredContract.id, filteredContract.unit_id)}>YES!</Button>{' '}
                                                                        <Button onClick={this.toggle}>No</Button>

                                                                    </div>
                                                                </Modal>

                                                            </Col>
                                                        </Row>
                                                        </Container>
                                                    </div>

                                                    <div>
                                                        <Container>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button onClick={() => this.viewContract(filteredContract.code)}>Preview</Button>
                                                           
                                                                <Button onClick={() => this.downloadContract(filteredContract.code)}>Download</Button>
                                                            </Col>
                                                        </Row>
                                                        </Container>
                                                    </div>

                                                </div>
                                            }

                                            {
                                                filteredContract.contract_status === false &&
                                                filteredContract.tenant_signature === null &&
                                                <div>

                                                    <div className='primary5'>Pending (Waiting Signature from the Tenant)</div>
                                                    <div>Tenant Name: Work In Progress...</div>
                                                    <div>Contract Starting Date: {moment(filteredContract.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                    <div>Contract Expiry Date: {moment(filteredContract.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                    {/* <div>Contract URL: {`https://localhost:3000/contract/${filteredContract.code}`}</div> */}
                                                    <div className='primary5'>Contract URL: {`https://we-vital.com/contract/${filteredContract.code}`}</div>
                                                    <div>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button className='delete' onClick={this.toggle} color="red">Delete Contract</Button>
                                                                <Modal isOpen={this.state.modal} toggle={this.toggle}>

                                                                    <ModalBody toggle={this.toggle}>
                                                                        Are You Sure You Want to DELETE this Contract? You Can't Go Back If You Do this.
                                                                    </ModalBody>
                                                                    <div>
                                                                        <Button color="red" className='delete' onClick={() => this.deleteContract(filteredContract.id, filteredContract.unit_id)}>YES!</Button>{' '}
                                                                        <Button onClick={this.toggle}>No</Button>

                                                                    </div>
                                                                </Modal>
                                                            </Col>

                                                        </Row>
                                                    </div>
                                                    <div>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button onClick={() => this.viewContract(filteredContract.code)}>Preview</Button>
                                                            
                                                                <Button onClick={() => this.downloadContract(filteredContract.code)}>Download</Button>
                                                            </Col>
                                                        </Row>
                                                    </div>

                                                </div>
                                            }

                                            {
                                                filteredContract.contract_status === true &&
                                                filteredContract.confirm_clicked === true &&
                                                <div>

                                                    <div color="primary2">Activated</div>
                                                    <div>Tenant Name: {filteredContract.tenant_name}</div>
                                                    <div>Contract Starting Date: {moment(filteredContract.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                    <div>Contract Expiry Date: {moment(filteredContract.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                    {moment(filteredContract.contract_expiry_date).diff(moment()) < 0 && <div color="red" >Expired!</div>}
                                                    {/* <div>Contract URL: {`https://localhost:3000/contract/${filteredContract.code}`}</div> */}
                                                    <div className='primary5'>Contract URL: {`https://we-vital.com/contract/${filteredContract.code}`}</div>
                                                    <div>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button className='delete' onClick={this.toggle} color="red" >{moment(filteredContract.contract_expiry_date).diff(moment(), 'days') < 0 ? "Expired" : "Void"}</Button>
                                                                <Modal isOpen={this.state.modal} toggle={this.toggle}>

                                                                    <ModalBody toggle={this.toggle}>
                                                                        Confirm to Void this Contract?
                                                                    </ModalBody>
                                                                    <div>
                                                                        <Button color="blue" onClick={() => this.deleteContract(filteredContract.id, filteredContract.unit_id)}>YES</Button>{' '}
                                                                        <Button onClick={this.toggle}>No</Button>

                                                                    </div>
                                                                </Modal>
                                                            </Col>

                                                        </Row>
                                                    </div>
                                                    <div>
                                                        <Row>
                                                            <Col md="12" className='space-between'>
                                                                <Button onClick={() => this.viewContract(filteredContract.code)}>View Contract</Button>
                                                           
                                                                <Button onClick={() => this.downloadContract(filteredContract.code)}>Download Contract</Button>
                                                            </Col>
                                                        </Row>
                                                    </div>

                                                </div>
                                            }
                                        </Col>

                                    )
                            }


                        </Row>

                    </div>


                )}</div>
            </div>)
    }
}

const mapStateToProps = (state: IRootState) => ({
    contractList: state.contractList.contractList,
    units: state.unit.units
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getUnit: (property_id: number) => dispatch(getUnit(property_id)),
    getContractByPropertyId: (property_id: number) => dispatch(getContractByPropertyId(property_id)),

    viewContract: (code: string) => dispatch(push(`/contract_preview/${code}`)),
    downloadContract: (code: string) => dispatch(pdf(code)),

    confirmContract: (contract: IContractList, unit_id: number) => dispatch(confirmContract(contract, unit_id)),
    deleteContract: (id: number, unit_id: number) => dispatch(deleteContract(id, unit_id))

})

export default connect(mapStateToProps, mapDispatchToProps)(ContractListCurrent)