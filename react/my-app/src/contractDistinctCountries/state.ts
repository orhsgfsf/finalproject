export interface IContractDistinctCountries{
    id:number
    country:string
}

export interface IContractDistinctCountriesState{
    contractDistinctCountriesList:IContractDistinctCountries[]
    // msg:string
}