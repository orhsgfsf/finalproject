import {Dispatch} from "redux"
import { CallHistoryMethodAction} from "connected-react-router"
import { IContractDistinctCountriesActions,  getContractDistinctCountriesSuccess } from "./actions";

export function getContractDistinctCountries(){
    return async(dispatch:Dispatch<IContractDistinctCountriesActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/contracts/contractDistinctCountries`,{
            headers: {
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }})
            const result = await res.json() 
            console.log(result)

            if (res.status !== 200){
                // dispatch(failed("GET_DISTINCT_COUNTRIES_FAILED", result.msg))
            }else{
                console.log(result.data)
                dispatch(getContractDistinctCountriesSuccess(result.data))
            }
        }
    }
    
