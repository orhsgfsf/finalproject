import { IContractDistinctCountries } from './state';



export function getContractDistinctCountriesSuccess(contractDistinctCountriesList: IContractDistinctCountries[]) {
    console.log(contractDistinctCountriesList)
    return {
        type: "GET_CONTRACT_DISTINCT_COUNTRIES_SUCCESS" as "GET_CONTRACT_DISTINCT_COUNTRIES_SUCCESS",
        contractDistinctCountriesList
    }
}

// export function addDistinctCountrySuccess(country:IDistinctCountries){
//     return {
//         type: "ADD_DISTINCT_COUNTRY_SUCCESS" as "ADD_DISTINCT_COUNTRY_SUCCESS",
//         country
//     }
// }

// export function deleteDistinctCountrySuccess(country:IDistinctCountries){
    
//     return {
//         type: "DELETE_DISTINCT_COUNTRY_SUCCESS" as "DELETE_DISTINCT_COUNTRY_SUCCESS",
//         country
//     }
// }

// type FAILED = "GET_DISTINCT_COUNTRIES_FAILED"

// export function failed(type: FAILED, msg: string) {
//     return{
//         type,
//         msg
//     }
// }

type contractDistinctCountriesListActionCreators = typeof getContractDistinctCountriesSuccess 
export type IContractDistinctCountriesActions = ReturnType<contractDistinctCountriesListActionCreators>
    
