import React from 'react';
import {Container, Grid } from '@material-ui/core';
import PersonalForm from './PersonalForm';
import { RouteComponentProps } from 'react-router-dom';
import Logout from './logout';


export default class PersonalInfo extends React.Component<RouteComponentProps<{}>>{
    public render() {
        return (
            <div className='pri-block'>
                <Container>
                    <Grid item xs={12}>
                        <div>
                            <Logout />
                        </div>
                        <br></br>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="primary3">Personal Information</div>
                    </Grid>
                   
                   
                    <Grid item xs={12}>
                        
                        <PersonalForm />
                    </Grid>
                    
                </Container>
            </div>
        )
    }
}