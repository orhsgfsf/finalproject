import { IPropertyState } from "./state";

export function getPropertySuccess(property: IPropertyState){
    return {
        type: "GET_PROPERTY_SUCCESS" as "GET_PROPERTY_SUCCESS",
        property
    }
}


type FAILED = "GET_PROPERTY_FAILED"

export function failed(type:FAILED, msg:string){
    return {
        type,
        msg
    }
}


type propertyActionCreators = typeof getPropertySuccess | typeof failed
export type IPropertyActions = ReturnType<propertyActionCreators>