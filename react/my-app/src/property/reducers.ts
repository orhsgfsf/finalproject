import { IPropertyState } from './state'
import { IPropertyActions } from './actions'

const initialState: IPropertyState = {

        address:"",
        country:"",
        propertyType:"",
        gross_floor_area:0,
        saleable_area:0,
        government_rent:0,
        management_fee:0,
        currency:"",
        rates:0,
        reminder:"",
        deposit:0,
        country_id: 0,
        floor_plan:""
       
  

}

export function propertyReducer(state: IPropertyState = initialState, action: IPropertyActions) {
    switch (action.type) {
        case "GET_PROPERTY_SUCCESS":
            console.log(action.property)
            return {
                ...state,
                ...action.property
            }
        case "GET_PROPERTY_FAILED":
            return {
                ...state,
                msg: action.msg
            }

        default:
            return state
    }
}