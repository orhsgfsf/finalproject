import { Dispatch } from "redux";
import { getPropertySuccess, failed, IPropertyActions } from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";


export function getProperty(id:number){
    return async (dispatch:Dispatch<IPropertyActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/properties/toGetProperties`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        const result = await res.json()
        if (res.status !== 200) {
            dispatch(failed('GET_PROPERTY_FAILED',result.msg))
        }else{
            // dispatch(addPropertySuccess({
            //     id: result.data[0].id,
            //     address: propertyInformation.address,
            //     country: propertyInformation.country
            // }))
            // dispatch(addDistinctCountrySuccess({
            //     country: propertyInformation.country
            // }))
            
            dispatch(getPropertySuccess(result.data[0]))
            console.log(result.data[0])
            // dispatch(push(`/menu/properties/property/${result.data[0].id}`))
        }
    }
}

// export function getPropertyForEdit(id:number){
//     return async (dispatch:Dispatch<IPropertyActions|CallHistoryMethodAction>) => {
//         const res = await fetch (`${process.env.REACT_APP_API_SERVER}/properties/toGetProperties`, {
//             method: "POST",
//             headers: {
//                 "Content-type" : "application/json",
//                 "Authorization" : `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({id})
//         })
//         const result = await res.json()
//         if (res.status != 200) {
//             dispatch(failed('GET_PROPERTY_FAILED',result.msg))
//         }else{
//             // dispatch(addPropertySuccess({
//             //     id: result.data[0].id,
//             //     address: propertyInformation.address,
//             //     country: propertyInformation.country
//             // }))
//             // dispatch(addDistinctCountrySuccess({
//             //     country: propertyInformation.country
//             // }))
            
//             dispatch(getPropertySuccess(result.data[0]))
//             dispatch(push(`/menu/properties/edit_property/${result.data[0].id}`))
//         }
//     }
// }
