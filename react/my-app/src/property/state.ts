
export interface IPropertyState{
        address:string
        country:string
        propertyType:string
        gross_floor_area:number
        saleable_area:number
        government_rent:number
        management_fee:number
        currency:string
        rates:number
        reminder:string
        deposit:number
        country_id:number
        floor_plan:string
}