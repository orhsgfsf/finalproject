import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import './property.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileSignature } from '@fortawesome/free-solid-svg-icons';



export default class ContractInterface extends React.Component {
    public render() {

        return (
            <div className='primary'>
                <Container>  <video id="background-video" autoPlay loop>
                <source src="https://cdn.flixel.com/flixel/tgqtzsqactco3i3stjqv.hd.mp4" type="video/mp4" />

                Your browser does not support the video tag.
            </video>

                    <Row>
                        <Col md="12" >
                            <div>
                                <h5>Please check the contract status on your left </h5>
                                <h5> and create a new contract by &nbsp;{<FontAwesomeIcon icon={faFileSignature} size="1x" />}&nbsp;button.</h5>
                            </div>
                        </Col>
                    </Row>

                </Container>

            </div>
        )
    }
}


