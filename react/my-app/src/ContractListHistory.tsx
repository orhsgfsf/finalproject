import React from 'react'
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
// import { IDistinctUnitsList } from './distinctUnitsList/state';
import { IUnit } from './unit/state';
import { getUnit } from './unit/thunks';
import { getContractByPropertyId } from './contractList/thunks';
// import { IBriefContracts } from './briefContract/state';
import { IContractList } from './contractList/state';
import { Col, Row, Button } from 'reactstrap';
import { push } from 'connected-react-router';
import { pdf } from './contractDraft/thunks';
import moment from 'moment'

interface IContractListHistoryProps {
    match: {
        params: {
            id: string
        }
    }

    contractList: IContractList[]
    units: IUnit[]

    getUnit: (property_id: number) => void
    getContractByPropertyId: (property_id: number) => void

    viewContract: (code: string) => void
    downloadContract: (code: string) => void

}


class ContractListHistory extends React.Component<IContractListHistoryProps>{

    private viewContract = (code: string) => {
        this.props.viewContract(code)
    }

    private downloadContract = (code: string) => {
        this.props.downloadContract(code)
    }

    public componentDidMount() {
        this.props.getUnit(parseInt(this.props.match.params.id))
        this.props.getContractByPropertyId(parseInt(this.props.match.params.id))
        console.log(this.props.match.params.id)

    }

    public render() {

        console.log(this.props.contractList)
        console.log(this.props.units)
        return (
            <div>{this.props.units.length === 0 && <div>No Unit here!</div>}
                <div>{this.props.units.map(unit =>
                    <div className="contract-list-block">
                        <Row>
                            <Col md="12">
                                Unit: {unit.unit}
                            </Col>

                            {this.props.contractList.filter(contract => (contract.unit_id === unit.id && contract.contract_status === false && contract.confirm_clicked === true)).length === 0 ? <div><Col md="12">Empty</Col></div> :
                                this.props.contractList.filter(contract => (
                                    contract.unit_id === unit.id)).map(filteredContract =>


                                        <Col md="12">
                                            {
                                                filteredContract.contract_status === false &&
                                                filteredContract.confirm_clicked === true &&
                                                <div>

                                                    <div className="history-name">History</div>
                                                    <div>Tenant Name: {filteredContract.tenant_name}</div>
                                                    <div>Contract Starting Date: {moment(filteredContract.contract_starting_date).format("YYYY-MM-DD")}</div>
                                                    <div>Contract Expiry Date: {moment(filteredContract.contract_expiry_date).format("YYYY-MM-DD")}</div>
                                                    {filteredContract.void_contract_date !== null && <div>Date to Void Contract: <div color="red">{moment(filteredContract.void_contract_date).format("YYYY-MM-DD")}</div></div>}
                                                    {/* <div>Contract URL: {`https://localhost:3000/contract/${filteredContract.code}`}</div> */}
                                                    <div>Contract URL: {`https://we-vital.com/contract/${filteredContract.code}`}</div>

                                                    <div>
                                                    <div className="view-download-button">
                                                        <Row>
                                                           
                                                            <Col md="6">
                                                                <Button onClick={() => this.viewContract(filteredContract.code)}>Preview</Button>
                                                            </Col>
                                                            <Col md="6">
                                                                <Button onClick={() => this.downloadContract(filteredContract.code)}>Download</Button>
                                                            </Col>
                                                            
                                                        </Row>
                                                        </div>
                                                    </div>

                                                </div>
                                            }
                                            <br />
                                        </Col>


                                    )
                            }


                        </Row>

                    </div>




                )}</div></div>

        )

    }

}

const mapStateToProps = (state: IRootState) => ({
    contractList: state.contractList.contractList,
    units: state.unit.units
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    getUnit: (property_id: number) => dispatch(getUnit(property_id)),
    getContractByPropertyId: (property_id: number) => dispatch(getContractByPropertyId(property_id)),

    viewContract: (code: string) => dispatch(push(`/contract_preview/${code}`)),
    downloadContract: (code: string) => dispatch(pdf(code)),

})

export default connect(mapStateToProps, mapDispatchToProps)(ContractListHistory)