import {IPropertyEditState} from './state'
import {IPropertyEditActions} from './actions'

const initialState: IPropertyEditState = {

    address:"",
    country:"",
    propertyType:"",
    gross_floor_area:0,
    saleable_area:0,
    government_rent:0,
    management_fee:0,
    currency:"",
    rates:0,
    deposit:0,
    reminder:""

}



export function propertyEditReducer(state: IPropertyEditState = initialState, action: IPropertyEditActions) {
    switch (action.type) {
       
        case "GET_PROPERTY_FOR_EDIT_SUCCESS":
              
            console.log(action.propertyForEdit)
            return {
                ...state,
                ...action.propertyForEdit
            }
        case "CHANGE_VALUE":
            
            return {
                ...state,
                ...action.x
            }
        
        case "GET_PROPERTY_FOR_EDIT_FAILED":
            return {
                ...state,
                msg: action.msg
            }

        default:
            return state
    }
}