export interface IPropertyEditState{
    address:string
    country:string
    propertyType:string
    gross_floor_area:number
    saleable_area:number
    government_rent:number
    management_fee:number
    currency:string
    rates:number
    deposit:number
    reminder:string
}