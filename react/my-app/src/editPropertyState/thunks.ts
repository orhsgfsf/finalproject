import { Dispatch } from "redux";
import { getPropertyForEditSuccess, failed, IPropertyEditActions} from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";



export function getPropertyForEdit(id:number){
    return async (dispatch:Dispatch<IPropertyEditActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/properties/toGetProperties`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({id})
        })
        const result = await res.json()
        if (res.status !== 200) {
            dispatch(failed('GET_PROPERTY_FOR_EDIT_FAILED',result.msg))
        }else{
            // dispatch(addPropertySuccess({
            //     id: result.data[0].id,
            //     address: propertyInformation.address,
            //     country: propertyInformation.country
            // }))
            // dispatch(addDistinctCountrySuccess({
            //     country: propertyInformation.country
            // }))
            
            dispatch(getPropertyForEditSuccess(result.data[0]))
            dispatch(push(`/menu/properties/edit_property/${result.data[0].id}`))
        }
    }
}