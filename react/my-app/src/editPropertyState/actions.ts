import {IPropertyEditState} from './state'


export function getPropertyForEditSuccess(propertyForEdit: IPropertyEditState){
    return {
        type: "GET_PROPERTY_FOR_EDIT_SUCCESS" as "GET_PROPERTY_FOR_EDIT_SUCCESS",
        propertyForEdit
    }
}

export function changeValue(x:any){
    return {
        type: "CHANGE_VALUE" as "CHANGE_VALUE",
        x
    }
}




type FAILED = "GET_PROPERTY_FOR_EDIT_FAILED"

export function failed(type:FAILED, msg:string){
    return {
        type,
        msg
    }
}

type propertyEditActionCreators =  typeof getPropertyForEditSuccess | typeof changeValue | typeof failed

export type IPropertyEditActions = ReturnType<propertyEditActionCreators>

