import React from 'react';
// import Ripples from 'react-ripples';
import { Grid } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { getBalanceToCheck } from './notification/thunks';
import { connect } from 'react-redux';
import CheckRentModal from './CheckRentModal'
import { Input, Button } from 'reactstrap';
import { addImage } from './addImage/thunks';
import profile1 from "./Image/profile1.png";
import profile2 from "./Image/profile2.png";

interface IDashboardState {
   
        // rent_due_date: Date | null
        // rent_price: number
        // currency: string
        // unit: string
        // full_address: string
        // country: string

        // todo: string 
        // todo_status: string 

        file:File | null

        
}

interface IDashboardProps {
    notification:{
        rent_due_date: Date | null
        rent_price: number
        currency: string
        unit: string
        full_address: string
        country: string
    }[]

    today:{
        todo: string
        todo_status: string
    }
    getBalanceToCheck: () => void
    addImage: (file:File) => void
}


class Dashboard extends React.Component <IDashboardProps,IDashboardState > {
    constructor(props:IDashboardProps){
        super(props)


            this.state = {
                file:null
            }
        
    }

    private handleFileOnChange = (files:FileList) => {
        console.log(files)
        this.setState({
            file:files[0]
        })
    }

    private testClick = () => {
        if (this.state.file){
            this.props.addImage(this.state.file)
        }
    }

    public componentDidMount() {
        this.props.getBalanceToCheck()
    }
    
    public render() {
        console.log(this.props.notification[0])
        console.log(this.props.today)
        return (
            <div>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                   
                    </Grid>
                    <Grid item xs={6}>
                       
                    </Grid>

                    <Grid item xs={12}>
                        <Grid item xs={8}>

                            {<CheckRentModal value={this.props.notification}/>}
                            <Input type='file' name="pic" onChange={(e)=>this.handleFileOnChange(e.target.files as FileList)}/>
                           <Button onClick={this.testClick}>Click</Button>
                            {/* <img src="https://www.dccomics.com/sites/default/files/Char_GetToKnow_Batman80_5ca54cb83a27a6.53173051.png" alt=''/> */}
                            {/* <img src = "./" alt=''/> */}
                            <img className="profile" src={profile1} alt=''/>
                            <img className="profile" src={profile2} alt=''/>
                        </Grid>
                        <Grid item xs={4}>
                          
                        </Grid>
                    </Grid>
                </Grid>
           
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    notification: state.notification.notification,
   
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getBalanceToCheck: () => dispatch(getBalanceToCheck()),
        addImage: (file:File) => dispatch(addImage(file))
    });
}

export default connect (mapStateToProps, mapDispatchToProps)(Dashboard)