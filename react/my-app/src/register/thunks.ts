import { Dispatch } from "redux";
import { IRegisterActions, registerFailed, registerSuccess} from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";

export function register(username:string, password:string, confirmPassword:string, email:string){
    return async (dispatch:Dispatch<IRegisterActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/auth/register`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({username, password, confirmPassword, email})
        })
       
        const result = await res.json()
        console.log(result)
        if (res.status !== 200){
            dispatch(registerFailed(result.msg))
        }else{
            console.log("test success!!")
            dispatch(registerSuccess(result.status))
            dispatch(push('/login'))
        }
    }
}