import { IRegisterActions } from './actions'
import { IRegisterState } from './state'

const initialState = {
    msg: {},
    status: {}
}

export function registerReducer(state: IRegisterState = initialState, action: IRegisterActions) {
    switch (action.type) {
        case "REGISTER_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        case "REGISTER_SUCCESS":
            return {
                ...state,
                status: action.status
            }
        case "CLEAR_VALUE":
            return{
                ...state,
                status: initialState.status
            }
        default:
            return state
    }


}

