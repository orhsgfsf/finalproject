export function registerFailed(msg:{}){
    return {
        type: "REGISTER_FAILED" as "REGISTER_FAILED",
        msg
    }
}

export function registerSuccess(status:{}){
    return {
        type: "REGISTER_SUCCESS" as "REGISTER_SUCCESS",
        status
    }
}


export function clearStatusSuccess(){
    return {
        type: "CLEAR_VALUE" as "CLEAR_VALUE"
    }
}



type RegisterActionCreators = typeof registerFailed | typeof registerSuccess | typeof clearStatusSuccess
export type IRegisterActions = ReturnType<RegisterActionCreators>