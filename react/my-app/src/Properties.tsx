import * as React from 'react'
import { Container, Row, Col } from 'reactstrap';
import PropertiesList from './PropertiesList';
import { Switch, Route } from 'react-router-dom';
import CreateProperty from './CreateProperty';
import PropertyInterface from './PropertyInterface';
import Property from './Property';
import moment from 'moment'
import PropertyEdit from './PropertyEdit';
import UnitCreate from './UnitCreate';
import UnitEdit from './UnitEdit';

export default class Properties extends React.Component {
    
    public render() {
   
        return (
            <div>
                <Container>
                    <Row>
                    
                    </Row>
                    <br></br>
                    <Row>
                        <Col md="12" className="primary1">
                            Date: {moment().format("DD-MM-YYYY")}
                        </Col>
                    </Row>
                    <br></br>
                    <Row>
                        <Col md="4">
                            <PropertiesList />
                        </Col>
                        <Col md="8">
                            
                            <Switch>

                                <Route path="/menu/properties/property_interface" component={PropertyInterface} />
                                <Route path="/menu/properties/create_property" component={CreateProperty} />
                                <Route path="/menu/properties/property/:id" component={Property} /> 
                                <Route path="/menu/properties/edit_property/:id" component={PropertyEdit}/>  
                                <Route path="/menu/properties/unit/:id" component={UnitCreate}/>       
                                <Route path="/menu/properties/edit_unit/:id" component={UnitEdit}/>                  
                            </Switch>
                        </Col>
                    </Row>
                </Container>
           
            </div>
        )
    }
}


