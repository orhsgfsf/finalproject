import { Button } from "reactstrap";
import * as React from 'react';
import { IRootState } from "./store";

import { ThunkDispatch } from "./store";
import { logout } from "./auth/thunks";
import { connect } from "react-redux";
import { createRipples } from 'react-ripples';
import { updateLoginTime } from "./userData/thunks";

const MyRipples = createRipples({
    color: "rgba(255, 254, 254, 0.33)",
    during: 1000,
  })

interface ILoginProps{
    isAuthenticated: boolean
    logout: () => void
    updateLoginTime: (loginTime:string) => void
}

class Logout extends React.Component<ILoginProps,{}> {

    
   
    private logout = ()=>{
        const loginTime = localStorage.getItem('loginTime')
        console.log(loginTime)
        if (loginTime !== null){
            this.props.updateLoginTime(loginTime)
        }
      
        this.props.logout();
        
    }

    public render() {
        return (
            <MyRipples>
        <div className="logout-bar">
            {
                this.props.isAuthenticated?
                <Button color="info" onClick={this.logout}>Logout</Button>:
                ""
            }
           
        </div>
        </MyRipples>
        );
    }
}

const mapStateToProps = (state:IRootState)=>({
    isAuthenticated:  state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch:ThunkDispatch)=>({
    logout: ()=> dispatch(logout()),
    updateLoginTime: (loginTime:string) => dispatch(updateLoginTime(loginTime))
})


export default connect(mapStateToProps,mapDispatchToProps)(Logout);
