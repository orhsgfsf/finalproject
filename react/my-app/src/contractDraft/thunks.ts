import { Dispatch } from 'redux'
import { push, CallHistoryMethodAction } from "connected-react-router"
import { getContractByCodeSuccess, IContractDraftActions } from './actions';
import { ThunkDispatch } from '../store';


export function getContractByCode(code: string) {
    return async (dispatch: Dispatch<IContractDraftActions | CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/tenantContracts/toGetContractByCode`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
                // "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({ code })
        })
        const result = await res.json()
        console.log(result)

        if (res.status !== 200) {
            // dispatch(failed("GET_DISTINCT_COUNTRIES_FAILED", result.msg))
        } else {
            console.log(result.data)
            dispatch(getContractByCodeSuccess(result.data[0]))
        }
    }
}

export function addContractDraft(contractInformation: {}) {
    return async (dispatch: ThunkDispatch) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/contracts/contractDraft`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-type": "application/json"
                // "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(contractInformation)
        })
        console.log("done!")
        dispatch(push('/menu/contract'))
    }

}

export function confirmContractByTenant(contractUpdate: {}, id:string) {
    return async (dispatch: ThunkDispatch) => {
        await fetch(`${process.env.REACT_APP_API_SERVER}/tenantContracts/toSignContract`, {
            method: "PATCH",
            headers: {
                "Content-type": "application/json"

            },
            body: JSON.stringify(contractUpdate)
        })
        dispatch(push(`/sign_contract_success/${id}`))
    }
}

export function pdf(code:string){
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/tenantContracts/contractPdf`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"

            },
            body: JSON.stringify({code})
            
        })
        const result = await res.json()
        window.location.href = `${result.data}`
        
    }
}
