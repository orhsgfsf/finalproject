import {IContractDraftActions} from './actions'
import {IContractDraftState} from './state'

const initialState:IContractDraftState = {
    id:0,
    rent_due_date: 0, 
    grace_period: 0, 
    contract_starting_date: new Date(),
    contract_expiry_date: new Date(),
    rent_price: 0,
    deposit: 0, 
    keys: 0,
    landlord_id_or_br: "",
    currency:"",
    landlordSignature: "",
    code:"",   
    landlord_name:"",
    grace_period_expiry_date: new Date(),
    grace_period_starting_date: new Date(),
    landlord_signature: "",
    propertyType:"",
    unit:"",
    unit_id:0,
    country:"",
    address:"",
    management_fee:0,
    government_rent:0,
    rates:0,
    saleable_area:0,
    gross_floor_area:0,
    tenant_name: null,
    contact_person: null,
    contact_number: null,
    tenant_id_or_br: null,
    tenant_business_nature: null,
    tenant_signature: null
}

export function contractDraftReducer(state:IContractDraftState = initialState, action:IContractDraftActions):IContractDraftState{
    console.log("WTF?")
    switch (action.type){
        case "GET_CONTRACT_DRAFT_SUCCESS":
            console.log(state)
            console.log(action.contractDraft)
            return {
                ...state,
                ...action.contractDraft
            }
            default:
                return state
        }
}