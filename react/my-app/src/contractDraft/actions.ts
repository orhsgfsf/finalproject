import {IContractDraftState} from './state'

export function getContractByCodeSuccess (contractDraft:IContractDraftState){ 
    console.log("XXXXXX")  
    return {
        type: "GET_CONTRACT_DRAFT_SUCCESS" as "GET_CONTRACT_DRAFT_SUCCESS",
        contractDraft
    }
}

type contractDraftActionCreators = typeof getContractByCodeSuccess
export type IContractDraftActions = ReturnType<contractDraftActionCreators>