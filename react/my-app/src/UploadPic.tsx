import React from 'react'
import { ThunkDispatch, IRootState } from './store';
import { Input, Label } from 'reactstrap';
import { addImage } from './addImage/thunks';
import { connect } from 'react-redux';




interface IIconProps {
  image: string

  addImage:(file:File) => void
}
interface IIconState {
  file: File | null
}

export interface IIcon {
  image: string
}
class UploadPic extends React.Component<IIconProps, IIconState>{




  private handleFileOnChange = (files:FileList) => {
    this.props.addImage(files[0])
  }
  render() {

    return (
      <div>
        <div>
          {this.props.image === "" ? <div className="icon-block">Profile Picture</div> : <img className="icon" src={this.props.image} />}

        </div>
        <div>
          <Input type='file' id={"file"} name="pic" style={{ display: 'none' }} onChange={(e) => this.handleFileOnChange(e.target.files as FileList)} />
          <div className="upload-image-button"><Label htmlFor={"file"}>Upload Profile Piceure</Label></div>
        </div>
      </div>

    )
  }

}



const mapStateToProps = (state: IRootState) => ({
  image: state.image.image
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  addImage:(file:File) => dispatch(addImage(file))
})

export default connect (mapStateToProps,mapDispatchToProps)(UploadPic)