import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Logo from "./Image/sakura.png";
import { useSelector, useDispatch } from 'react-redux';
import { ThunkDispatch, IRootState } from './store';
import { push } from 'connected-react-router';
import { getUserData } from './userData/thunks';
import Logout from './logout';
import { createRipples } from 'react-ripples';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileSignature, faBuilding, faPencilRuler, faCircle } from '@fortawesome/free-solid-svg-icons';

import thief from "./Image/Seanau-User-Thief.png";


const MyRipples = createRipples({
  color: "rgba(255, 254, 254, 0.33)",
  during: 1000,
})

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function Menu(props: {}) {
  const classes = useStyles()
  const loginData = useSelector((state: IRootState) => state.userData.userData)
  console.log(loginData)
  const [state, setState] = React.useState({

    left: false,

  });

  const dispatch = useDispatch<ThunkDispatch>()

  useEffect(() => {
    // async function getUser() {
    // await console.log("Run this effect")
    dispatch(getUserData());
    //   await console.log("Run this effect")
    //   await console.log("Run this effect")

    //   await console.log(loginData)
    //   await loginData.first_name === "" || loginData.first_name === null ? dispatch(push('/menu/properties/property_interface')) : dispatch(push('/menu/properties/property_interface'))
    // }
    // getUser()
  }, [dispatch])









  type DrawerSide = 'left' | 'bottom' | 'right';
  const toggleDrawer = (side: DrawerSide, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event &&
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  console.log(loginData)

  const sideList = (side: DrawerSide) => (

    <MyRipples>
      <div
        className={classes.list}
        role="presentation"
        onClick={toggleDrawer(side, false)}
        onKeyDown={toggleDrawer(side, false)}
      >



        <Divider />
        <div className="listlogo">
          <List>

            <img className="logo" src={Logo} alt="abc" />

          </List>
          <List>
            <b>WellCome!</b> <b>{loginData.first_name}</b>
          </List>
        
          <List>
            <div className="lastLogin">
              <div className="lastLogin2">
                Last login: <br /> {loginData.login_time !== null ? loginData.login_time : "This is the First Time to Login!"}
              </div>
            </div>
          </List>
        </div>
        <List>
          <ListItem button key="Properties" onClick={() => {
            dispatch(push('/menu/properties/property_interface'))
          }}>
            <ListItemIcon>{<FontAwesomeIcon icon={faBuilding} size="2x" />}</ListItemIcon>
            <ListItemText primary="Properties" />
          </ListItem>
          <ListItem button key="Contract" onClick={() => { dispatch(push('/menu/contract/contract_interface')) }}>
            <ListItemIcon>{<FontAwesomeIcon icon={faFileSignature} size="2x" />}</ListItemIcon>
            <ListItemText primary="Contract" />
          </ListItem>
          <ListItem button key="Floor Plan" onClick={() => dispatch(push('/menu/floor_plan'))}>
            <ListItemIcon>{<FontAwesomeIcon icon={faPencilRuler} size="2x" />}</ListItemIcon>
            <ListItemText primary="Floor Plan" />
          </ListItem>
          {/* <ListItem button key="Annual Report" onClick={() => dispatch(push('/annual-report'))}>
          <ListItemIcon>{<FontAwesomeIcon icon={faFileExcel} size = "2x"/>}</ListItemIcon>
          <ListItemText primary="Annual Report" />
        </ListItem> */}

          <br />
          <div className="center-block">

            {(loginData.profile_picture !== null && loginData.profile_picture !== "") &&
              <div className="menu-profile-block">
                <img className="menu-profile-picture" src={loginData.profile_picture}  alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))}/>
                <div className="profile-pic">
                  {/* {loginData.gender === "Female" && <img className="profile" src={profile1} alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))} />} */}
                  {/* {loginData.gender === "Male" && <img className="profile" src={profile2} alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))} />} */}
                </div>
      </div>} 
              {(loginData.profile_picture === "" || loginData.profile_picture === null) &&
              <div className="menu-profile-block">
                <img className="menu-profile-picture" src={thief} alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))} />
                <div className="profile-pic">
                  {/* {loginData.gender === "Female" && <img className="profile" src={profile1} alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))} />} */}
                  {/* {loginData.gender === "Male" && <img className="profile" src={profile2} alt='' onClick={() => dispatch(push('/menu/personal_information_edit'))} />} */}
                </div>
              </div>}

          </div>

        

          {/* {['Properties', 'Contract', 'Floor Plan', 'Annual Report'].map((text, index) => (
        //   <ListItem button key={text} onClick={() => dispatchEvent()}>
        //     <ListItemIcon>{<InboxIcon />}</ListItemIcon>
        //     <ListItemText primary={text} />
        //   </ListItem>
        // ))} */}
          <List>

          <List>
            <div className="indicator">
              <div className="lastLogin3">
                Indicators<br></br>
          
              {<FontAwesomeIcon icon={faCircle} size="1x" color='red'/>} Vacant/Exclamation <br></br>
     
              {<FontAwesomeIcon icon={faCircle} size="1x" color='yellow'/>} Pending <br></br>
            
              {<FontAwesomeIcon icon={faCircle} size="1x" color ='green'/>} Rented<br></br>
              
              </div>
            </div>
          </List>

          </List>

          <div className="logout-button"><Logout /></div>
        </List>

        <div>

        </div>
      </div>
    </MyRipples>
  );

  //   const fullList = (side: DrawerSide) => (
  //     <div
  //       className={classes.fullList}
  //       role="presentation"
  //       onClick={toggleDrawer(side, false)}
  //       onKeyDown={toggleDrawer(side, false)}
  //     >
  //       <List>
  //         {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
  //           <ListItem button key={text}>
  //             <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
  //             <ListItemText primary={text} />
  //           </ListItem>
  //         ))}
  //       </List>
  //       <Divider />
  //       <List>
  //         {['All mail', 'Trash', 'Spam'].map((text, index) => (
  //           <ListItem button key={text}>
  //             <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
  //             <ListItemText primary={text} />
  //           </ListItem>
  //         ))}
  //       </List>
  //     </div>
  //   );

  return (
    <div>
      <Button onClick={toggleDrawer('left', true)}><img className="logo" src={Logo} alt="abc" /></Button>

      <SwipeableDrawer
        open={state.left}
        onClose={toggleDrawer('left', false)}
        onOpen={toggleDrawer('left', true)}
      >

        {sideList('left')}
      </SwipeableDrawer>


    </div>
  );
}