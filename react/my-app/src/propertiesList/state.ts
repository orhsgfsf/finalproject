export interface IPropertiesList {
    id:number
    address?:string
    country?:string
}

export interface IPropertiesListState{
    propertiesList: IPropertiesList[]
    msg:string
   
}
