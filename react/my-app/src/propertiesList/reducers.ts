import { IPropertiesListState } from './state'
import { IPropertiesListActions } from './actions'

const initialState: IPropertiesListState = {
    propertiesList: [],
    msg: "",

}

export function propertiesListReducer(state: IPropertiesListState = initialState, action: IPropertiesListActions) {
    switch (action.type) {

        case "GET_PROPERTIES_LIST_SUCCESS":
            console.log(state)
            return {
                ...state,
                propertiesList: action.propertiesList
            }
        case "ADD_PROPERTY_SUCCESS":
            return {
                ...state,
                propertiesList: [...state.propertiesList, action.property]
            }
        case "DELETE_PROPERTY_SUCCESS":
            console.log(state.propertiesList)
            console.log(typeof action.property.id)
        return {
                ...state,
                propertiesList: state.propertiesList.filter(property => property.id !== action.property.id)
            }
        case "GET_PROPERTIES_LIST_FAILED":
            return {
                ...state,
                msg: action.msg
            }
        default:
        return state
    }
}


