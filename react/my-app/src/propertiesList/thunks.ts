import { Dispatch } from "redux";
import { IPropertiesListActions,  failed, getPropertiesListSuccess} from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";


export function getPropertiesList(){
    return async (dispatch:Dispatch<IPropertiesListActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/properties/numberOfProperties`, {
            headers: {
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }})
        const result = await res.json()

        if (res.status !== 200) {
            dispatch(failed("GET_PROPERTIES_LIST_FAILED",result.msg))
        }else {
            if (result.data.length > 0){
                dispatch(getPropertiesListSuccess(result.data))
            }
        }
    }     
}

