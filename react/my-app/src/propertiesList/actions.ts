import { IPropertiesList } from "./state";

export function getPropertiesListSuccess(propertiesList: IPropertiesList[]) {
    console.log(propertiesList)
    return {
        type: "GET_PROPERTIES_LIST_SUCCESS" as "GET_PROPERTIES_LIST_SUCCESS",
        propertiesList
    }
}

export function addPropertySuccess(property: IPropertiesList){
    return {
        type: "ADD_PROPERTY_SUCCESS" as "ADD_PROPERTY_SUCCESS",
        property
    }
}

export function deletePropertySuccess(property:IPropertiesList){
    return {
        type: "DELETE_PROPERTY_SUCCESS" as "DELETE_PROPERTY_SUCCESS",
        property
    }
}




type FAILED = "GET_PROPERTIES_LIST_FAILED" 

export function failed(type: FAILED, msg: string) {
    return {
        type,
        msg
    }
}

type PropertiesListActionCreators =  typeof getPropertiesListSuccess  | typeof addPropertySuccess | typeof deletePropertySuccess | typeof failed

export type IPropertiesListActions = ReturnType<PropertiesListActionCreators>

