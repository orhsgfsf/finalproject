import { Dispatch } from "redux";
import { IAuthActions, loginFailed, loginSuccess, logoutSuccess } from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";
import moment from 'moment'

export function login(username:string, password:string){
    return async (dispatch:Dispatch<IAuthActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/auth/login`,{
            method: "POST",
            headers:{
                "Content-Type":"application/json"
                
            },
            body: JSON.stringify({username, password})
        })

        const result = await res.json()
        if (res.status !== 200){
            dispatch(loginFailed(result.msg))
        }else{
            localStorage.setItem('token', result.token)
            localStorage.setItem('loginTime', moment().format('Do MMMM YYYY, h:mm:ss a'))            
            dispatch(loginSuccess())
            dispatch(push('/home'))
        }
    }

}

export function loginFacebook(accessToken:string){
    return async (dispatch:Dispatch<IAuthActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/auth/facebook`, {
            method: "POST",
            headers: {
                "content-type":"application/json"
            },
            body: JSON.stringify({accessToken})
        })

        const result = await res.json()

        if (res.status === 200){
            localStorage.setItem('token', result.token)
            localStorage.setItem('loginTime', moment().format('Do MMMM YYYY, h:mm:ss a'))
            dispatch(loginSuccess())
            dispatch(push('/home'))
        }else{
            dispatch(loginFailed(result.msg))
        }
    }
}

export function logout() {
    return async (dispatch:Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        
        localStorage.removeItem('token')
        localStorage.removeItem('loginTime')
        dispatch(logoutSuccess())
        dispatch(push('/login'))
    }
}