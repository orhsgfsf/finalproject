import React from 'react'
import { Container, InputGroupAddon, Col, Row, Button } from 'reactstrap';
import moment from 'moment'
import DatePicker from "react-datepicker";
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { IContractDraftState } from './contractDraft/state';
import { getContractByCode, addContractDraft } from './contractDraft/thunks';
import { deleteContract, activateContractPending } from './contractList/thunks';
const SignatureCanvas = require('react-signature-canvas').default

interface IContractReRentProps {
    getContractByCode: (code:string) => void
    addContractDraft: (contractInformation:IContractReRentDetails) => void
    deleteOriginalContract: (id:number, unit_id:number) => void
    activateContractPending: (id:number) => void

    contractDraft: IContractDraftState

    match:{
        params:{
            id:string
        }
    }

    history:any
}

interface IContractReRentState{

        rentDueDate: string | null
        rentPrice: string | null
        deposit: string | null
        keys: string | null 
    
            startContract: any
            endContract: any
            startGracePeriod: any
            endGracePeriod: any
           
          
}

interface IContractReRentDetails{
    rentDueDate: number | null
    gracePeriod: number | null
    startContract: string
    endContract: string
    rentPrice: number
    deposit: number 
    keys: number
    IdOrBrNumber: string
    currency:string
    landlordSignature: string
    code:string
    unitId:number
    landlordName:string
    startGracePeriod: string
    endGracePeriod: string
    tenantName: string | null
    tenantIdOrBr: string | null
    contactPerson: string | null
    contactNumber: number | null
    businessNature: string | null
    
}

class ContractReRent extends React.Component<IContractReRentProps,IContractReRentState>{
    constructor(props:IContractReRentProps){
        super(props)
        
        this.state = {
            rentDueDate: "",
            startContract: "",
            endContract: "",
            startGracePeriod: "",
            endGracePeriod: "",
            rentPrice: "",
            deposit: "",
            keys: ""          
        }
    }
    
    sigPadForLandLord:any = {}

    private onChange = (field: "rentDueDate" | "rentPrice" | "deposit" | "keys" , e: React.FormEvent<HTMLInputElement>) => {
        let state:any = {}
        state[field] = e.currentTarget.value
        this.setState(state)
    }
    private handleChange = (field: "startGracePeriod" | "endGracePeriod"| "startContract" | "endContract", date:any) => {
        let state:any = {}
        state[field] = date
        this.setState(
          state
        );
    }

    private onConfirmByLandLord = async() => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 50; i++) {
            result += await characters.charAt(Math.floor(Math.random() * charactersLength));
        }
       
        const contractInformation:IContractReRentDetails = {
           
            rentDueDate: (this.state.rentDueDate != null ? parseInt(this.state.rentDueDate): 0),
            gracePeriod: moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days'),
            startContract: this.state.startContract,
            endContract: this.state.endContract,
            rentPrice: (this.state.rentPrice != null ? parseInt(this.state.rentPrice): 0),
            deposit: (this.state.deposit != null ? parseInt(this.state.deposit): 0),
            keys: (this.state.keys != null ? parseInt(this.state.keys): 0),
            IdOrBrNumber: this.props.contractDraft.landlord_id_or_br,
            landlordName: this.props.contractDraft.landlord_name,
            currency: this.props.contractDraft.currency,
            landlordSignature: this.sigPadForLandLord.toDataURL('image/png'),
            code: result,
            unitId: this.props.contractDraft.unit_id,
            startGracePeriod: this.state.startGracePeriod,
            endGracePeriod: this.state.endGracePeriod,
            tenantName: this.props.contractDraft.tenant_name,
            tenantIdOrBr: this.props.contractDraft.tenant_id_or_br,
            contactPerson: this.props.contractDraft.contact_person,
            contactNumber: this.props.contractDraft.contact_number,
            businessNature: this.props.contractDraft.tenant_business_nature

        }

        // If true
       
        await this.props.addContractDraft(contractInformation)
        await this.props.deleteOriginalContract(this.props.contractDraft.id, this.props.contractDraft.unit_id)
        await this.props.activateContractPending(this.props.contractDraft.unit_id)
    

    }

    public componentDidMount(){
        this.props.getContractByCode(this.props.match.params.id)
        
    }

    public render () {
        return (
            <div>
                <Container className="contract-background">
                    <div>
                        <h2>Tenancy Agreement</h2>

                     
                        <div className="contract">
                            <Row>
                                <Col md="12">
                                    <div>
                                        <h3>Activate Date</h3>
                                        
                                        The Landlord and the Tenant as more particularly declared in <b>Schedule I</b> that an Agreement made the date of  {moment(this.state.startContract).format("YYYY-MM-DD") !== "Invalid date" ? <b><u>{moment(this.state.startContract).format("YYYY-MM-DD")}</u></b> : "______________"}
                                       <br />
                                        {/* 業主和租客<b>(詳細資料詳列於附表一)</b>於<DatePicker selected={this.state.startDate} onChange={this.handleChange.bind(this, "startDate")}  dateFormat="yyyy/MM/dd" value={this.state.startDate}/>訂立本合約。 */}
                                               
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="12">
                                    <div>
                                    <h3>Terms</h3>
                                    <b>The Landlord and the Tenant shall follow the Terms and the Rent of the Premises as more particularly
                                described in Schedule I.
                                Both , the Landlord and the Tenant, agree to observe and perform the terms and
                                conditions as
                                below:</b>
                                <br />
                                {/* <b>業主和租客雙方須遵從詳列於<b>附表一</b>的租賃期限及租賃金額租入和租出該物業, 並同意遵守和履行下列條款。</b> */}
                                <br />
                                <b>1) </b>The Tenant shall pay to the Landlord the Rent in advance on the {this.state.rentDueDate ? <b><u>{this.state.rentDueDate}</u></b> : "_________"} day of each and every calendar month during the Term.</div>
                                {/* <b>1) </b>租客須在租期內每月份第<input type="text" value={this.state.rentDueDate} onChange={this.onChange.bind(this, "rentDueDate")}/>天上期繳付指定的租金予業主。 */}
                                <br />
                                <b>2) </b>The Tenant shall not make any additions or alteration to the Premises unless the Landlords acquire and accept the written consent.
                                {/* <b>2) </b>除非業主獲悉並同意書面通知, 不然租客不能對該物業作任何加建或改動。 */}
                                <br />

                                <b>3) </b>The Tenant shall not assign, transfer, sublet or part with the possession of the Premises or any part thereof to any other person. This tenancy shall be personal to the Tenant named herein. 
                                <br />                       
                                {/* <b>3) </b>租客不得轉讓、轉租或分租該物業或其他任何部分或將該物業或其他任何部分的佔用權讓予任何其他人等。此租約權益將為租客個人擁有。 */}
                                <br />
                                <b>4) </b>The Tenant shall comply with all ordinances, regulations and rules of Hong Kong and
                            shall
                            observe and perform the covenants, terms and conditions of the Deed fo Mutual Covenant
                            and
                            Sub-Deed of Mutual Covenant (if any) relating to the Premsises. The Tenant shall not
                            contravene any negative or restrictive covenants contained in the Government Lease(s)
                            under
                            which the Premises are held from the Government.
                            <br />
                                {/* <b>4) </b>租客須遵守香港一干法律條例和規則及該物業所屬的大廈有關的公契內的條款。租客亦不可違反屬該物業地段內的官批地契上的任何制約性條款。 */}
                                <br />
                                <b>5) </b>The Tenant shall pay to the Landlord the Security Deposit set out in Schedule I for
                            the due observance and performance of the terms and
                            conditions herein contained and on his part to be observed and performed. If the Rent and/
                            or any changes payable by the Tenant hereunder or any part
                            thereof shall be unpaid for seven (7) days after the same shall become payable (whether
                            legally demanded or not) or if the Tenant shall commit a breach of
                            any of the terms and conditions herein contained, it shall be lawful for the Landlord at any
                            time thereafter to rn-enter the Premises whereupon this Agreement shall
                            absolutely determine and the Landlord may deduct any loss or damage suffered by the Landlord
                            as a result of the Tenant's breach from the Security.
                            Deposit without prejudice to any other right of action or any remedy of the Landlord in
                            respect of such breach of the Tenant.
                            <br />
                                {/* <b>5) </b>租客須交予業主保証 (金額如附表一所列) 作為保証租客遵守及履行此租約上租客所需遵守及履行的條款的按金。若租客拖欠根據本合約需要支付的租金及/ 或其他款項超過七天
                            (無論有否以法律行動追討)
                            或若租客違反此合約內任何條款，業主可合法收回該物業而此租約將會被終止;
                            業主可從保証金內扣除因租客違約而業主所受的損失，而此項權利將不會影響業主因租客違約而可採取的其他合法行動的權利。
                            <br /> */}
                                <b>6) </b>the Tenant shall have rights to hold and enjoy the Premises during the Term
                            without any interruption by the Landlord.
                            <br />
                                {/* <b>6) </b>業主不可在租約期內干擾租客享用該物業。 */}
                                <br />
                                <b>7 </b>)The Landlord shall keep and maintain the structural parts of the Premises.
                            Provided that the Landlord's liability
                            shall not be incurred unless and until written notice of any defect or want of repair
                            has been given by the Tenant to the Landlord and the Landlord shall have failed to take
                            reasonable steps to repair and remedy the same after the lapse of a reasonable time from
                            the date of service of such notice.
                            <br />
                                {/* <b>7) </b>業主須保養及適當維修該物業內各主要結構部分。唯業主須在收到租客的書面要求後才會有責任在合理時限內將有關損壞維修妥當。 */}
                                <br />
                                <b>8) </b>The Stamp Duty payable on this Agreement in duplicate shall be shared in equal
                            shares by both the Landlord and the Tenant.
                            <br />
                                {/* <b>8) </b>此合約為一式兩份, 業主和租客共同負責印花稅一半費用。 */}
                                <br />
                                <b>9) </b>The Landlord and the Tenant agree to be bound by the additional terms and
                                
                            conditions contained in Appendix (if any)
                            <br />
                                {/* <b>9) </b>業主及租客雙方同意遵守附加條款 (如有的話) 。 */}
                                <br />
                                <b>10) </b>The Tenant shall not use or permit to be used the Premises or any part there of for {this.props.contractDraft.propertyType} purpose only.
                                <br /> 
                                {/* <b>10) </b> 租客除將該物業作XXXXX用途外, 不可將該物業或其任何部分作其他用途。 */}
                                <br /> 
                                <b>11) </b>The Tenant shall be entitled to a rent free period of <b>{isNaN(moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')) ? "_________" : moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')}</b> {moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days') > 1 ? "days" : "day"} from {this.state.startGracePeriod? <b><u>{moment(this.state.startGracePeriod).format("YYYY-MM-DD")}</u></b> : "_________"} to {this.state.endGracePeriod? <b><u>{moment(this.state.endGracePeriod).format("YYYY-MM-DD")}</u></b> : "_________"} (both days inclusive) provided that the
                            Tenant shall be responsible for the charges of water, electricity, gas, telephone and other
                            outgoings payable in respect of Premises during such rent free period.
                            <br />
                            {/* <b>11) </b>租客可享有<b>{moment(this.state.endGracePeriod).diff(moment(this.state.startGracePeriod), 'days')}</b> 天免租期由
                            <DatePicker selected={this.state.startGracePeriod} onChange={this.handleChange.bind(this, "startGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.startGracePeriod}/> 
                                至
                            <DatePicker selected={this.state.endGracePeriod} onChange={this.handleChange.bind(this, "endGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.endGracePeriod}/> 

                            (包括首尾兩天)
                            但租客仍需負責繳付免租期內一切水、電、煤氣及電話費用及其他一切雜費支出。
                            除XXXXXXX用途外, 不可將該物業或其任何部分作其他用途。 */}
                            <br />
                            <b>12) </b>If there is any contradiction between the English version and the Chinese version
                            in this Agreement, the Chinese version shall prevail.
                            <br />
                            {/* <b>12) </b>此合約內私英文文本與中文文本存有差異時, 將以中文文本為準。 */}
                                </Col>
                            </Row>
                            <div className='pri-sign'>
                            <Row>
                                <Col md="12">
                                    <div>
                                    <h3>Schedule I</h3>
                                   
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Premise</InputGroupAddon>
                                    <input type="text" className="form-control" id="formGroupExampleInput" value={`${this.props.contractDraft.unit}, ${this.props.contractDraft.address}, ${this.props.contractDraft.country}`} disabled={true}/>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Full Name of the Landlord</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={`${this.props.contractDraft.landlord_name}`} disabled={true}/>
                                    </div>
                                    <br />
                                    {/* <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Full Name of the Tenant</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "tenantName")} value={this.state.tenantName}/>
                                    </div>
                                    <br /> */}
                                   
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Rent Due Date</InputGroupAddon>
                                    <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.rentDueDate || ""} onChange={this.onChange.bind(this, "rentDueDate")}/>
                                    </div>
                                    </div>
                                    <br />
                                </Col>
                                <Col md="12">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Term (both days inclusive):</InputGroupAddon>
                                    <InputGroupAddon addonType="prepend">From</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.startContract} onChange={this.handleChange.bind(this, "startContract")}  dateFormat="yyyy/MM/dd" value={this.state.startContract}/>
                                    <InputGroupAddon addonType="prepend">To</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.endContract} onChange={this.handleChange.bind(this, "endContract")}  dateFormat="yyyy/MM/dd" value={this.state.endContract}/>
                                    </div>
                                    </div>
                                    <br />
                                </Col>
                                <Col md="12">
                                    <div>
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Grace Period:</InputGroupAddon>
                                    <InputGroupAddon addonType="prepend">From</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.startGracePeriod} onChange={this.handleChange.bind(this, "startGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.startGracePeriod}/>
                                    <InputGroupAddon addonType="prepend">To</InputGroupAddon>
                                    <DatePicker className="form-control" selected={this.state.endGracePeriod} onChange={this.handleChange.bind(this, "endGracePeriod")}  dateFormat="yyyy/MM/dd" value={this.state.endGracePeriod}/>
                                    </div>
                                    </div>
                                </Col>
                                <Col md="8">
                                    <div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Rent Per Month</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "rentPrice" )} value={this.state.rentPrice || ""}/>
                                    <InputGroupAddon addonType="append">{this.props.contractDraft.currency}</InputGroupAddon>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Management Fees</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.management_fee} disabled={true}/>
                                        <InputGroupAddon addonType="append">{this.props.contractDraft.currency}</InputGroupAddon>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Government Rents</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.government_rent} disabled={true}/>
                                        <InputGroupAddon addonType="append">{this.props.contractDraft.currency}</InputGroupAddon>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Government Rates</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" value={this.props.contractDraft.rates} disabled={true}/>
                                        <InputGroupAddon addonType="append">{this.props.contractDraft.currency}</InputGroupAddon>
                                    </div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Security Deposit</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "deposit" )} value={this.state.deposit || ""}/>
                         <InputGroupAddon addonType="append">{this.props.contractDraft.currency}</InputGroupAddon>
                                    </div>
                                    </div>
                                </Col>
                                <Col md="12">
                                    <div>
                                    <br />
                                    <div className="schedule">
                                    <InputGroupAddon addonType="prepend">Number of key(s) of the Premises by the Tenant:</InputGroupAddon>
                                        <input type="text" className="form-control" id="formGroupExampleInput" onChange={this.onChange.bind(this, "keys" )} value={this.state.keys || ""}/>
                                        <InputGroupAddon addonType="append">key(s)</InputGroupAddon>
                                    </div>
                                  
                                    </div>
                                </Col>
                            </Row>
                            </div>
                            <Row>
                                <Col md="12">
                                    <div>
                                        <h3>Signature</h3>
                                    </div>
                                    <div>
                                       
                                        <b><div className="contract-text"> Confirmed and Accepted all the terms and Conditions contained herein by the Landlord:</div></b>
                                            <br></br>
                                        {/* JSX Library */}
                                       
                                        <div className="signature-pad">
                                        <Row>
                                            <Col md="12">
                                            <div className="contract-text">Signature of LandLord:</div>
                                            <br />
                                            <div>
                                        <SignatureCanvas canvasProps={{width: 425, height: 250, className: "sigCanvas"}}
                                          ref={(ref:any) => { return this.sigPadForLandLord = ref }}/>
                                        </div>
                                        <br />
                                        <div>
                                        ID/BR number of LandLord:
                                        <input type="text" className="form-control" id="formGroupExampleInput"  value={this.props.contractDraft.landlord_id_or_br} disabled={true}/>
                                        </div>
                                        </Col>
                                        {/* <Col md="6">
                                        
                                            <h6>Signature of Tenant:</h6>
                                            <br />
                                            <div>
                                                
                                        <SignatureCanvas canvasProps={{width: 425, height: 250, className: "sigCanvas"}}
                                          ref={(ref:any) => { return this.sigPad2 = ref }} />
                                        </div>
                                        </Col> */}
                                        </Row>
                                        </div>
                                        <Button onClick={this.onConfirmByLandLord}>Confirm By LandLord</Button>
                                        <Button onClick={() => this.props.history.goBack()}>Back</Button>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return ({
        contractDraft: state.contractDraft,
      
    })
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
       
        getContractByCode: (code: string) => dispatch(getContractByCode(code)),
        addContractDraft: (contractInformation:IContractReRentDetails) => dispatch(addContractDraft(contractInformation)),
        deleteOriginalContract: (id:number, unit_id:number) => dispatch(deleteContract(id, unit_id)),
        activateContractPending: (id:number) => dispatch(activateContractPending(id))

      
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ContractReRent)