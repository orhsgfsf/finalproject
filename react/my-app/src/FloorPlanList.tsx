import React from 'react'
import { UncontrolledCollapse, ListGroupItem } from 'reactstrap';
import { Button } from '@material-ui/core'
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { getDistinctCountries } from './distinctCountries/thunks';
import { getPropertiesList } from './propertiesList/thunks';
import { getProperty } from './property/thunks';
import { getUnit } from './unit/thunks';
import { push } from 'connected-react-router';
import { clearDetailsSuccess } from './floorPlanUnit/actions';
import { getCoordinates } from './draggable/thunks';
import './property.css';

interface IFloorPlanListState {

}

interface IFloorPlanListProps {
    propertiesList: {
        id: number
        address?: string
        country?: string
    }[]

    distinctCountriesList: {
        id?: number
        country?: string
    }[]

    getPropertiesList: () => void

    getDistinctCountries: () => void

    getProperty: (id: number) => void
    getUnit: (id: number) => void

    goto: (id: number) => void
    clearDetails: () => void
    getCoordinates: (id: number) => void
}

class FloorPlanList extends React.Component<IFloorPlanListProps, IFloorPlanListState>{

    private loadProperty = (id: number) => {
        this.props.clearDetails()
        this.props.getProperty(id)
        this.props.getUnit(id)
        this.props.goto(id)
        this.props.getCoordinates(id)

    }



    public componentDidMount() {
        this.props.getPropertiesList()
        this.props.getDistinctCountries()
    }

    public render() {
        console.log(this.props.distinctCountriesList)
        console.log(this.props.propertiesList)
        return (
            <div>
                <br></br>
            <div className='primary2'>
                <div >
                    <h5>Floor Plan</h5>
                </div>
                {
                    this.props.distinctCountriesList.length !== 0 &&
                    <div>

                        <div>
                            {this.props.distinctCountriesList.map((country, idx) =>
                                <div>
                                    <br></br>
                                    <Button color="primary" id={`btn_${country.id}`} style={{  }} >
                                        {country.country}
                                    </Button>
                                    <UncontrolledCollapse toggler={`#btn_${country.id}`}>
                                        {this.props.propertiesList.filter(property =>
                                            property.country === country.country).map(property =>
                                                <ListGroupItem key={property.id} className='lightgreen'>
                                                    <div className="property">
                                                        <Button  onClick={() => this.loadProperty(property.id)}>{property.address}</Button>
                                                    </div>
                                                </ListGroupItem>
                                            )
                                        }
                                    </UncontrolledCollapse>
                                </div>
                            )}
                        </div>


                    </div>

                }
            </div>
            </div>
        )
    }
}


const mapStateToProps = (state: IRootState) => ({

    distinctCountriesList: state.distinctCountriesList.distinctCountriesList,
    propertiesList: state.propertiesList.propertiesList,

})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getDistinctCountries: () => dispatch(getDistinctCountries()),
        getPropertiesList: () => dispatch(getPropertiesList()),
        getProperty: (id: number) => dispatch(getProperty(id)),
        getUnit: (id: number) => dispatch(getUnit(id)),
        goto: (id: number) => dispatch(push(`/menu/floor_plan/details/${id}`)),
        clearDetails: () => dispatch(clearDetailsSuccess()),
        getCoordinates: (id: number) => dispatch(getCoordinates(id))
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(FloorPlanList)

