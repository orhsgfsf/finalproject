import * as React from 'react';
import { Alert, Col } from 'reactstrap';
import { IRootState } from './store';
import { ThunkDispatch } from './store';
import { login, loginFacebook } from './auth/thunks';
import { connect } from 'react-redux';
import './index.css';
import { ReactFacebookLoginInfo } from 'react-facebook-login';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBModalFooter,
  MDBIcon,
  MDBBtn,
  MDBInput
} from "mdbreact";
import { Link, Redirect } from 'react-router-dom';
import sakura from "./Image/sakura1.png";
import './login.css'
import { clearStatusSuccess } from './register/actions';



// import FacebookLogin require('react-facebook-login/dist/facebook-login-render-props');
const FacebookLogin = require('react-facebook-login/dist/facebook-login-render-props').default;

interface ILoginState {
  username: string
  password: string
  regStatus: string
}

interface ILoginProps {
  isAuthenticated: boolean

  login: (username: string, password: string) => void
  loginFacebook: (accessToken: string) => void
  goTo: () => void
  clearStatus: () => void

  msg: string
  regStatus: { regStatus?: string }
}

class Login extends React.Component<ILoginProps, ILoginState> {
  constructor(props: ILoginProps) {
    super(props)
    this.state = {
      username: "",
      password: "",
      regStatus: ""
    }
  }



  private onChange = (field: "username" | "password", e: React.FormEvent<HTMLInputElement>) => {
    const state: any = {}
    state[field] = e.currentTarget.value

    this.setState(state)
  }

  private login = () => {
    const { username, password } = this.state
    if (username && password) {
      this.props.login(username, password)
    }
  }

  private fBOnClick = () => {
    return null
  }

  private fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
    console.log(userInfo.accessToken)
    if (userInfo.accessToken) {
      this.props.loginFacebook(userInfo.accessToken)
    }
    return null
  }

  public componentDidMount() {
    // this.props.isAuthenticated == true && this.props.goTo()
  }

  public componentWillUnmount() {
    this.props.clearStatus()
  }

  public render() {
    if (this.props.isAuthenticated === true) {
      return <Redirect to="/home" />
    }

    return (
      <div>
        <MDBContainer >

          <MDBRow>

            <MDBCol md="6" style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <MDBCardBody className="mx-4" >
                <div className='Hero'>
                  weVital
                <img className="sakura" src={sakura} alt="abc" />
                </div>
              </MDBCardBody>


            </MDBCol>

            <MDBCol md="6" style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>

              <MDBCard>

                <MDBCardBody className="mx-4" >
                  <Col className="status_box">
                    {this.props.regStatus.regStatus ? <Alert color="info">
                      {this.props.regStatus.regStatus}
                    </Alert> : ""}
                  </Col>
                  <div className="text-center">
                    <h3 className="dark-grey-text mb-5">
                      <strong>Sign in</strong>
                    </h3>
                  </div>
                  <MDBInput
                    label="Your username"
                    className="class"
                    group
                    type="email"
                    validate
                    error="wrong"
                    success="right"
                    value={this.state.username}
                    onChange={this.onChange.bind(this, "username")}
                  />
                  <MDBInput
                    label="Your password"
                    className="class"
                    group
                    type="password"
                    validate
                    containerClass="mb-0"
                    value={this.state.password}
                    onChange={this.onChange.bind(this, "password")}

                  />
                  {/* <p className="font-small blue-text d-flex justify-content-end pb-3">
                    Forgot
                    <a href="#!" className="blue-text ml-1">
    
                      Password?
                    </a>
                  </p> */}
                  <div className="text-center mb-3">
                    {this.props.msg ? <Alert color="danger">
                      {this.props.msg}
                    </Alert> : ""}
                    <MDBBtn
                      type="button"
                      gradient="blue"
                      rounded
                      className="btn-block z-depth-1a"
                      onClick={this.login}
                    >

                      Sign in
                    </MDBBtn>

                  </div>
                  <p className="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2">
                    or Sign in with:
                  </p>
                  <div className="row my-3 d-flex justify-content-center">
                    <FacebookLogin
                      appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
                      fields="name, email, picture"
                      onClick={this.fBOnClick}
                      callback={this.fBCallback}

                      render={(renderProps: any) => (<MDBBtn
                        type="button"
                        color="white"
                        rounded
                        className="mr-md-3 z-depth-1a"
                        onClick={renderProps.onClick}
                      >
                        <MDBIcon fab icon="facebook-f" className="blue-text text-center" />
                      </MDBBtn>)}
                    />


                  </div>
                </MDBCardBody>
                <MDBModalFooter className="mx-5 pt-3 mb-1">
                  <p className="font-small grey-text d-flex justify-content-end">
                    Not a member?
                    <Link to="/register" className="blue-text ml-1">Sign Up</Link>
                    {/* <a href="#!" className="blue-text ml-1">
    
                      Sign Up
                    </a> */}
                  </p>
                </MDBModalFooter>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </MDBContainer>

        <MDBRow>
          <MDBCol md="12">

          </MDBCol>
        </MDBRow>

      </div>
    );

  };

}


//     <Form>
//         <FormGroup>
//             <Label for="username">Username</Label>
//             <Input type="email"
//             value={this.state.username}
//             onChange={this.onChange.bind(this, "username")}
//             placeholder="Username"
//             />
//         </FormGroup>
//         <FormGroup>
//             <Label for="password">Password</Label>
//             <Input type="password"
//             value={this.state.password}
//             onChange={this.onChange.bind(this, "password")}
//             placeholder="Password"
//             />
//         </FormGroup>
//         <FormGroup>
//             <ReactFacebookLogin
//             appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
//             fields="name, email, picture"
//             onClick={this.fBOnClick}
//             callback={this.fBCallback}
//             />
//         </FormGroup>
//         {this.props.msg ? <Alert color="danger">
//             {this.props.msg}
//         </Alert>:""
//     }
//     <Button onClick={this.login}>Login</Button>
//     </Form>
// )



const mapStateToProps = (state: IRootState) => ({
  isAuthenticated: state.auth.isAuthenticated,
  msg: state.auth.msg,
  regStatus: state.register.status
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  login: (username: string, password: string) => dispatch(login(username, password)),
  loginFacebook: (accessToken: string) => dispatch(loginFacebook(accessToken)),
  clearStatus: () => dispatch(clearStatusSuccess())
  // goTo: () => dispatch(push('/home'))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)