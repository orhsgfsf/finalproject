import { ICoordinatesState } from './state'
import { ICoordinatesActions } from './actions'

const initialState:ICoordinatesState = {
    coordinates:[]
}



export function coordinatesReducer  (state:ICoordinatesState = initialState, action:ICoordinatesActions):ICoordinatesState{
    switch(action.type){
        case "ADD_COORDINATES_SUCCESS":
            console.log(action.coordinates)
            return {
                ...state,
                coordinates: action.coordinates
            }

        case "GET_COORDINATES_SUCCESS":
          return {
              ...state,
                coordinates:action.coordinates
          }
            default:
                return state
            
    }


}