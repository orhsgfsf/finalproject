export interface ICoordinates{
    x:number
    y:number
    height:number
    width:number
    id:number
    unit:string
    rental_status:boolean
    contract_pending:boolean
}

export interface ICoordinatesState{
    coordinates: ICoordinates[]
}



