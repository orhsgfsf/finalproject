import { ICoordinates } from "./state";

export function addCoordinatesSuccess (coordinates:ICoordinates[]){
   
    return {
        type: "ADD_COORDINATES_SUCCESS" as "ADD_COORDINATES_SUCCESS",
        coordinates
    }
}

export function getCoordinatesSuccess (coordinates:ICoordinates[]){
    return {
        type: "GET_COORDINATES_SUCCESS" as "GET_COORDINATES_SUCCESS",
        coordinates
    }
}
type coordinatesActionCreator = typeof addCoordinatesSuccess | typeof getCoordinatesSuccess
export type ICoordinatesActions = ReturnType<coordinatesActionCreator>