import {Dispatch} from 'redux'
import {CallHistoryMethodAction, push} from "connected-react-router"

import { ICoordinatesActions,getCoordinatesSuccess } from './actions';
import { ICoordinatesResult } from '../Drag';
import { ThunkDispatch } from '../store';

// export function addCoordinates(coordinates:ICoordinates[], property_id:number){
//     return async (dispatch:Dispatch<ICoordinatesActions|CallHistoryMethodAction>) => {
//         const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/dropCoordinates`,{
//             method: "POST",
//             headers: {
//                 "Content-type":"application/json",
//                 "Authorization":`Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify(coordinates)

//         })

//         const result = await res.json()
//         if (res.status !== 200){
//             console.error(result.msg)
//         }else{

//             dispatch(addCoordinatesSuccess(coordinates))
            
//             dispatch(push(`/menu/floor_plan/details/${property_id}`))
            
//         }
//     }
// }
export function getCoordinates(property_id:number){
    console.log(property_id)
    return async (dispatch:Dispatch<ICoordinatesActions|CallHistoryMethodAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/units/toGetCoordinates`, {
            method: "POST",
            headers:{
                "Content-type":"application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({property_id})
        })
        const result = await res.json()
        console.log({drop:result})
        if(res.status !== 200){
            console.error(result.msg)
        }else{
            dispatch(getCoordinatesSuccess(result.data))
        }
    }

}

export function updateCoordinates(coordinates:ICoordinatesResult[], property_id:number){
    console.log(coordinates)
    return async (dispatch:Dispatch<ICoordinatesActions|CallHistoryMethodAction>) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/dropCoordinates`,{
            method: "PATCH",
            headers: {
                "Content-type":"application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(coordinates)

        })

        const result = await res.json()
        if (res.status !== 200){
            console.error(result.msg)
        }else{

          
            
            dispatch(push(`/menu/floor_plan/details/${property_id}`))
            
        }
    }
}

export function clearCoordinates(property_id:number){
    return async (dispatch:ThunkDispatch) => {
        const res = await fetch (`${process.env.REACT_APP_API_SERVER}/units/clearCoordinates`,{
            method: "PATCH",
            headers: {
                "Content-type":"application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({property_id})

        })

        const result = await res.json()

        if (res.status !== 200){
            console.error(result.msg)
        }else{
            console.log("ok")
        }
    }
}