import React from 'react'
import './property.css';
import { Button } from '@material-ui/core';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { Col, Row, InputGroupAddon } from 'reactstrap';
import { getProperty } from './property/thunks';
import { IInsideAddUnitFailState } from './addUnitFail/state';
import { editUnit } from './unit/thunks';
import { ISingleUnitState } from './singleUnit/state';
import { getUnitByUnitId } from './singleUnit/thunks';
import { changeUnitValue } from './singleUnit/actions';
import { resetUnitFailed } from './addUnitFail/actions';

interface IUnitEditProps {

    singleUnit: ISingleUnitState
    addUnitFail: IInsideAddUnitFailState

    match: {
        params: {
            id: number
        }
    }


    getUnitByUnitId: (id: number) => void
    editUnit: (unitEdit: IUnitEdit) => void
    change: (state: any) => void
    resetUnitFailed: () => void
}


interface IUnitEditState {
    unitMsg: string
    saleableAreaMsg: string
    grossFloorAreaMsg: string
}
export interface IUnitEdit {
    id: number
    unit: string
    rental_status: boolean
    saleable_area: number | null
    gross_floor_area: number | null
    reminder: string
    property_id: number
    contract_pending: boolean
}



class UnitCreate extends React.Component<IUnitEditProps, IUnitEditState> {
    constructor(props: IUnitEditProps) {
        super(props)
        this.state = {
            unitMsg: "",
            saleableAreaMsg: "",
            grossFloorAreaMsg: ""
        }
    }


    private onEditClick = async() => {
        const unitEdit: IUnitEdit = {
            id: this.props.match.params.id,
            unit: this.props.singleUnit.unit,
            rental_status: true,
            contract_pending: false,
            saleable_area: this.props.singleUnit.saleable_area,
            gross_floor_area: this.props.singleUnit.gross_floor_area,
            reminder: this.props.singleUnit.reminder,
            property_id: this.props.singleUnit.property_id
        }

      

        if (this.props.singleUnit.unit.trim() === "") {
            await this.setState({
                unitMsg: "Unit can't be empty"
            })
        } else {
            await this.setState({
                unitMsg: ""
            })
        }

        if (this.props.singleUnit.saleable_area.toString().match(/\D/) !== null) {
            await this.setState({
                saleableAreaMsg: "Saleable Area can only be integer"
            })
        }
        if (this.props.singleUnit.saleable_area.toString().match(/\D/) === null) {
            await this.setState({
                saleableAreaMsg: ""
            })
        }
        if (this.props.singleUnit.saleable_area.toString().trim() === "") {
            await this.setState({
                saleableAreaMsg: "Saleable Area can't be empty, if no Saleable Area, input 0"
            })
        }


        if (this.props.singleUnit.gross_floor_area.toString().match(/\D/) !== null) {
            await this.setState({
                grossFloorAreaMsg: "Gross Floor Area can only be integer"
            })
        }
        if (this.props.singleUnit.gross_floor_area.toString().match(/\D/) === null) {
            await this.setState({
                grossFloorAreaMsg: ""
            })            
        }
        if (this.props.singleUnit.gross_floor_area.toString().trim() === "") {
            await this.setState({
                grossFloorAreaMsg: "Gross Floor Area can't be empty, if no Gross Floor Area, input 0"
            })
        }
        
        const {unitMsg, saleableAreaMsg, grossFloorAreaMsg} = this.state
        if(unitMsg === "" && saleableAreaMsg === "" && grossFloorAreaMsg === ""){
            this.props.editUnit(unitEdit)
        }
        
    }

    private onChange = (field: "unit" | "saleable_area" | "gross_floor_area" | "reminder", e: React.FormEvent<HTMLInputElement>) => {
        const state: any = {}
        state[field] = e.currentTarget.value

        this.props.change(state)
    }

    public componentDidMount() {
        this.props.getUnitByUnitId(this.props.match.params.id)
    }


    public componentWillUnmount() {
        this.props.resetUnitFailed()
    }

    public render() {
        console.log(this.props.singleUnit)
        return (
            <div className='primary'>
                <Row>
                    <Col md="6">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Unit:</label>
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.props.singleUnit.unit}
                                onChange={this.onChange.bind(this, "unit")}
                            />
                        </div>
                        <div className="error-msg">{this.state.unitMsg && `*${this.state.unitMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">

                        <label htmlFor="formGroupExampleInput">Saleable Area:</label>
                        <div className="form-group currency">
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.props.singleUnit.saleable_area}
                                onChange={this.onChange.bind(this, "saleable_area")}
                            />
                            <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{this.state.saleableAreaMsg && `*${this.state.saleableAreaMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">

                        <label htmlFor="formGroupExampleInput currency">Gross Floor Area:</label>
                        <div className="form-group currency">
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.props.singleUnit.gross_floor_area}
                                onChange={this.onChange.bind(this, "gross_floor_area")}
                            />
                            <InputGroupAddon addonType="append">sq.ft</InputGroupAddon>
                        </div>
                        <div className="error-msg">{this.state.grossFloorAreaMsg && `*${this.state.grossFloorAreaMsg}`}</div>
                    </Col>
                </Row>
                <Row>
                    <Col md="6">
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput">Reminder:</label>
                            <input
                                type="textarea"
                                className="form-control"
                                id="formGroupExampleInput"
                                // placeholder="First name"
                                value={this.props.singleUnit.reminder}
                                onChange={this.onChange.bind(this, "reminder")}
                            />
                        </div>
                        <div className="error-msg">{this.props.addUnitFail.reminder ? `*${this.props.addUnitFail.reminder}` : ""}</div>
                    </Col>
                </Row>
                <div><Button className='btn btn-secondary' onClick={this.onEditClick}>Update</Button></div>
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => ({
    property: state.property,
    addUnitFail: state.addUnitFail.addUnitFail,
    singleUnit: state.singleUnit
})

const mapDispatchToProps = (dispatch: ThunkDispatch) => {
    return ({
        getProperty: (id: number) => dispatch(getProperty(id)),
        editUnit: (unitEdit: IUnitEdit) => dispatch(editUnit(unitEdit)),
        getUnitByUnitId: (id: number) => dispatch(getUnitByUnitId(id)),
        change: (state: any) => dispatch(changeUnitValue(state)),
        resetUnitFailed: () => dispatch(resetUnitFailed())
    });
}

export default connect(mapStateToProps, mapDispatchToProps)(UnitCreate)