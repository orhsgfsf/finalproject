import * as passport from 'passport'
import * as passportJWT from 'passport-jwt'
import jwt from './jwt'

import { userService } from './app'

const JWTStrategy = passportJWT.Strategy
const { ExtractJwt } = passportJWT

passport.use(new JWTStrategy({
        secretOrKey: jwt.jwtSecret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    }, async (payload, done) => {
        const user = await userService.getUser(payload.username)
        console.log("payload", payload, user)
        if (user) {
            return done(null, user[0])
        } else {
            return done(new Error("User not found"))
        }
    })
)
