import * as express from 'express'
import { UserService } from '../services/userService'


export class UserRouter {
    private userService: UserService

    constructor(userService: UserService) {
        this.userService = userService
    }

    public router() {
        const router = express.Router()

        router.get('/userDataAll', this.getUserDataAll)
        router.post('/personalinfo', this.checkPersonalInfo, this.addPersonalInfo)
        router.patch('/userData', this.updateUserData)
        router.patch('/loginTime', this.updateLoginTime)


        return router
    }

    private getUserDataAll = async (req: express.Request, res: express.Response, ) => {
        try {
            
            const result = await this.userService.getUserDataAll(req.user.id)
            res.json({ data: result })
        } catch (e) {
            console.error(e)
            res.json(e)
        }
    }

    private updateUserData = async (req: express.Request, res: express.Response, ) => {
        try {
          
            await this.userService.updateUserData(req.body, req.user.id)
            res.json({ success:true })
        } catch (e) {
            console.error(e)
            res.json(e)
        }
    }



    private checkPersonalInfo = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const { first_name, last_name, gender, date_of_birth, contact_number } = req.body

            let msg = {}

            if (first_name.trim() == "") {
                msg = { ...msg, first_name: "First Name can't be empty" }
            }
            if (last_name.trim() == "") {
                msg = { ...msg, last_name: "Last Name can't be empty" }
            }
            if (gender == "--Gender--") {
                msg = { ...msg, gender: "Please select gender" }
            }
            // if (id_number.trim() == "") {
            //     msg = {...msg, id_number: "ID Number can't be empty"}
            // }
            if (date_of_birth.trim() == "") {
                msg = { ...msg, date_of_birth: "Date of Birth can't be empty" }
            }
            // console.log(date_of_birth)
            // if (!(date_of_birth instanceof Date)) {
            //     console.log(date_of_birth)
            //     msg = {...msg, date_of_birth: "Wrong format of date"}
            // }
            // if (district_number.trim() == "") {
            //     msg = {...msg, district_number: "District Number can't be empty"}
            // }
            // if (isNaN(district_number)) {
            //     msg = {...msg, district_number: "District Number can only be numbers"}
            // }
            if (contact_number.trim() == "") {
                msg = { ...msg, contact_number: "Phone Number can't be empty" }
            }
            if (isNaN(contact_number)) {
                msg = { ...msg, contact_number: "Phone Number can only be numbers" }
            }



            // await this.userService.addPersonalInfo(req.body, req.user.id)
            if (Object.keys(msg).length != 0) {
                console.log(Object.keys(msg))
                console.log(msg)
                console.log({ msg })
                return res.status(400).json({ msg })
            }

            console.log("next")
            return next()


        } catch (e) {
            res.status(400).json(e)
        }
    }

    private addPersonalInfo = async (req: express.Request, res: express.Response) => {
        try {
            console.log("WHAT?")
            console.log(req.body)
            await this.userService.addPersonalInfo(req.body, req.user.id)
            console.log("WHAT?")
            console.log(req.body)
            res.json({ Success: true })
        } catch (e) {
            res.json(e)
        }
    }

    private updateLoginTime = async (req: express.Request, res: express.Response) => {
        try{
            console.log("I am going to logout")
            await this.userService.updateLoginTime(req.body, req.user.id)
        }catch(e){
            console.error(e)
            res.json(e)
        }
    }


}


