import { AutoCheckService } from '../services/autoCheckService'

export class AutoCheckRouter {
    private autoCheckService: AutoCheckService

    constructor(autoCheckService: AutoCheckService){
        this.autoCheckService = autoCheckService
    }

    public corn(){
        this.autoCheckEveryday()
    }

    autoCheckEveryday = async () => {
        await this.autoCheckService.autoCheckEveryday()
    }
}