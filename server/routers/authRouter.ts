import * as express from 'express'
import { UserService } from '../services/userService'
import * as jwtSimple from 'jwt-simple'
import jwt from '../jwt'
import { checkPassword } from '../hash'
import fetch from 'cross-fetch';

export class AuthRouter{
    private userService: UserService

    constructor(userService: UserService){
        this.userService = userService
    }

    public router() {
        const router = express.Router()

        router.post('/login', this.login)
        router.post('/facebook', this.loginFacebook)
        router.post('/register', this.register, this.addUser)
       

        return router
    }

    private login = async (req:express.Request, res:express.Response) => {
        try {
            if (!req.body.username || !req.body.password){
                res.status(401).json({msg:"Invalid username or password"})
                return
            }
            const { username, password } =  req.body
            const user = (await this.userService.getUser(username))[0]
            if (!user || !(await checkPassword(password, user.password))){
                res.status(401).json({msg:"Invalid username or password"})
                return
            }
            const payload = {
                id: user.id,
                username: user.username
            }
            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            res.json({
                token: token
            })
        }catch(e){
            console.log(e)
            res.status(500).json({msg:e.toString()})
        }
    }

    private loginFacebook = async (req:express.Request, res:express.Response) => {
        try{
            console.log("testing login fb")
            if(!req.body.accessToken){
                console.log("wrong access token?")
                res.status(401).json({msg:"Wrong Access Token!"})
                return 
            }
            console.log("success?")
            const { accessToken } = req.body
            const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            const result = await fetchResponse.json();


            if(result.error){
                res.status(401).json({msg:"Wrong access token"})
                return 
            }
            let user = (await this.userService.getUserByEmail(result.email))[0]
            if (!user){
                user = (await this.userService.createUser(result.email))[0]
            }

            const payload = {
                id: user.id,
                username: user.username 
            }
            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            res.json({
                token
            })
        }catch(e){
            console.log(e)
            res.status(500).json({msg:e.toString()})
        }
    }


    private register = async (req:express.Request,res:express.Response, next:express.NextFunction) => {

        const char: string = "^[\\w-_\.]*[\\w]$"
        const correctEmail =  /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/

        const { username, password, confirmPassword, email } = req.body;
        console.log(req.body)

        const isDuplicated = await this.userService.checkUserReg(username)
        let msg = {};

        if (isDuplicated){
            msg = {...msg, username: "The username has already exist"}
        } if (username.trim() == "") {
            msg = {...msg, username: "Username can\'t be empty"}
        } if (!username.match(char)) {
            msg = {...msg, username: "Username can only include numbers or words"}
        } if (username.length < 4) {
            msg = {...msg, username: "Username is too short"}
        } if (!password.match(char)) {
            msg = {...msg, password: "Password can only include numbers or words"}
        } if (password.trim() == "") {
            msg = {...msg, password: "Password can\t be empty"}           
        } if (password.length < 4) {
            msg = {...msg, password: "Password is too short"}
        } if (password != confirmPassword) {
            msg = {...msg, password: "Password and Confirm Password are not consistent"}
        } if (email.trim() == "") {
            msg = {...msg, email: "Email can\'t be empty"}
        } if (!correctEmail.test(email)) {
            msg = {...msg, email: "This is not an effective email"}
        }  
        if (Object.keys(msg).length != 0){
            console.log(Object.keys(msg))
            console.log(msg)
            console.log({msg})
            return res.status(400).json({msg})
        }
        console.log("success???")
        return next()

    }


    private addUser = async (req:express.Request,res:express.Response) => {
        const { username, password, email } = req.body
        try{
            let status = {}
            await this.userService.addUser(username, password, email)
            status = {...status, regStatus: "Register Success!"}
            console.log(status)
            res.json({status})
        }catch(e){
            res.status(400).json(e)
        }
    }
}
