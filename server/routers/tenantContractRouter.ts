import * as express from 'express'

import {TenantContractService} from '../services/tenantContractService'

export class TenantContractRouter{
    private tenantContractService: TenantContractService
    constructor(tenantContractService:TenantContractService){
        this.tenantContractService = tenantContractService
    }

    public router(){
        const router = express.Router()

        router.post('/toGetContractByCode', this.loadContractByCode)
        router.post('/contractPdf', this.contractToPdf)
        router.get('/downloadContract', this.downloadContract)
        router.get('/businessNaturesList', this.loadBusinessNaturesList)
        router.patch('/toSignContract', this.signContractByTenant)




        return router
    }

    loadContractByCode = async (req:express.Request,res:express.Response) => {
        try {
            const contract = await this.tenantContractService.loadContractByCode(req.body)
            res.json({data:contract})
        } catch (e) {
            res.status(400).json(e.message)
        }       
    }

    loadBusinessNaturesList = async (req:express.Request,res:express.Response) => {
        try {
            const businessNatures = await this.tenantContractService.loadBusinessNaturesList()
            res.json({data:businessNatures})          
        } catch (e) {
            res.status(400).json(e.message)
        }  
    }








    signContractByTenant = async (req:express.Request,res:express.Response) => {
        try {
            await this.tenantContractService.signContractByTenant(req.body)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }    
    }


    contractToPdf = async (req:express.Request,res:express.Response) => {
        
        try{
           
            const x = await this.tenantContractService.contractToPdf(req.body.code)
            // res.download(path.resolve(`${__dirname}/../${req.body.code}.pdf`))
            // res.download(path.join(__dirname, `../${req.query.code}.pdf`));
            console.log({dir:x})
            res.json({data:x})
        }
        catch (e) {
            console.log(e)
            res.status(400).json(e.message)
        } 
    }

    downloadContract = async (req:express.Request,res:express.Response) => {
        try{
            let path = require('path')
            // await this.contractService.contractToPdf(req.body.code)
            // res.download(path.resolve(`${__dirname}/../${req.body.code}.pdf`))
            res.download(path.join(__dirname, `../${req.query.code}.pdf`));
        }
        catch (e) {
            console.log(e)
            res.status(400).json(e.message)
        } 
    }




}