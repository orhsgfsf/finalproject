import * as express from 'express'
import { PropertyService } from '../services/propertyService'

export class PropertyRouter {
    private propertyService: PropertyService

    constructor(propertyService: PropertyService) {
        this.propertyService = propertyService
    }

    public router() {
        const router = express.Router()

        router.get('/numberOfProperties', this.loadNumberOfProperties)
        router.get('/distinctCountries', this.loadDistinctCountries)
        router.get('/countriesList', this.loadCountriesList)
        router.get('/currenciesList', this.loadCurrenciesList)
        router.get('/propertyTypesList', this.loadPropertyTypesList)
        router.post('/country', this.checkDistinctCountry)
        router.post('/toGetProperties', this.loadDetailsOfProperties)
        router.post('/properties', this.addDetailsOfProperty)
        router.patch('/floorPlans', this.updateFloorPlans)
        router.patch('/properties', this.updateDetailsOfProperty)
        router.delete('/properties', this.deleteProperty)

        return router
    }


    // ------------------------------------------ Get things -------------------------------------------------

    // Get the number of the properties
    loadNumberOfProperties = async (req: express.Request, res: express.Response) => {
        try {

            const result = await this.propertyService.loadNumberOfProperties(req.user.id)
            res.json({ data: result })
        } catch (e) {
            res.status(400).json({ msg: e.message })
        }
    }

    loadDistinctCountries = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.propertyService.loadDistinctCountries(req.user.id)
            res.json({ data: result })
        } catch (e) {
            res.status(400).json({ msg: e.message })
        }
    }

    loadCountriesList = async (_req: express.Request, res: express.Response) => {
        try {
            const result = await this.propertyService.loadCountriesList()
            res.json({ data: result })
        } catch (e) {
            res.status(400).json({ msg: e.message })
        }
    }

    loadCurrenciesList = async (_req: express.Request, res: express.Response) => {
        try {

            const result = await this.propertyService.loadCurrenciesList()
            res.json({ data: result })
        } catch (e) {

            res.status(400).json({ msg: e.message })
        }
    }

    loadPropertyTypesList = async (_req: express.Request, res: express.Response) => {
        try {
            const result = await this.propertyService.loadPropertyTypesList()

            res.json({ data: result })
        } catch (e) {
            res.status(400).json({ msg: e.message })
        }
    }

    // Post propertyID to get the details of the properties
    loadDetailsOfProperties = async (req: express.Request, res: express.Response) => {
        try {
            console.log(req.body)
            const result = await this.propertyService.loadDetailsOfProperties(req.body, req.user.id)
            res.json({ data: result })
        } catch (e) {
            res.status(400).json({ msg: e.message })
        }
    }


    // ------------------------------------------ Add things -------------------------------------------------
    checkPropertyInput = async (req: express.Request, res: express.Response, next:express.NextFunction) => {
        try {
            console.log("XXXXXXXXXXXXXXXXXXXXXXXXX")
            console.log(req.body)
            const { address, country, propertyType, grossFloorArea, saleableArea, governmentRent, managementFee, currency, rates} = req.body
            let msg = {}

            if (address.trim() == "") {
                msg = {...msg, address: "Please Input Address"}
            }
            if (grossFloorArea == null || grossFloorArea == "" || isNaN(grossFloorArea)) {
                msg = {...msg, grossFloorArea: "Please Input Gross Floor Area with right format"}
            }
            if (saleableArea == null || saleableArea == "" || isNaN(saleableArea)) {
                msg = {...msg, saleableArea: "Please Input Saleable Area with right format"}
            }
            if (governmentRent == null || governmentRent == "" || isNaN(governmentRent)) {
                msg = {...msg, governmentRent: "Please Input Government Rent with right format"}
            }
            if (managementFee == null || managementFee == "" || isNaN(managementFee)) {
                msg = {...msg, managementFee: "Please Input Management Fee with right format"}
            }
            if (rates == null|| rates == "" || isNaN(rates)) {
                msg = {...msg, rates: "Please Input Rates with right format"}
            }
        
        
            if (country == "--Country--" || country == "") {
                msg = {...msg, country: "Please select country"}
            }
            if (propertyType == "--Property type--" || propertyType == "") {
                msg = {...msg, propertyType: "Please select property type"}
            }
            if (currency == "--Currency--") {
                msg = {...msg, currency: "Please select currency"}
            }

           
            console.log(msg)

            // await this.userService.addPersonalInfo(req.body, req.user.id)
            if (Object.keys(msg).length != 0) {
                console.log(Object.keys(msg))
                console.log(msg)
                console.log({ msg })
                return res.status(400).json({ msg })
            }

            console.log("next")
            return next()

        } catch (e) {
            res.status(400).json(e)
        }
    }
    // Add property to the database
    addDetailsOfProperty = async (req: express.Request, res: express.Response) => {
        try {
            console.log(req.body)
            const id = await this.propertyService.addDetailsOfProperty(req.user.id, req.body)
            res.json({ data: id })
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    // Add floor plan to the database
    updateFloorPlans = async (req: express.Request, res: express.Response) => {
        try {
            await this.propertyService.updateFloorPlans(req.body)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }
    }


    // ------------------------------------------ Update things -------------------------------------------------

    // Start updating details of the property
    updateDetailsOfProperty = async (req: express.Request, res: express.Response) => {
        try {
            await this.propertyService.deleteProperty(req.body)
            const result = await this.propertyService.addDetailsOfProperty(req.user.id, req.body)
            console.log("DELETEEE")
            console.log(result)
            res.json({ data: result })
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }
    }


    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete details of the property by changing it's status to be "history"
    deleteProperty = async (req: express.Request, res: express.Response) => {
        try {
            await this.propertyService.deleteProperty(req.body)
            res.json({ success: true })
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    checkDistinctCountry = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.propertyService.checkDistinctCountry(req.body)
            console.log("TESTTTTTTTTTTTT?")
            res.json({ data: result })
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }
    }
}