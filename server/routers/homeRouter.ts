import * as express from 'express' 
import { HomeService } from '../services/homeService'

export class HomeRouter {
    private homeService:HomeService

    constructor(homeService:HomeService) {
        this.homeService = homeService
    }

    public router() {
        const router = express.Router()

        router.get('/userData', this.loadUserData)
        router.get('/dayDifference', this.checkDateDifference)
        router.get('/notification', this.loadNotification)
        router.get('/todaySchedule', this.loadTodaySchedule)
        // router.get('/scheduleDateAll', this.loadScheduleDateAll)
        // router.get('/calenderSchedule', this.loadCalenderSchedule)
        // router.post('/calenderSchedule', this.createCalenderSchedule)
        // router.patch('/calenderSchedule', this.editCalenderSchedule)
        router.patch('/loginTime', this.changeLoginTime)

        return router
    }

// ------------------------------------------ Get things -------------------------------------------------

    private loadUserData = async (req:express.Request,res:express.Response) => {
        try{
            
          
            const userData = await this.homeService.loadUserData(req.user.id)
            res.json({msg:"Error", data:userData[0]})
            
        }catch(e){
            res.status(400).json({msg:e.message})
        }
    }

    private checkDateDifference = async (req:express.Request,res:express.Response) => {
        try{
            const dataDifference = await this.homeService.checkDateDifference(req.user.id)
           
            console.log(dataDifference)
            res.json({msg:"Error", data:dataDifference})

        }catch(e){
            res.status(400).json({msg:e.message})
        }
    }
    private loadNotification = async (req:express.Request,res:express.Response) => {
        try{
            const today = await this.homeService.loadNotification(req.user.id)
            res.json({msg:"Error", data:today})
        }catch(e){
            res.status(400).json({msg:e.message})
        }
    }

    private loadTodaySchedule = async (req:express.Request,res:express.Response) => {
        try{
            const userData = await this.homeService.loadTodaySchedule(req.user.id)
            res.json(userData)
        }catch(e){
            res.status(400).json(e.message)
        }
    }

    // private loadScheduleDateAll = async (req:express.Request,res:express.Response) => {
    //     try{
    //         const scheduleDateAll = await this.homeService.loadScheduleDateAll(req.user.id)
    //         res.json(scheduleDateAll)
    //     }catch(e){
    //         res.status(400).json(e.message)
    //     }
    // }

    // private loadCalenderSchedule = async (req:express.Request,res:express.Response) => {
    //     try{
    //         const calenderSchedule = await this.homeService.loadCalenderSchedule(req.body)
    //         res.json(calenderSchedule)
    //     }catch(e){
    //         res.status(400).json(e.message)
    //     }
    // }

// ------------------------------------------ Add things -------------------------------------------------

    // private createCalenderSchedule = async (req:express.Request,res:express.Response) => {
    //     try{
    //         await this.homeService.createCalenderSchedule(req.body, req.user.id)
    //         res.redirect("/")
    //         // Webpage has not prepared yet!!
    //     }catch(e){
    //         res.status(400).json(e.message)
    //     }
    // }

// ------------------------------------------ Update things -------------------------------------------------


    // private editCalenderSchedule = async (req:express.Request,res:express.Response) => {
    //     try{
    //         const {id, things} = req.body
    //         await this.homeService.editCalenderSchedule(id, things)
    //         res.redirect("/")
    //         // Webpage has not prepared yet!!
    //     }catch(e){
    //         res.status(400).json(e.message)
    //     }
    // }

    private changeLoginTime = async (req:express.Request,res:express.Response) => {
        try{            
            await this.homeService.changeLoginTime(req.user.id, req.body.lastLoginTime)
            res.json({success: true})
            // Webpage has not prepared yet!!
        }catch(e){
            res.status(400).json(e.message)
        }
    }
}