import * as express from 'express'
import {FloorPlanService} from '../services/floorPlanService'
import * as multer from 'multer'

export class FloorPlanRouter{
    private floorPlanService:FloorPlanService

    constructor(floorPlanService:FloorPlanService,private upload:multer.Instance){
        this.floorPlanService = floorPlanService
    }

    public router() {
        const router = express.Router()

        router.post('/', this.upload.single('pic'), this.uploadPic)
        router.post('/floorPlan', this.upload.single('pic'), this.uploadFloorPlan)

        return router
    }

    private uploadPic = async (req:express.Request,res:express.Response) => {
        try{
        
         
          await this.floorPlanService.uploadPic(req.file.location, req.user.id)
          res.json({data:req.file.location})
            
            
            
        }catch(e){
            console.error(e)
            res.status(400).json({msg:e.message})
        }
    }

     

    private uploadFloorPlan = async (req:express.Request,res:express.Response) => {
        try{
        
         
            // await this.floorPlanService.uploadFloorPlan(req.file.location, req.body.property_id)
            res.json({data:req.file.location})
              
              
              
          }catch(e){
              console.error(e)
              res.status(400).json({msg:e.message})
          }
    }
}