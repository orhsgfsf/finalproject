import * as express from 'express'
import { AnnualReportService } from '../services/annualReportService'

export class AnnualReportRouter {
    private annualReportService: AnnualReportService
    constructor(annualReportService: AnnualReportService) {
        this.annualReportService = annualReportService
    }


    public router() {
        const router = express.Router();
        router.get('/generateReport', this.generateReport);
        router.get('/test', this.test);
        return router;
    }

    generateReport = async (req: express.Request, res: express.Response) => {
        try {
            console.log('userid',req.user.id)
            const printReport = await this.annualReportService.generateReport(req.user.id);
            res.json(printReport);
        } catch (e) {
            res.status(400).json(e.stack);
        }
    }

    test = async (req: express.Request, res: express.Response) => {
        try {
            const printReport = await this.annualReportService.loadRentDueDay(req.user.id);
            res.json(printReport);
        } catch (e) {
            res.status(400).json(e.stack);
        }
    }
}