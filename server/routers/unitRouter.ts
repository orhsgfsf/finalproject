import * as express from 'express'
import { UnitService } from '../services/unitService'

export class UnitRouter {
    private unitService: UnitService

    constructor(unitService: UnitService) {
        this.unitService = unitService
    }

    public router() {
        const router = express.Router()

        router.post('/toGetUnits', this.loadDetailsOfUnits)
        router.post('/toGetCoordinates', this.loadDropCoordinates)
        router.post('/units', this.addDetailsOfUnit)
        // router.post('/dropCoordinates', this.addDropCoordinates)
        router.post('/unitByUnitId', this.loadUnitByUnitId)
        router.post('/unitForContract', this.loadUnitForContract)
        
        router.get('/unitsList', this.loadUnitsList)
        router.patch('/units', this.updateDetailsOfUnit)
        router.patch('/dropCoordinates', this.updateDropCoordinates)
        router.patch('/clearCoordinates', this.clearCoordinates)
        router.delete('/units', this.deleteUnit)
        router.delete('/dropCoordinates', this.deleteCoordinates)

        return router
    }


    // ------------------------------------------ Get things -------------------------------------------------

    // Get the details of units by posting the property_id of the units
    loadDetailsOfUnits = async (req: express.Request, res: express.Response) => {
        try {
            const units = await this.unitService.loadDetailsOfUnits(req.body, req.user.id)
            
            res.json({ data: units })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }


    loadUnitByUnitId = async (req: express.Request, res: express.Response) => {
        try {
            const unit = await this.unitService.loadUnitByUnitId(req.body, req.user.id)
          
            res.json({ data: unit })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    loadUnitForContract = async (req: express.Request, res: express.Response) => {
        try {
            console.log(req.body)
            const result = await this.unitService.loadUnitForContract(req.body.unitId, req.user.id)          
            res.json({ data: result })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    loadUnitsList = async (req: express.Request, res: express.Response) => {
        try {
            const result = await this.unitService.loadUnitsList(req.user.id)  
            console.log(result)        
            res.json({ data: result })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    loadDropCoordinates = async (req:express.Request, res:express.Response) => {
        try {
            console.log({proroerer:req.body})
            const result = await this.unitService.loadDropCoordinates(req.body.property_id, req.user.id)  
            console.log(result)        
            res.json({ data: result })
        } catch (e) {
            console.error(e)
            res.status(400).json(e.message)
        }
    }

    // ------------------------------------------ Add things -------------------------------------------------
    // CheckUnitInput = async (req: express.Request, res: express.Response, next:express.NextFunction) => {
    //     try {
    //         console.log("XXXXXXXXXXXXXXXXXXXXXXXXX")
    //         console.log(req.body)
    //         const { unit, gross_floor_area, saleable_area } = req.body
    //         let msg = {}

    //         if (unit.trim() == "") {
    //             msg = { ...msg, unit: "Please Input Unit" }
    //         }
    //         if (gross_floor_area == null || gross_floor_area == "" || isNaN(gross_floor_area)) {
    //             msg = { ...msg, grossFloorArea: "Please Input Gross Floor Area with right format" }
    //         }
    //         if (saleable_area == null || saleable_area == "" || isNaN(saleable_area)){
    //             msg = { ...msg, saleableArea: "Please Input Saleable Area with right format" }
    //         }



    //             console.log(msg)

    //             // await this.userService.addPersonalInfo(req.body, req.user.id)
    //             if (Object.keys(msg).length != 0) {
    //                 console.log(Object.keys(msg))
    //                 console.log(msg)
    //                 console.log({ msg })
    //                 return res.status(400).json({ msg })
    //             }

    //             console.log("next")
    //             return next()
            

    //     } catch (e) {
    //         res.status(400).json(e)
    //     }
    // }


    // Start inserting things about the unit to database
    addDetailsOfUnit = async (req: express.Request, res: express.Response) => {
        try {
            const id = await this.unitService.addDetailsOfUnit(req.body)
            console.log({WIREWIRIIASIFIDSAFI:id})
            res.json({ data: id })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    // Start inserting things about the layer which puts on the floor plan for draggable to database
    // addDropCoordinates = async (req: express.Request, res: express.Response) => {
    //     try {
    //         const result = await this.unitService.addDropCoordinates(req.body)
    //         console.log(result)
    //         res.json({ success: result })
    //     } catch (e) {
    //         res.status(400).json(e.message)
    //     }
    // }


    // ------------------------------------------ Update things -------------------------------------------------

    // Start updating details of the unit 
    updateDetailsOfUnit = async (req: express.Request, res: express.Response) => {
        try {

            await this.unitService.updateDetailsOfUnit(req.body)
            res.json({ success: true })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    updateDropCoordinates  = async (req: express.Request, res: express.Response) => {
        try {

            await this.unitService.updateDropCoordinates(req.body)
            res.json({ success: true })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    clearCoordinates = async (req: express.Request, res: express.Response) => {
        try {

            await this.unitService.clearCoordinates(req.body.property_id)
            res.json({ success: true })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }


    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete details of the unit by changing it's status to be "history"
    deleteUnit = async (req: express.Request, res: express.Response) => {
        try {
            console.log("FEEEEEEEEEEE")
            console.log(req.body)
            await this.unitService.deleteUnit(req.body)
            res.json({ success: true })
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    // Delete drop coordinates
    deleteCoordinates = async (req: express.Request, res: express.Response) => {
        try {
            await this.unitService.deleteCoordinates(req.body)
            res.redirect("/")
        } catch (e) {
            res.status(400).json(e.message)
        }
    }
}