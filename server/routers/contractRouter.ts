import * as express from 'express' 
import { ContractService } from '../services/contractService'

export class ContractRouter {
    private contractService:ContractService

    constructor(contractService:ContractService) {
        this.contractService = contractService
    }
    public router() {
        const router = express.Router()

        router.post('/toGetContractByCode', this.loadContractByCode)
        router.post('/contractPdf', this.contractToPdf)
        router.post('/toGetContract', this.loadContractByPropertyId)
        router.post('/toGetContractByStatus', this.loadContractByStatus)
        router.post('/toGetContractByUnitId', this.loadContractByUnitId)
        router.post('/toActiveContractPending', this.activateContractPending)
        router.get('/downloadContract', this.downloadContract)
        router.get('/generalContract', this.loadGeneralContracts)
        router.get('/contractDistinctCountries', this.loadContractDistinctCountries)
        router.get('/distinctUnitForContract', this.loadDistinctUnitForContract)
        router.get('/businessNaturesList', this.loadBusinessNaturesList)
        router.post('/contractDraft', this.addContractDraft)
        router.patch('/toSignContract', this.signContractByTenant)
        router.patch('/toConfirmContract', this.confirmClick)
       
        router.delete('/contract', this.deleteContract)

        return router
    }

    
    

    // ------------------------------------------ Get things -------------------------------------------------

    // When tenant load the draft of the contract (post)
    loadContractByCode = async (req:express.Request,res:express.Response) => {
        try {
            const contract = await this.contractService.loadContractByCode(req.body)
            res.json({data:contract})
        } catch (e) {
            res.status(400).json(e.message)
        }       
    }

    // Get brief description of all contracts
    loadGeneralContracts = async (req:express.Request,res:express.Response) => {
        try {
            const result = await this.contractService.loadGeneralContracts(req.user.id)
            res.json({data:result})          
        } catch (e) {
            res.status(400).json(e.message)
        }  
    }

    loadContractByPropertyId = async (req:express.Request,res:express.Response) => {
        try {
            console.log({propertyid: req.body})
            const result = await this.contractService.loadContractByPropertyId(req.body.property_id)
            res.json({data:result})          
        } catch (e) {
            console.error(e)
            res.status(400).json(e.message)
        } 
    }

    loadContractDistinctCountries = async (req:express.Request,res:express.Response) => {
        try {
            const result = await this.contractService.loadContractDistinctCountries(req.user.id)
            res.json({data:result})          
        } catch (e) {
            res.status(400).json(e.message)
        } 
    }

    loadDistinctUnitForContract = async (req:express.Request,res:express.Response) => {
        try {
            const result = await this.contractService.loadDistinctUnitForContract(req.user.id)
            res.json({data:result})          
        } catch (e) {
            res.status(400).json(e.message)
        } 
    }

    loadContractByStatus = async (req:express.Request,res:express.Response) => {
        try {
            const result = await this.contractService.loadContractByStatus(req.body.property_id)
           
            res.json({data:result})          
        } catch (e) {
            res.status(400).json(e.message)
        } 
    }

    loadContractByUnitId =  async (req:express.Request,res:express.Response) => {
        try {
            console.log("SDFASDFASDFSADFASDFSADFSD")
            const result = await this.contractService.loadContractByUnitId(req.body.id)
            
            res.json({data:result})          
        } catch (e) {
            res.status(400).json(e.message)
        } 
    }


    loadBusinessNaturesList = async (req:express.Request,res:express.Response) => {
        try {
            const businessNatures = await this.contractService.loadBusinessNaturesList()
            res.json({data:businessNatures})          
        } catch (e) {
            res.status(400).json(e.message)
        }  
    }




    // ------------------------------------------ Add things -------------------------------------------------
    
    // Draft a new contract for people to sign
    addContractDraft = async (req:express.Request,res:express.Response) => {
        try {
            console.log(req.body)
            await this.contractService.addContractDraft(req.body)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            console.error(e)
            res.status(400).json(e.message)
        }    
    }

    activateContractPending = async (req:express.Request, res:express.Response) => {
        try {
            console.log(req.body)
            await this.contractService.activateContractPending(req.body.unit_id)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            console.error(e)
            res.status(400).json(e.message)
        } 
    }


    // ------------------------------------------ Update things -------------------------------------------------

    // When the contract is confirmed by tenants
    signContractByTenant = async (req:express.Request,res:express.Response) => {
        try {
            await this.contractService.signContractByTenant(req.body)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        }    
    }

    // To start the contract
    confirmClick = async (req:express.Request,res:express.Response) => {
        try {
            console.log({SFSADFSDFS:req.body.unit_id, FDEFEF: req.body})
            await this.contractService.confirmClick(req.body.contract_id, req.body.unit_id)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            res.status(400).json(e.message)
        } 
    }


    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete contract
    deleteContract = async (req:express.Request,res:express.Response) => {
        try {
            await this.contractService.deleteContract(req.body.id, req.body.unit_id)
            res.json({success:true})
            // Webpage has not prepared yet!!
        } catch (e) {
            console.error(e)
            res.status(400).json(e.message)
        } 
    }

    

    contractToPdf = async (req:express.Request,res:express.Response) => {
       
        try{
           
            const x = await this.contractService.contractToPdf(req.body.code)
            // res.download(path.resolve(`${__dirname}/../${req.body.code}.pdf`))
            // res.download(path.join(__dirname, `../${req.query.code}.pdf`));
            console.log(x)
            res.redirect(x[0])
        }
        catch (e) {
            console.log(e)
            res.status(400).json(e.message)
        } 
    }

    downloadContract = async (req:express.Request,res:express.Response) => {
        try{
            let path = require('path')
            // await this.contractService.contractToPdf(req.body.code)
            // res.download(path.resolve(`${__dirname}/../${req.body.code}.pdf`))
            res.download(path.join(__dirname, `../${req.query.code}.pdf`));
        }
        catch (e) {
            console.log(e)
            res.status(400).json(e.message)
        } 
    }

}