import * as express from 'express'

import {CalendarService} from '../services/calenderService'

export class CalendarRouter{
    private calendarService: CalendarService
    constructor(calendarService: CalendarService){
        this.calendarService = calendarService
    }

    router(){
        const router = express.Router()
        router.get("/scheduleDate", this.getScheduleDate)
        router.post('/toGetScheduleDetails', this.getScheduleDetails)
        router.post('/oneDate', this.getOneDate)
        router.post('/scheduleDate', this.AddScheduleDate)
        router.post('/scheduleContent', this.AddScheduleContent)
        router.patch('/scheduleContent', this.updateContent)
        router.patch('/toFinishScheduleContent', this.finishContent)
        router.delete('/scheduleContent', this.deleteContent)
        router.delete('/scheduleDate', this.deleteScheduleDate)
        return router
    }

    getScheduleDate = async (req:express.Request, res:express.Response) => {
        try{
            const getScheduleDate = await this.calendarService.getScheduleDate(req.user.id)
            console.log(getScheduleDate)
            res.json({data:getScheduleDate})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    
    getOneDate = async (req:express.Request, res:express.Response) => {
        try{
            const getOneDate = await this.calendarService.getOneDate(req.body, req.user.id)
            if (getOneDate.length > 0){
                await this.getScheduleDetails(req.body, req.user.id)
                // [CODE REVIEW] no response??
            }else{
                res.json(getOneDate)
            }
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }
    
    getScheduleDetails = async (req:express.Request, res:express.Response) => {
        try{
            const getScheduleDetails = this.calendarService.getScheduleDetails(req.body, req.user.id)
            res.json({data:getScheduleDetails})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    AddScheduleDate = async (req:express.Request, res:express.Response) => {
        try{
            const id = await this.calendarService.AddScheduleDate(req.body.calendar, req.user.id)
            res.json({data:id})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    AddScheduleContent = async (req:express.Request, res:express.Response) => {
        try{
            const id = await this.calendarService.AddScheduleContent(req.body.calendar, req.body.todo)
            res.json({data:id})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    updateContent = async (req:express.Request, res:express.Response) => {
        try{
            await this.calendarService.updateContent(req.body.todo)
            res.json({success:true})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    finishContent = async (req:express.Request, res:express.Response) => {
        try{
            await this.calendarService.finishContent(req.body)
            res.json({success:true})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }


    deleteContent = async (req:express.Request, res:express.Response) => {
        try{
            await this.calendarService.deleteContent(req.body)
            res.json({success:true})
        }catch(e){
            res.status(500).json({msg:e.message})
        }
    }

    deleteScheduleDate = async (req:express.Request, res:express.Response) => {
        try{
            const deleteScheduleDate = this.calendarService.deleteScheduleDate(req.body)
            res.json(deleteScheduleDate)
        }catch(e){
            res.status(500).json(e.message)
        }
    }

}
