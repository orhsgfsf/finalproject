import { isLoggedIn } from './guards'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as expressSession from 'express-session'
import * as Knex from 'knex'
import * as cors from 'cors'
import { HomeService } from './services/homeService'
import { HomeRouter } from './routers/homeRouter'
import { UserService } from './services/userService'
import { UserRouter } from './routers/userRouter'
import { CalendarService } from './services/calenderService';
import { CalendarRouter } from './routers/calenderRouter';
import { PropertyService } from './services/propertyService';
import { PropertyRouter } from './routers/propertyRouter';
import { UnitService } from './services/unitService';
import { UnitRouter } from './routers/unitRouter';
import { ContractService } from './services/contractService';
import { ContractRouter } from './routers/contractRouter';
import { AuthRouter } from './routers/authRouter';
import { AnnualReportService } from './services/annualReportService'
import { AnnualReportRouter} from './routers/annualReportRouter';
import { FloorPlanService } from './services/floorPlanService'
import { FloorPlanRouter } from './routers/floorPlanRouter'
import { TenantContractService } from './services/tenantContractService'
import { TenantContractRouter } from './routers/tenantContractRouter'
import * as passport from "passport"
import * as aws from 'aws-sdk'
import * as multer from 'multer'
import * as multerS3 from 'multer-s3'

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"] )

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});


const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'cdn.danny.hk',
        metadata: (req,file,cb)=>{
            cb(null,{fieldName: file.fieldname});
        },
        key: (req,file,cb)=>{
            cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})

const sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false}
});

app.use(sessionMiddleware);

app.use(passport.initialize());
import './passport';




const homeService = new HomeService(knex)
const calendarService = new CalendarService(knex)
const propertyService = new PropertyService(knex)
const unitService = new UnitService(knex)
const contractService = new ContractService(knex)
const annualReportService = new AnnualReportService(knex)
const floorPlanService = new FloorPlanService(knex)
const tenantContractService = new TenantContractService(knex)
export const userService = new UserService(knex)

const homeRouter = new HomeRouter(homeService)
const authRouter = new AuthRouter(userService)
const userRouter = new UserRouter(userService)
const calendarRouter = new CalendarRouter(calendarService)
const propertyRouter = new PropertyRouter(propertyService)
const unitRouter = new UnitRouter(unitService)
const contractRouter = new ContractRouter(contractService)
const floorPlanRouter = new FloorPlanRouter(floorPlanService, upload)
const tenantContractRouter = new TenantContractRouter(tenantContractService)
const annualRouter = new AnnualReportRouter(annualReportService)

app.use(express.static(__dirname + '/testing'));
app.use('/auth', authRouter.router())
app.use('/tenantContracts', tenantContractRouter.router())
app.use('/contracts', isLoggedIn, contractRouter.router())
app.use('/user', isLoggedIn, userRouter.router())
app.use('/home', isLoggedIn, homeRouter.router())
app.use('/calendar', isLoggedIn, calendarRouter.router())
app.use('/properties', isLoggedIn, propertyRouter.router())
app.use('/units', isLoggedIn, unitRouter.router())
app.use('/contracts', isLoggedIn, contractRouter.router())
app.use('/annual', isLoggedIn, annualRouter.router())

app.use('/floorPlan', isLoggedIn, floorPlanRouter.router())

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
