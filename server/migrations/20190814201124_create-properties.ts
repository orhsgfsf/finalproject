import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("properties", (table) => {
        table.increments()

        // table.integer("total_leasable_units").notNullable()
        table.string("floor_plan")
        table.string("currency").notNullable()
        table.integer("gross_floor_area") // compulsory to fill one between "gfa" or "sa", but allow null for both 
        table.integer("saleable_area")
        table.decimal("government_rent", 10, 2)
        table.decimal("management_fee", 10, 2)
        table.decimal("rates", 10, 2)
        table.decimal("deposit", 10, 2)
        table.string("address").notNullable()
        table.boolean("property_status").notNullable().defaultTo(true)
        table.text("reminder") //reminder likes address

        // table.integer("currency_id").unsigned()
        // table.foreign("currency_id").references("currencies.id")

        table.integer("user_id").unsigned()
        table.foreign("user_id").references("users.id")

        table.integer("property_type_id").unsigned()
        table.foreign("property_type_id").references("property_types.id")

        table.integer("country_id").unsigned()
        table.foreign("country_id").references("countries.id")
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("properties")
}

