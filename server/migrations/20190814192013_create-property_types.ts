import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("property_types", (table) =>{
        table.increments()
        
        table.string("propertyType").notNullable()

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("property_types")
}

