import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("countries", (table) =>{
        table.increments()
        
        table.string("country").notNullable()

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("countries")
}

