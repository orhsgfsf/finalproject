import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("contracts", (table)=>{
        table.increments()
        
        table.decimal("rent_price", 10, 2).notNullable()
        table.integer("rent_due_date").notNullable() //whether number or date as it is a fixed number in every month e.g. 2 Jan; 2 Feb; 2 Mar etc.
        table.date("contract_starting_date").notNullable()
        table.date("contract_expiry_date").notNullable()
        table.decimal("deposit", 10, 2).notNullable()
        table.string("currency").notNullable()
        table.integer("grace_period").notNullable() //depends on how many days that landlord will free to tenant
        table.text("reminder") // in case  a grace period excluded, a landlord can drop down notes to remind himself e.g. (1/7sign; 1/8 grace period; 1/9 contract period)
        table.integer("keys").notNullable() //An optional requirement in contract
        table.string("code").notNullable()
        table.boolean("contract_status").notNullable().defaultTo(false) //true = activate; false = voided?
        table.string("tenant_name") //Whether the tenant is company or person, it is a role only. Do we have to create another column?
        table.string("contact_person") //Optional for tenant to fill in convenience for landlord to contact.
        table.integer("contact_number")
        // table.string("tenant_company").notNullable()
        table.string("tenant_id_or_br") //same case as row17, both id and br can merge as tenant_id
        table.string("tenant_business_nature") //confusing whether to set nature in contract or not
        table.text('landlord_signature').notNullable() //Do we have to save the signature? Even saving signature, is this the right setting?
        table.text('tenant_signature')
        table.boolean('confirm_clicked').notNullable().defaultTo(false)
        table.string("landlord_name").notNullable()
        table.string("landlord_id_or_br").notNullable()
        table.date("grace_period_starting_date").notNullable()
        table.date("grace_period_expiry_date").notNullable()
        table.date("void_contract_date")
        

        // table.integer("currency_id").unsigned()
        // table.foreign("currency_id").references("currencies.id")

        // table.integer("contract_type_id").unsigned()
        // table.foreign("contract_type_id").references("contract_types.id")

        table.integer("unit_id").unsigned()
        table.foreign("unit_id").references("units.id")

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("contracts")
}
