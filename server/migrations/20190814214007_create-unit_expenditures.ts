import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("contract_expenditures", (table) => {
        table.increments()

        table.decimal('date').notNullable()
        table.decimal("expenditure", 10, 2).notNullable() 

        table.integer("unit_fee_type_id").unsigned()
        table.foreign("unit_fee_type_id").references("unit_fee_types.id")

        table.integer("unit_id").unsigned()
        table.foreign("unit_id").references("units.id")

        table.timestamps(false,true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("contract_expenditures")
}
