import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("contract_incomes", (table) => {
        table.increments()

        table.decimal('received_amount', 10, 2)
        table.date("rent_due_date").notNullable() //whether number or date as it is a fixed number in every month e.g. 2 Jan; 2 Feb; 2 Mar etc.
        table.date('received_date')
      

        table.integer("contract_id").unsigned()
        table.foreign("contract_id").references("contracts.id")

        table.timestamps(false,true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("contract_incomes")
}
