import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("potential_tenants", (table)=>{
        table.increments()
        
        
        table.string("tenant_full_name").notNullable() //Whether the tenant is company or person, it is a role only. Do we have to create another column?
        table.string("contact_person") //Optional for tenant to fill in convenience for landlord to contact.
        table.integer("contact_number").notNullable()
        // table.string("tenant_company").notNullable()
        table.string("tenant_id_or_br").notNullable() //same case as row17, both id and br can merge as tenant_id
        

        table.integer("business_nature_id").unsigned()
        table.foreign("business_nature_id").references("business_natures.id")

        // table.integer("contract_type_id").unsigned()
        // table.foreign("contract_type_id").references("contract_types.id")


        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("potential_tenants")
}
