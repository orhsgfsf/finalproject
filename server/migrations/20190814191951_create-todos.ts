import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    return knex.schema.createTableIfNotExists("todos", (table) => {
        table.increments()

        table.text('todo')
        table.boolean('todo_status')

        table.integer("calendar_id").unsigned()
        table.foreign("calendar_id").references("calendars.id")

        table.timestamps(false,true)
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("todos")
}

