import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("currencies", (table) => {
        table.increments()
        table.string("currency").notNullable()//symbol? e.g "$" or "hkd"/"usd"

        table.timestamps(false, true)

    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("currencies")
}