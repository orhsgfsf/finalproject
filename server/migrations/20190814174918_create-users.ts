import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("users", (table) => {
        table.increments()
        
        table.string('username').notNullable()
        table.string('password').notNullable()
        table.string('first_name') //*
        table.string('last_name')
        table.string('gender')//*
        table.date('date_of_birth')//*
        table.integer('district_number')
        table.integer('contact_number')//*
        table.string('email').notNullable()
        table.string('id_number')//*
        table.string('company')//*
        table.string('profile_picture')//*
        table.string('br_number')//*
        table.string('login_time')//*

        table.timestamps(false,true)
    })
}


export async function down(knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("users")
}

