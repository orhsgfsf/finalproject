import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("calendars", (table) => {
        table.increments()
        
        table.date('date').notNullable()
        table.text('content').notNullable() //content likes address

        table.integer("user_id").unsigned()
        table.foreign("user_id").references("users.id")

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("calendars")
}

