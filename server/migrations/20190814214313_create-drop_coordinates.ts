import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("drop_coordinates", (table) => {
        table.increments()

        table.integer("x").notNullable()
        table.integer("y").notNullable()
        table.integer("height").notNullable()
        table.integer("width").notNullable()
   
        table.integer("unit_id").unsigned()
        table.foreign("unit_id").references("units.id")

        table.timestamps(false, true)

    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("drop_coordinates")
}