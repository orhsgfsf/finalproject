import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("addresses", (table) => {
        table.increments()
        table.text("address").notNullable()

        table.integer("country_id").unsigned()
        table.foreign("country_id").references("countries.id")

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex) {
    return knex.schema.dropTableIfExists("addresses")
}

