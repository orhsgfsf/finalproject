import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("property_expenditures", (table) => {
        table.increments()

        table.date("date").notNullable()
        table.decimal("expenditure",10, 2).notNullable()

        // table.integer("property_fee_type_id").unsigned()
        // table.foreign("property_fee_type_id").references("property_fee_types.id")

        table.integer("property_id").unsigned()
        table.foreign("property_id").references("properties.id")

        table.timestamps(false, true)

    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("property_expenditures")
}
