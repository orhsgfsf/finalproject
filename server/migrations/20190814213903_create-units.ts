import * as Knex from "knex";


export async function up(knex: Knex){
    return knex.schema.createTableIfNotExists("units", (table) => {
        table.increments()

        table.string("unit").notNullable() //can users input either integer or string e.g "B1" or "1"?
        table.text("reminder") //reminder likes address
        table.integer("gross_floor_area") // compulsory to fill one between "gfa" and "sa", but allow null for both 
        table.integer("saleable_area")    
        table.boolean("rental_status").notNullable().defaultTo(false)
        table.boolean("unit_status").notNullable().defaultTo(true) 
        table.boolean("contract_pending").notNullable().defaultTo(false)  
        table.integer("x").notNullable().defaultTo(0)
        table.integer("y").notNullable().defaultTo(0)
        table.integer("height").notNullable().defaultTo(100)
        table.integer("width").notNullable().defaultTo(100)

        table.integer("property_id").unsigned()
        table.foreign("property_id").references("properties.id")

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("units")
}

