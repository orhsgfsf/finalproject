import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("unit_fee_types", (table) =>{
        table.increments()
        
        table.string("type").notNullable()

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("unit_fee_types")
}
