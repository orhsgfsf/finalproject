import * as Knex from "knex";


export async function up(knex: Knex) {
    return knex.schema.createTableIfNotExists("business_natures", (table) =>{
        table.increments()
        
        table.string("businessNature").notNullable()

        table.timestamps(false, true)
    })
}


export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("business_natures")
}
