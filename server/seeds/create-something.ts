

import * as jsonfile from "jsonfile";
import * as Knex from "knex";

// const place = require('countrycitystatejson')

export async function seed(knex: Knex) {
//     // Deletes ALL existing entries
//     await knex("cities").del()
//     await knex("districts").del()
//     await knex("hk_districts").del()
    await knex("currencies").del()
    await knex("countries").del()
    await knex("property_types").del()
//     await knex("business_natures").del()
//     await knex("fee_types").del()
   

//     // Inserts seed entries

    // countries
    const countries = await jsonfile.readFileSync(`./seeds/countries.json`)
    for (let i = 0; i < Object.keys(countries).length; i++) {
        await knex.insert({
            country: countries[i].name,
            // short_name: countries[i].shortName
        }).into("countries")       
    }

    // districts/states and currencies
    for (let i = 0; i < Object.keys(countries).length; i++) {
        // const states = place.getStatesByShort(countries[i].shortName)

        await knex.insert({
            currency: countries[i].currency,
            // country_id: (await knex.select("id").from("countries").where("countries.country", countries[i].name))[0].id
        }).into("currencies")
    }

//         if (states) {
//             for (let j = 0; j < states.length; j++) {
//                 await knex.insert({
//                     district: states[j],
//                     country_id: (await knex.select("id").from("countries").where("countries.country", countries[i].name))[0].id
//                 }).into("districts")
//             }
//         }
//     }

//     const hk_districts = [
//         ["Central and Western","Hong Kong Island"],
//         ["Eastern", "Hong Kong Island"],
//         ["Southern", "Hong Kong Island"],
//         ["Wan Chai", "Hong Kong Island"],
//         ["Sham Shui Po", "Kowloon"],
//         ["Kowloon City", "Kowloon"],
//         ["Kwun Tong", "Kowloon"],
//         ["Wong Tai Sin", "Kowloon"],
//         ["Yau Tsim Mong", "Kowloon"],
//         ["Islands", "New Territories"],
//         ["Kwai Tsing", "New Territories"],
//         ["North", "New Territories"],
//         ["Sai Kung", "New Territories"],
//         ["Sha Tin", "New Territories"],
//         ["Tai Po", "New Territories"],
//         ["Tsuen Wan", "New Territories"],
//         ["Tuen Mun", "New Territories"],
//         ["Yuen Long", "New Territories"]
//     ]

    
//     for (let i = 0; i < hk_districts.length; i++){
        
//         await knex.insert({
//             hk_districts: hk_districts[i],
//             country_id: (await knex.select("id").from("countries").where("countries.country", "Hong Kong"))[0].id
//         }).into("hk_districts")
//     }


//     // cities
//     for (let i = 0; i < Object.keys(countries).length; i++) {
//         const states = place.getStatesByShort(countries[i].shortName)
//         if (states) {
//             for (let j = 0; j < states.length; j++) {
//                 const cities = place.getCities(countries[i].shortName, states[j])
//                 if (cities) {
//                     for (let k = 0; k < cities.length; k++) {
//                         await knex.insert({
//                             city: cities[k],
//                             district_id: (await knex.select("id").from("districts").where("districts.district", states[j]))[0].id
//                         }).into("cities")
//                     }
//                 }
//             }
//         }
//     }


    // property types
    await knex.insert([
        { propertyType: "Residential" },
        { propertyType: "Industrial" },
        { propertyType: "Office" },
        { propertyType: "Retail" }
    ]).into("property_types")




    // business nature
    await knex.insert([
        { businessNature: "Government" },
        { businessNature: "Accessories" },
        { businessNature: "Electrical and Electronic Products" },
        { businessNature: "Restaurant" },
        { businessNature: "Clothes" },
        { businessNature: "House ware" },
        { businessNature: "Metals and Mineral" },
        { businessNature: "Optical and Photographic Products" },
        { businessNature: "Machinery" },
        { businessNature: "Pharmacy" },
        { businessNature: "Printing" },
        { businessNature: "Publishing" },
        { businessNature: "Toys" },
        { businessNature: "Watches and Clocks" },
        { businessNature: "Banking" },
        { businessNature: "Financing Services" },
        { businessNature: "Real Estate" },
        { businessNature: "Building & Construction" },
        { businessNature: "Environmental" },
        { businessNature: "Trade" },
        { businessNature: "Insurance" },
        { businessNature: "Logistics" },
        { businessNature: "Professional Services" },
        { businessNature: "Medical and Health" },
        { businessNature: "Beauty" },
        { businessNature: "Qualification Validation" },
        { businessNature: "Telecommunications" },
        { businessNature: "Tourism" },
        { businessNature: "Retail" },
    ]).into("business_natures")
}

//     // fee types
//     await knex.insert([
//         { fee_type: "Other expenses" },
//         { fee_type: "Water expenses" },
//         { fee_type: "Electricity expenses" },
//         { fee_type: "Gas expenses" }

//     ]).into("fee_types")

//     // paid_status
//     await knex.insert([
//         { rent_status: "Paid"},
//         { rent_status: "Late Paid"},
//         { rent_status: "Not Paid"},
//     ])
