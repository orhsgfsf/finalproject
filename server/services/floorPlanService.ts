import * as Knex from 'knex'



export class FloorPlanService{
    constructor(private knex:Knex){

    }

    uploadPic = async (iconLocation:string, userId:number) => {
        console.log(iconLocation)
        await this.knex('users').update({profile_picture:iconLocation}).where('users.id', userId)
    }

    uploadFloorPlan = async (floorPlanLocation:string, property_id:number) => {
        await this.knex('users').update({floor_plan:floorPlanLocation}).where('property.id', property_id)
    }
}