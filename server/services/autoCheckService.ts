import * as Knex from 'knex'
import * as moment from 'moment'

export class AutoCheckService {
    constructor(private knex: Knex) {
    }

    autoCheckEveryday = async () => {
        const contracts = await this.knex.select("contract_incomes.rent_due_date", "contract_incomes.contract_id")
            .from("contract_incomes")
            .innerJoin("contracts", "contracts.id", "contract_incomes.contract_id")
            .where("contracts.rental_status", true)
            .orderBy("contracts.id")
            .orderBy("contract_incomes.rent_due_date")

        for (let contract of contracts) {
            const rent_due_date = moment(contract.rent_due_date)
            const now = moment()

            if (rent_due_date.diff(now, 'days') < 0) {
                const daysInMonth = moment().daysInMonth()
                const newRentDueDate = moment(contract.rent_due_date).add(daysInMonth, 'days')

                const newIncome = {
                    received_amount: null,
                    rent_due_date: newRentDueDate,
                    received_date: null,
                    contract_id: contract.contract_id
                } 

                await this.knex.insert(newIncome).into("contract_incomes")
            }
        }
    }
}