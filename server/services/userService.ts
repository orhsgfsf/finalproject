import * as Knex from 'knex';
import { hashPassword } from '../hash';


export class UserService{
    constructor(private knex:Knex){}


    getUser(username:string){
        return this.knex.select('*').from('users').where('username',username);
    }

    getUserByEmail(email:string){
        return this.knex.select('*').from('users').where('email',email);
    }

    getUserDataAll(userId:number){
        return this.knex.select('*').from ('users').where('id', userId)
    }

    async createUser(email:string){
        // const randomString = Math.random().toString(36)
        return this.knex.insert({
            email: email,
            username: email.split("@")[0],
            password: null,
        }).into('users').returning("*");
    }

    checkUserReg = async (username: string) => {
            return await this.knex.select('username').from("users").where("username", username).first()
        }

    addUser = async(username:string, password:string, email:string) => {
        await this.knex.insert({
            username: username,
            password: await hashPassword(password),
            email: email
        }).into("users")
    }

    addPersonalInfo = async(personalInfo:any, userId:number) => {
        await this.knex("users").update(personalInfo).where("users.id", userId)
    }

    updateUserData = async(personalInfo:any, userId:number) => {
        await this.knex("users").update(personalInfo).where("users.id", userId)
    }


    updateLoginTime = async (loginTime:string, userId:number) => {
        await this.knex("users").update(loginTime).where("users.id", userId)
    }
}












// import { hashPassword } from '../hash'
// import * as Knex from 'knex'

// export interface User {
//     id?: number;
//     username: string;
//     password: string;
//     name_eng: string;
//     gender: string;
//     date_of_birth: number;
//     contact_number: number;
//     email: string;
//     id_number: number;
// }

// export class UserService {
//     constructor (private knex: Knex){
//     }

//     getUserData = async () => {
//         return this.knex.select("*").from("users")
//     }
    
//     addUser = async (username: string, password: string, name_eng: string, gender: string, date_of_birth: number, contact_number: number, email: string, id_number: number) => {
//         const hash = await hashPassword(password)
//         await this.knex.insert({
//             username: username,
//             password: hash,
//             name_eng: name_eng,
//             gender: gender,
//             date_of_birth: date_of_birth,
//             contact_number: contact_number,
//             email: email,
//             id_number: id_number
//         }).into("users").returning("id")
//     }

//     checkUserReg = async (username: string) => {
//         return await this.knex.select('username').from("users").where("username", username).first()
//     }
// }