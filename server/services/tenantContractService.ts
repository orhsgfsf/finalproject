import * as Knex from 'knex'

const puppeteer = require('puppeteer');


export interface Contract {

    // -------------------------------------- For contracts --------------------------------------
    id?: number
    rentPrice: number
    rentDueDate: number
    startingDate: number
    expiryDate: number
    deposit: number
    gracePeriod: number
    gracePeriodReminder: string
    keys: number
    code: string
    landlordSignature: string
    tenantSignature: string
    startContract: Date
    endContract: Date
    IdOrBrNumber: string
    landlordName: string
    unitId: number
    startGracePeriod: Date
    endGracePeriod: Date
    contract_pending: boolean




    // foreign key
    unit_id: number
    currency_id: number

    // -------------------------------------- For tenants --------------------------------------
    tenantName: string
    tenantIdOrBr: string
    contactPerson: string
    contactNumber: number
    businessNature: string

    // -------------------------------------- For currency --------------------------------------
    currency: string
}

export class TenantContractService {
    constructor(private knex: Knex) {
    }

    loadContractByCode = async (contract: Contract) => {
        return await this.knex.select(
            "contracts.*",

            "units.unit",

            "property_types.propertyType",

            "properties.address", "properties.management_fee", "properties.government_rent", "properties.rates", "properties.saleable_area", "properties.gross_floor_area",

            "countries.country",
        )
            .from("contracts")

            .innerJoin("units", "contracts.unit_id", "units.id")
            .innerJoin("properties", "properties.id", "units.property_id")
            .innerJoin("property_types", "property_types.id", "properties.property_type_id")

            .innerJoin("countries", "countries.id", "properties.country_id")
            .where("contracts.code", contract.code)
    }


    loadBusinessNaturesList = async () => {
        return await this.knex.select("*").from("business_natures")
    }


    signContractByTenant = async (contract: Contract) => {


        // Add tenant
        const tenant = {
            tenant_name: contract.tenantName,
            contact_person: contract.contactPerson,
            contact_number: contract.contactNumber,
            tenant_id_or_br: contract.tenantIdOrBr,
            tenant_business_nature: contract.businessNature,
            tenant_signature: contract.tenantSignature
        }

        await this.knex("contracts").update(tenant).where("contracts.code", contract.code)

    }
 
    contractToPdf = async (path: string) => {
        console.log("S33333333333333S3333")
        const browser = await puppeteer.launch({
            ignoreHTTPSErrors: true,
            headless: true,
            args: ['--no-sandbox']
        })
        const page = await browser.newPage();
        await page.goto(`${process.env.PDF_PREVIEW_URL}/contract_preview/${path}`, { waitUntil: 'networkidle2', ignoreHTTPSErrors: true });
        // await page.goto(`https://localhost:3000/contract_preview/${path}`, {waitUntil: 'networkidle2', ignoreHTTPSErrors: true});
        const AWS = require("aws-sdk");
        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
            region: 'ap-southeast-1'
        });
        const bucket = "cdn.danny.hk";
        const key =
            `${Date.now()}-floor_plan.pdf`

        const pdf = await page.pdf({ path: `${path}.pdf`, format: 'A4' });
        const params = { Bucket: bucket, Key: key, Body: pdf };
        await s3.putObject(params).promise();
        await browser.close();

        const x = await s3.getSignedUrl('getObject', {Bucket: bucket, Key: key});
        console.log(x)
        return x
        
    };


}