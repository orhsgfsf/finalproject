import * as Knex from 'knex'


export interface Property {
    id: number
    totalLeasableUnits: number
    managementFee: number
    governmentRent: number
    rates: number
    propertyStatus?: boolean
    reminder: string

    // foreign key
    user?: number
    propertyType: number
    street: string
    district: string
    flat: string | null
    floor: string | null
    tower: string | null
    court: string | null
    saleableArea: number | null
    grossFloorArea: number | null
    floorPlan: string | null
    address: string
    country: string
    currency: string
    country_id:number
    image:string
}


export class PropertyService {
    constructor(private knex: Knex) {
    }


    // ------------------------------------------ Get things -------------------------------------------------

    // Get the number of properties and the general things of properties
    loadNumberOfProperties = async (userId: number) => {
        return await this.knex.select("properties.id",
            "properties.address",
            "countries.country")
            .from("properties")
            .innerJoin("countries", "countries.id", "properties.country_id")
            .innerJoin("users", "users.id", "properties.user_id")
            .where("properties.property_status", true)
            .where("users.id", userId)
            .orderBy("countries.country")
    }

    // Get distinct country
    loadDistinctCountries = async (userId:number) => {
        
        const x = await this.knex.distinct("countries.*")
        .from("properties")
        .innerJoin("countries", "properties.country_id", "countries.id")
        .where("properties.property_status", true)
        .where("properties.user_id", userId)
        .orderBy("countries.country", "asc")
       
        return x
    }

    // Get countries list
    loadCountriesList = async () => {
        return await this.knex.select("*").from("countries")
    }

    // Get currencies list
    loadCurrenciesList = async () => {
        return await this.knex("currencies").distinct("currency").orderBy("currency", "asc")
    }

    // Get property types list 
    loadPropertyTypesList = async () => {
        return await this.knex.select("*").from("property_types")
    }

    // Get the details of properties
    loadDetailsOfProperties = async (property: Property, userId:number) => {
        return await this.knex.select("properties.*",           
            "countries.country",
            "property_types.propertyType")
            .from("properties")
            .innerJoin("countries", "countries.id", "properties.country_id")
            .innerJoin("property_types", "properties.property_type_id", "property_types.id")
            .where("properties.property_status", true)
            .andWhere("properties.id", property.id)
            .andWhere("properties.user_id", userId)
    }

    // ------------------------------------------ Add things -------------------------------------------------

    // Start inserting things about the property to database
    addDetailsOfProperty = async (userID: number, property: Property) => {
       
       
        // Get all the data from the database we need first

        const propertyTypeDB = await this.knex.select("id").from("property_types").where("property_types.propertyType", property.propertyType).first()
        const countryDB = await this.knex.select("id").from("countries").where("countries.country", property.country).first()
        // const currencyDB = await this.knex.select("id").from("currencies").where("currencies.id", property.currency).first()
    
        // Start inserting things to table of "properties"

        const propertyDetails = {
            address: property.address,
            gross_floor_area: property.grossFloorArea,
            saleable_area: property.saleableArea,
            currency: property.currency,
            management_fee: property.managementFee,
            government_rent: property.governmentRent,
            rates: property.rates,
            property_status: true,
            reminder: property.reminder,
            user_id: userID,
            property_type_id: propertyTypeDB.id,
            country_id: countryDB.id,
           
        }

        return [await this.knex.insert(propertyDetails).into("properties").returning("id"), [countryDB.id]]
    }

    // Start inserting things to table of "property_floor_plans"
    updateFloorPlans = async (property: Property) => {
        const floorPlan = {
            floor_plan: property.image,          
        }
        console.log(floorPlan)
       await this.knex("properties").update(floorPlan)
       .where("properties.id", property.id)
    }


    // ------------------------------------------ Update things -------------------------------------------------

    // Start updating details of the property
    updateDetailsOfProperty = async (userID: number, property: Property) => {
        console.log("I am now going to delete things")
        console.log(property)
        await this.deleteProperty(property)
        return await this.addDetailsOfProperty(userID, property)
    }


    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete details of the property by changing it's status to be "history"
    deleteProperty = async (property: Property) => {
        await this.knex("properties").update({ property_status: false }).where("properties.id", property.id)
       
    }

    checkDistinctCountry = async (property: Property) => {      
        console.log(property)
        return await this.knex.select("properties.*").from("properties").where("properties.property_status",true).andWhere("properties.country_id", property.country_id)
    }
}

