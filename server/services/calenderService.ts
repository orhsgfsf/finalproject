import * as Knex from 'knex'
export interface Calendar {
    id: number
    date: number
    content: string
    user_id: number
}

export interface Todo {
    id: number
    todo: string
    todoStatus: boolean
    calendar_id: number

}

export class CalendarService {
    constructor(private knex:Knex){
    }

    getScheduleDate = async (userId:number) => {
        console.log("SUCCESS?")
        return await this.knex.select("id","date").from("calendars")
        .where("calendars.user_id", userId)
    }

    getOneDate = async (date:Date, userId:number) => {
        return await this.knex.select("calendars.*").from("calendars")
        .where("calendars.date", date)
        .andWhere("calendars.user_id", userId)
    }

    getScheduleDetails = async(calendar:Calendar, userId:number) => {
        return await this.knex.select("todos.*").from("todos")
        .innerJoin("calendars", "todos.calendar_id", "calendars")
        .where("calendars.date", calendar.date)
        .andWhere("calendars.user_id", userId)
    }


    AddScheduleDate = async(calendar:Calendar, userId:number) => {
        return await this.knex.insert({
            date: calendar.date,
            user_id: userId
        }).into("calendar").returning("id").first()      
    }

    AddScheduleContent = async(calendar:Calendar, todo:Todo) => {
        return await this.knex.insert({
            todo: todo.todo,
            todo_status: true,
            calendar_id: calendar.id
        }).into("todos").returning("id").first()
    }








    updateContent = async(todo:Todo) => {
        await this.knex("todos").update({
            todo: todo.todo,
            todo_status: true
        })
        .where("todos.id", todo.id)
    }

    finishContent = async(todo:Todo) => {
        await this.knex("todos").update({
            todo_status: false
        })
        .where("todos.id", todo.id)
    }









    deleteContent = async(todo:Todo) => {
        await this.knex.delete("todos").where("todos", todo.id)
    }

    deleteScheduleDate = async(calendar:Calendar) => {
        await this.knex.delete("calendar").where("calendar.id", calendar.id)
    }
}