import * as Knex from 'knex'


export interface AnnualReport {
    // ------------------------------ For annual report ----------------------------------------
    receivedAnoumt: number
    receivedDate: number
    expenditure: number

    // ------------------------------ For pay rent ----------------------------------------
    fee: number
    contractDB: number
    feeTypeDB: number

    // ------------------------------ For date --------------------------------------------
    startYear: number
    endYear: number
}

export class AnnualReportService {
    constructor(private knex: Knex) {
    }


    // ------------------------------------------ Get things -------------------------------------------------

    loadAllUnitsForReport = async (userId:number) => {
        return await this.knex.select(
            "units.*",
            "address.full_address",
            "countries.country"
        )
        .from("units")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("addresses", "addresses.id", "properties.address_id")
        .innerJoin("countries", "countries.id", "addresses.country_id")        
        .where("properties.user_id", userId)
        .orderBy("countries.country")
        .orderBy("properties.id")
        .orderBy("units.unit_status")
        .orderBy("units.id")
        // Still have to type condition of date
    }

    loadRentDueDay = async (userId:number) => {
        return await this.knex.select(
            "contract_incomes.rent_due_date",
            "units.unit",
            "address.full_address",
            "countries.country"
        )
        .from("contract_incomes")
        .innerJoin("contracts", "contract_incomes.contract_id", "contracts.id")
        .innerJoin("units", "contracts.unit_id", "units.id")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("addresses", "addresses.id", "properties.address_id")
        .innerJoin("countries", "countries.id", "addresses.country_id")
        .where("properties.user_id", userId)
        .orderBy("contract_incomes.rent_due_date")
        .orderBy("countries.country")
        .orderBy("properties.id")
        .orderBy("units.unit_status")
        .orderBy("units.id")
    }

    loadDetailsOfReport = async (contractId:number) => {
        
    }

    generateReport = async (userId:number) => {
        return await this.knex.select({
            contracts_id: 'contracts.id',
            contracts_rent_price: 'contracts.rent_price',
            contracts_rent_due_date: 'contracts.rent_due_date',
            contracts_starting_date: 'contracts.contract_starting_date',
            contracts_expiry_date: 'contracts.contract_expiry_date',
            contracts_deposit: 'contracts.deposit',
            contracts_grace_period: 'contracts.grace_period',
            contracts_tenant_full_name: 'contracts.tenant_full_name',
            contracts_contact_person: 'contracts.contact_person',
            contracts_contact_number: 'contracts.contact_number',
            contracts_tenant_id_or_br: 'contracts.tenant_id_or_br',
            contracts_tenant_business_nature: 'contracts.tenant_business_nature',
            contracts_currency: 'contracts.currency',
            units_id: 'units.id',
            units_unit: 'units.unit',
            units_saleable_area: 'units.saleable_area',
            units_gross_floor_area: 'units.gross_floor_area',   
            properties_id: 'properties.id',
            properties_government_rent: 'properties.government_rent',
            properties_management_fee: 'properties.management_fee',
            properties_rate: 'properties.rates',
            properties_address: 'properties.address',
            property_types_property_types_id: 'property_types.id',
            property_types_property_types_propertyType: 'property_types.propertyType',
            users_id: 'users.id',

        })
        .from("contracts")
        .innerJoin("units", "contracts.unit_id", "units.id")
        .innerJoin("properties", "units.property_id", "properties.id")
        .innerJoin("countries", "properties.country_id", "countries.id")
        .innerJoin("property_types", "properties.property_type_id", "property_types.id")
        .innerJoin("users", "properties.user_id", "users.id")
        .where("properties.user_id", userId)
    }


}