import * as Knex from 'knex'
import * as moment from 'moment'

export interface Schedule {
    date: number;
    content: string;
    
    // foreign key
    user_id: number
}

export class HomeService{
    constructor(private knex:Knex){
    }



// ------------------------------------------ Get things -------------------------------------------------

    // Load user data first
    loadUserData = async (id:number) => {
        return await this.knex.select("users.first_name", "users.login_time", "users.profile_picture", "users.gender")
        .from("users")
        .where("users.id", id)
    }

    // To show the notification and to make the target day in the calender become red colour
    loadNotification = async (id:number) => {
       
        
        return await this.knex.select("contract_incomes.rent_due_date",
        "contracts.rent_price",
        "currencies.currency",
        "units.unit",
        "addresses.full_address",
        "countries.country"
        ).from("contract_incomes")
        .innerJoin("contracts", "contracts.id", "contract_incomes.contract_id")
        .innerJoin("currencies", "currencies.id", "contracts.currency_id")
        .innerJoin("units", "units.id", "contracts.unit_id")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("addresses", "addresses.id", "properties.address_id")
        .innerJoin("countries", "countries.id", "addresses.country_id")
        .innerJoin("users", "users.id", "properties.user_id")
        .orderBy("contract_incomes.rent_due_date").orderBy("countries.country")
        .where('contract_incomes.rent_due_date', '>=', moment().add(-3, 'day').format('YYYY-MM-DD'))
        .where('contract_incomes.rent_due_date', '<=', moment().add(3, 'day').format('YYYY-MM-DD'))
        .where("users.id", id)
    }

    // To show the result of METHOD "loadNotification()" if the date difference between today and the rent due day is not more than 3 days
    checkDateDifference = async (id:number) => {
        const check = await this.loadNotification(id)
      
        const filteredCheck = check.filter((notLongerThan3days:any) => (notLongerThan3days.rent_due_date).diff(moment(), 'days') <= 3)
      
        return filteredCheck
    }

    // To show what the landlord have to do today
    loadTodaySchedule = async (id:number) => {
        return await this.knex.select("todos.todo","todos.todo_status").from("todos")
        .innerJoin("calendar", "calendar.id", "todos.calendar_id")
        .where("calendar.date", moment().format("YYYY-MM-DD"))
        .andWhere("calendar.user_id", id)
    }

    // // To make the target day in the calender become green colour
    // loadScheduleDateAll = async (id:number) => {
    //     return await this.knex.select("calendar.date").from("calendar")
    //     .where("calendar.user_id", id)
    // }

    //  // When the date in the calender is being clicked, show what the landlord have to do that day
    // loadCalenderSchedule = async (date:number, id:number) => {
    //     return await this.knex.select("*").from("todos")
    //     .innerJoin("calendar", "calendar.id", "todos.calendar_id")
    //     .where("calendar.date", date)
    //     .andWhere("calendar.user_id", id)
    // }


// ------------------------------------------ Add things -------------------------------------------------

    // // When I click "Save" in the schedule edit area in the calender after finishing creating schedule
    // createCalenderSchedule = async (schedule:Schedule, id:number) => {
    //     const things = {
    //         date: schedule.date,
    //         content: schedule.content,
    //         user_id: id
    //     }
    //     await this.knex.insert(things)
    //     .into("schedule")
    // }


// ------------------------------------------ Update things -------------------------------------------------

    // // When I click "Save" in the schedule edit area in the calender after finishing editing schedule
    // editCalenderSchedule = async (scheduleId:number, things:string) => {
    //     await this.knex("schedule").update(things).where("schedule.id", scheduleId)
    // }

     // When I login and get into the login page, 
    // the system will automatically change the last login time on the website
    changeLoginTime = async (id:number, lastLoginTime:number) => {
        await this.knex("users").update({login_time: lastLoginTime})
        .where("users.id", id)
    } 

    // When I click "Change profile picture"
    // How to do that? still multer? or React?   
}