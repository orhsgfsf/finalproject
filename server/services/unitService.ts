import * as Knex from 'knex'

export interface Unit {
    id: number
    unit: string
    rentalStatus: boolean
    unitStatus: boolean
    saleableArea: number | null
    grossFloorArea: number
    reminder: string
    gross_floor_area: number
    saleable_area: number
   

    // foreign key
    property_id: number
}

export interface Coordinates {
    x: number
    y: number
    height: number
    width: number

    unit_id:number
    // foreign key
    id: number
}

export class UnitService {
    constructor(private knex:Knex) {
    }

    // ------------------------------------------ Get things -------------------------------------------------

    // Get the details of units
    loadDetailsOfUnits = async (unit: Unit, userId:number) => {
        console.log(unit)
        return await this.knex.select("units.*")
            .from("units")
            .innerJoin("properties","properties.id", "units.property_id")
            .where("units.property_id", unit.id)
            .andWhere("units.unit_status", true)
            .andWhere("properties.user_id", userId)
          
            .orderBy("units.unit")
    }

 

    loadUnitByUnitId = async (unit: Unit, userId:number) => {
        console.log(unit)
        return await this.knex.select("units.*")
        .from("units")
        .innerJoin("properties", "properties.id", "units.property_id")
        .where("units.id", unit.id)
        .andWhere("properties.user_id", userId)
    }

    loadUnitForContract = async (unitId:number, userId:number) => {
        console.log(unitId)
        return await this.knex.select("units.id", "units.unit",
        "properties.address", "properties.management_fee", "properties.government_rent", "properties.rates", "properties.currency", 
        "property_types.propertyType",
        "countries.country",
        "users.first_name", "users.last_name", "users.br_number", "users.id_number", "users.date_of_birth")
        .from("units")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("property_types", "property_types.id", "properties.property_type_id")
        .innerJoin("countries", "countries.id", "properties.country_id")
        .innerJoin("users", "users.id", "properties.user_id")       
        .andWhere("units.id", unitId)
        .andWhere("users.id", userId)
       
    }

    loadUnitsList = async(userId:number) => {
        
        return await this.knex.select("units.id", "units.unit", "units.unit_status", "units.rental_status", "units.contract_pending",
        "properties.address", 
        "countries.country",
        "users.first_name", "users.last_name", "users.br_number", "users.id_number")
        .from("units")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("countries", "countries.id", "properties.country_id")
        .innerJoin("users", "users.id", "properties.user_id")
        .where("units.unit_status", true)
        .andWhere("units.rental_status", false)
        .andWhere("users.id", userId)
    }

    loadDropCoordinates = async(property_id:number, userId:number) => {
        return await this.knex.select(
            'units.x',
            'units.y',
            'units.width',
            'units.height',
            'units.id',
            'units.unit',
            'units.contract_pending',
            'units.rental_status'
        )
        .from('units')
        .where("units.unit_status", true)
        .andWhere("units.property_id", property_id)
       

    }

    // ------------------------------------------ Add things -------------------------------------------------

    // Start inserting things about the unit to database
    addDetailsOfUnit = async (unit:Unit) => {
        const unitDetails = {
            unit: unit.unit,
            gross_floor_area: unit.gross_floor_area,
            saleable_area: unit.saleable_area,
            rental_status: false,
            unit_status: true,
            reminder: unit.reminder,
            property_id: unit.property_id
        }
        
        return await this.knex.insert(unitDetails).into("units")
    }

        // Start inserting things to table of "unit_saleable_areas"
        

    // Start inserting things about the layer which puts on the floor plan for draggable to database

    // addDropCoordinates = async (coordinates:Coordinates[]) => {
    //     const result = coordinates.map(async(coordinate) => { 
    //         return {
    //         x_coordinate: coordinate.x,
    //         y_coordinate: coordinate.y,
    //         height: coordinate.height,
    //         width: coordinate.width,
    //         unit_id: coordinate.unit_id
    //         }
    //    })

    //    const promiseResult = await Promise.all(result)
    //    console.log(promiseResult)
      

    //   return await this.knex.insert(promiseResult).into("drop_coordinates")

        
       
    // }


    // ------------------------------------------ Update things -------------------------------------------------

    // Start updating details of the unit 
    updateDetailsOfUnit = async (unit:Unit) => {
        console.log("iamgoingtoeditthings")
        console.log(unit)
        await this.deleteUnit(unit)
        await this.addDetailsOfUnit(unit)
    }

    updateDropCoordinates = async (coordinates:Coordinates[]) => {
        const result = coordinates.map(async(coordinate) => { 
            return {
            x: coordinate.x,
            y: coordinate.y,
           id: coordinate.id
            }
       })

       const promiseResult = await Promise.all(result)
       console.log(promiseResult)
      

    promiseResult.map(async (result) => await this.knex("units").update(result).where("units.id", result.id))
    //   return await this.knex("units").update(promiseResult).where("units.id", )   
    }


    clearCoordinates = async (property_id:number) => {
        await this.knex("units").update({
            x: 0,
            y: 0,
            width: 100,
            height: 100
        })
        .where("units.property_id", property_id)
    }

    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete details of the unit by changing it's status to be "history"
    deleteUnit = async (unit: Unit) => {
        await this.knex("units").update({unit_status: false}).where("units.id", unit.id)
    }

    // Delete drop coordinates
    deleteCoordinates = async (unit: Unit) => {
        await this.knex.delete("drop_coordinates").where("drop_coordinates.unit_id", unit.id)
    }
}