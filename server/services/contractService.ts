import * as Knex from 'knex'

// import * as aws from 'aws-sdk'
// import * as multer from 'multer'
// import * as multerS3 from 'multer-s3'
const puppeteer = require('puppeteer');


export interface Contract {

    // -------------------------------------- For contracts --------------------------------------
    id?: number
    rentPrice: number
    rentDueDate: number
    startingDate: number
    expiryDate: number
    deposit: number
    gracePeriod: number
    gracePeriodReminder: string
    keys: number 
    code: string
    landlordSignature: string
    tenantSignature: string
    startContract: Date
    endContract: Date
    IdOrBrNumber: string
    landlordName: string
    unitId: number
    startGracePeriod: Date
    endGracePeriod: Date
    contract_pending: boolean
   



    // foreign key
    unit_id: number
    currency_id: number

    // -------------------------------------- For tenants --------------------------------------
    tenantName: string
    tenantIdOrBr: string
    contactPerson: string
    contactNumber: number
    businessNature: string

    // -------------------------------------- For currency --------------------------------------
    currency: string
}


export class ContractService {
    constructor(private knex: Knex) {
    }

    // ------------------------------------------ Get things -------------------------------------------------

    // When tenant load the draft of the contract (post)
    loadContractByCode = async (contract: Contract) => {
        return await this.knex.select(
            "contracts.*",
           
            "units.unit",
            
            "property_types.propertyType", 
            
            "properties.address", "properties.management_fee", "properties.government_rent", "properties.rates", "properties.saleable_area", "properties.gross_floor_area",
           
            "countries.country",
        )
            .from("contracts")
            
            .innerJoin("units", "contracts.unit_id", "units.id")
            .innerJoin("properties", "properties.id", "units.property_id")
            .innerJoin("property_types", "property_types.id", "properties.property_type_id")
            
            .innerJoin("countries", "countries.id", "properties.country_id")
            .where("contracts.code", contract.code)
    }

    // Get brief description of all contracts (distinct property)
    loadGeneralContracts = async (id: number) => {
        return await this.knex.distinct(
            // "contracts.*",
            "units.unit", "units.property_id",
            "properties.address",
            "countries.country",
        )
            .from("contracts")
            .innerJoin("units", "contracts.unit_id", "units.id")
            .innerJoin("properties", "units.property_id", "properties.id")           
            .innerJoin("countries", "countries.id", "properties.country_id")
            .innerJoin("users", "properties.user_id", "users.id")
            .where("users.id", id)
            .orderBy("contracts.contract_status").orderBy("contracts.confirm_clicked", "desc").orderBy("units.id").orderBy("properties.id")
    }

    loadContractByPropertyId = async (id: number) => {
        return await this.knex.select(
            "contracts.*",
            "units.unit", "units.property_id",
            "properties.address",
            "countries.country",
        )
            .from("contracts")
            .innerJoin("units", "contracts.unit_id", "units.id")
            .innerJoin("properties", "units.property_id", "properties.id")  
            .innerJoin("countries", "countries.id", "properties.country_id")         
            .where("units.property_id", id)
            .orderBy("contracts.contract_status").orderBy("contracts.confirm_clicked", "desc").orderBy("units.id")
    }

    loadContractByUnitId = async (id:number) => {
        const checkUnitRentalStatus =  await this.knex.select(
            "contracts.rent_due_date","contracts.grace_period","contracts.contract_starting_date","contracts.contract_expiry_date","contracts.rent_price",
            "contracts.deposit","contracts.grace_period_expiry_date","contracts.grace_period_starting_date",
            "contracts.tenant_name","contracts.contact_person","contracts.contact_number", "contracts.unit_id", "contracts.currency", "contracts.tenant_business_nature",
            "units.unit", "units.rental_status", "units.contract_pending", "units.gross_floor_area", "units.saleable_area"
        )
        .from("contracts")
            .innerJoin("units", "contracts.unit_id", "units.id")
            .where("contracts.contract_status", true)
            .andWhere("units.rental_status", true)
            .andWhere("contracts.unit_id", id)

        console.log({contract:checkUnitRentalStatus})

        if (checkUnitRentalStatus.length === 0){
            const checkConfirmClicked = await this.knex.select(
                "contracts.rent_due_date","contracts.grace_period","contracts.contract_starting_date","contracts.contract_expiry_date","contracts.rent_price",
            "contracts.deposit","contracts.grace_period_expiry_date","contracts.grace_period_starting_date",
            "contracts.tenant_name","contracts.contact_person","contracts.contact_number", "contracts.unit_id", "contracts.currency", "contracts.tenant_business_nature",
            "units.unit", "units.rental_status", "units.contract_pending", "units.gross_floor_area", "units.saleable_area"

            ).from("contracts")
            .innerJoin("units", "contracts.unit_id", "units.id")
            .where("contracts.confirm_clicked", false)
            .andWhere("contracts.unit_id", id)

            if (checkConfirmClicked.length === 0){
                return await this.knex.select(
                    "units.unit", "units.rental_status", "units.contract_pending", "units.gross_floor_area", "units.saleable_area"
                )
                .from("units")
                .where("units.id", id)
            }else{
                return checkConfirmClicked
            }
      
        }

        return checkUnitRentalStatus
        
    }

    loadDistinctUnitForContract = async (id:number) => {
        return await this.knex.distinct(
            "units.id", "units.unit"
        )
        .where("units.unit_status", true)
        .where("units.property_id", id)
        .orderBy("units.id")

    }

    loadContractDistinctCountries = async (id:number) => {
        await this.knex.distinct("countries.*")
        .from("contracts")
        .innerJoin("units", "units.id", "contracts.unit_id")
        .innerJoin("properties", "properties.id", "units.property_id")
        .innerJoin("countries", "properties.country_id", "countries.id")
        .where("properties.user_id", id)
    }

    loadContractByStatus = async (id:number) => {
        return await this.knex.select(
            "contracts.*",
            "units.unit", "units.property_id",
            "properties.address",
            "countries.country",
        )
            .from("contracts")
            .innerJoin("units", "contracts.unit_id", "units.id")
            .innerJoin("properties", "units.property_id", "properties.id")  
            .innerJoin("countries", "countries.id", "properties.country_id")         
            .where("units.property_id", id)
            .andWhere("contracts.contract_status", true)
            .orWhere("contracts.confirm_clicked", false)
            .orderBy("contracts.contract_status").orderBy("contracts.confirm_clicked", "desc").orderBy("units.id")
    }

    loadBusinessNaturesList = async () => {
        return await this.knex.select("*").from("business_natures")
    }



    // ------------------------------------------ Add things -------------------------------------------------

    // Draft a new contract for people to sign
    addContractDraft = async (contract: Contract) => {
        // Get currency 

        // Insert all things about the contract to the table but it's just a draft
        const contractObj = {
            rent_price: contract.rentPrice,
            rent_due_date: contract.rentDueDate,
            contract_starting_date: contract.startContract,
            contract_expiry_date: contract.endContract,
            deposit: contract.deposit,
            grace_period: contract.gracePeriod,
            code: contract.code,
            keys: contract.keys,            
            landlord_signature: contract.landlordSignature,
            landlord_id_or_br: contract.IdOrBrNumber,
            landlord_name: contract.landlordName,
            confirm_clicked: false,
            contract_status: false,
            unit_id: contract.unitId,
            currency: contract.currency,
            grace_period_starting_date: contract.startGracePeriod,
            grace_period_expiry_date: contract.endGracePeriod,
            

        }

        await this.knex("units").update({
            contract_pending: true
        }).where("units.id", contract.unitId)

        await this.knex.insert(contractObj).into("contracts")
        if (contract.tenantName){
            this.signContractByTenant(contract)
        }
    }

    activateContractPending = async(unitId:number) => {
        await this.knex("units").update({contract_pending:true}).where("units.id", unitId)
    }




    // ------------------------------------------ Update things -------------------------------------------------

    // When the contract is confirmed by tenants
    signContractByTenant = async (contract: Contract) => {
        
    
        // Add tenant
        const tenant = {
            tenant_name: contract.tenantName,
            contact_person: contract.contactPerson,
            contact_number: contract.contactNumber,
            tenant_id_or_br: contract.tenantIdOrBr,
            tenant_business_nature: contract.businessNature, 
            tenant_signature: contract.tenantSignature
        }

        await this.knex("contracts").update(tenant).where("contracts.code", contract.code)

    }


    // To start the contract
    confirmClick = async (id: number, unit_id:number) => {
        console.log(unit_id)
        await this.knex("contracts").update({
            confirm_clicked: true,
            contract_status: true
        }).where("contracts.id", id)

        await this.knex("units").update({
            rental_status: true,
            contract_pending: false
        }).where("units.id", unit_id)
    }


    // ------------------------------------------ Delete things -------------------------------------------------

    // Delete contract
    deleteContract = async (id:number, unit_id:number) => {

        await this.knex.transaction(async trx => {
     
            const check = await trx.select("contracts.confirm_clicked").from("contracts").where("contracts.id", id)
          
            if (check[0].confirm_clicked === true){
                await trx("contracts").update({ contract_status: false , void_contract_date: new Date()}).where("contracts.id", id)
            }else{
                await trx("contracts").where("contracts.id", id).delete()
            }
    
            await trx("units").update({ rental_status: false }).where("units.id", unit_id)
            await trx("units").update({ contract_pending: false }).where("units.id", unit_id)
        })
    }



    // --------------- to Pdf -----------------------
   
    contractToPdf = async (path:string) => {
        console.log("S33333333333333S3333")
        
        const browser = await puppeteer.launch({
            ignoreHTTPSErrors: true,
            headless: true
          })
        
        const page = await browser.newPage();

        await page.goto(`https://we-vital.com/contract/${path}`, {waitUntil: 'networkidle2', ignoreHTTPSErrors: true});
        // await page.goto(`https://localhost:3000/contract/${path}`, {waitUntil: 'networkidle2', ignoreHTTPSErrors: true});
      

        const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const bucket = "cdn.danny.hk";
const key = (req:any,file:any,cb:any)=>{
    cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`)}

const pdf = await page.pdf({path: `${path}.pdf`, format: 'A4'});
const params = { Bucket: bucket, Key: key, Body: pdf };
await s3.putObject(params).promise();
      
        await browser.close();
      };
    }



